"use strict";

module.exports = function(sequelize, DataTypes) {
  var CustomerPayments = sequelize.define("CustomerPayments", {
    customerId: DataTypes.INTEGER,
    driverId:DataTypes.INTEGER,
    partnerId:DataTypes.INTEGER,
    paymetType:DataTypes.STRING,
    acceptedBy:DataTypes.INTEGER,
    amount:DataTypes.STRING,
    status:DataTypes.STRING,
    paymentDate:DataTypes.DATE,
  }, {
    tableName: 'customerpayments'
  }

  );

  return CustomerPayments;
};