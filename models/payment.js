"use strict";

module.exports = function(sequelize, DataTypes) {
  var Payments = sequelize.define("Payments", {
    pickUpId:DataTypes.INTEGER,
    email: DataTypes.STRING,
    VendorReferenceNumber:DataTypes.STRING,
    amount:DataTypes.STRING,
    status:DataTypes.STRING,
    PaymentPlatform:DataTypes.STRING,
    paymentDate:DataTypes.DATE

  }, {
    tableName: 'payments'
  }

  );

  return Payments;
};