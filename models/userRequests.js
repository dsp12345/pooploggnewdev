"use strict";

//var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var UserRequests = sequelize.define("UserRequests", {
    refNo:DataTypes.INTEGER,
    title: DataTypes.STRING,
  	name: DataTypes.STRING,
    email:DataTypes.STRING,
    mobile:DataTypes.STRING,
    numberOfTrips: DataTypes.INTEGER,
    areaCode:DataTypes.STRING,
    address:DataTypes.STRING,
    driverId:DataTypes.INTEGER,
    partnerId: DataTypes.INTEGER,
    vehicleNumber: DataTypes.INTEGER,
    status:DataTypes.STRING,
    amount:DataTypes.FLOAT,
    managerId:DataTypes.INTEGER,
    callCenterMgrId:DataTypes.INTEGER,
    paymentType:DataTypes.STRING,
    paymentStatus:DataTypes.STRING,
    aboutPooplogg:DataTypes.STRING,
    dateTime:DataTypes.STRING
  }, {
    tableName: 'userRequests'
  }

  );

  return UserRequests;
};



8600617608



