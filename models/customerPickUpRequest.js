"use strict";

module.exports = function(sequelize, DataTypes) {
  var CustomerPickUpRequests = sequelize.define("CustomerPickUpRequests", {
    customerId: DataTypes.INTEGER,
    partnerId:DataTypes.INTEGER,
    driverId:DataTypes.INTEGER,
    location:DataTypes.STRING,
    latitude:DataTypes.STRING,
    longitude:DataTypes.STRING,
    amount:DataTypes.FLOAT,
    status:DataTypes.TEXT,
    paymentType:DataTypes.STRING,
    paymentStatus:DataTypes.STRING,
    requestDateTime:DataTypes.DATE,
  }, {
    tableName: 'customerPickUpRequests'
  }

  );

  return CustomerPickUpRequests;
};