"use strict";

//var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var DriverLocations = sequelize.define("DriverLocations", {
    driverId: DataTypes.INTEGER,
    location:DataTypes.STRING,
    latitude:DataTypes.STRING,
    longitude:DataTypes.STRING,
    isAssign:DataTypes.STRING
  }, {
    tableName: 'driverLocations'
  }

  );

  return DriverLocations;
};