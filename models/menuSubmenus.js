"use strict";

//var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var MenuSubMenus = sequelize.define("MenuSubMenus", {
    menuName: DataTypes.STRING,
    subMenuId: DataTypes.INTEGER,
    subMenuName: DataTypes.STRING,
    subMenuUrl:DataTypes.STRING,
    priority:DataTypes.STRING,
    chkOptional:DataTypes.STRING
  }, {
    tableName: 'menuSubMenus'
  }

  );

  return MenuSubMenus;
};