"use strict";

//var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var UpdatePartnerRequests = sequelize.define("UpdatePartnerRequests", {
    partnerId:DataTypes.INTEGER,
    username: DataTypes.STRING,
    email:DataTypes.STRING,
    name:DataTypes.STRING,
    mobile:DataTypes.STRING,
    directors:DataTypes.STRING,
    profileImage:DataTypes.TEXT,
    address:DataTypes.TEXT,
    partnerType:DataTypes.TEXT,
    certificateOfIncorporation:DataTypes.TEXT,
    proofOfOwnership:DataTypes.TEXT,
    idProof:DataTypes.TEXT,
    status:DataTypes.STRING,
    approvedDate:DataTypes.DATE

  }, {
    tableName: 'updatePartnerRequests'
  }

  );

  return UpdatePartnerRequests;
};