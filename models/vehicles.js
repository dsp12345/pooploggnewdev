"use strict";

module.exports = function(sequelize, DataTypes) {
  var Vehicles = sequelize.define("Vehicles", {
    userId: DataTypes.INTEGER,
    vehicleNumber:DataTypes.STRING,
    areaCode:DataTypes.STRING,
    vehicleColour:DataTypes.STRING,
    vehicleLicense:DataTypes.TEXT,
    hackneyPermit:DataTypes.TEXT,
    roadworthiness:DataTypes.TEXT,
    swadanCertificate:DataTypes.TEXT,
    insurance:DataTypes.TEXT,
    lagosStatePermit:DataTypes.TEXT,
    poopLogInspectionCert:DataTypes.INTEGER,
    isDriverAssign:DataTypes.STRING,
    isSpoterAssign:DataTypes.STRING,
    driverId: DataTypes.INTEGER,
    spoterId: DataTypes.INTEGER,
  }, {
    tableName: 'vehicles'
  }

  );

  return Vehicles;
};