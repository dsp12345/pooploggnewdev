"use strict";

//var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var Users = sequelize.define("Users", {
    username: DataTypes.STRING,
    email:DataTypes.STRING,
    password:DataTypes.STRING,
    name:DataTypes.STRING,
    mobile:DataTypes.STRING,
    directors:DataTypes.STRING,
    partnerType:DataTypes.STRING,
    registrationDocuments:DataTypes.STRING,
    certificateOfIncorporation:DataTypes.STRING,
    proofOfOwnership:DataTypes.STRING,
    address:DataTypes.STRING,
    areaCode:DataTypes.STRING,
    userType:DataTypes.INTEGER,
    vehicleNumber:DataTypes.STRING,
    logintype:DataTypes.STRING,
    profileId:DataTypes.STRING,
    token:DataTypes.TEXT,
    accesstoken:DataTypes.TEXT,
    profileImage:DataTypes.TEXT,
    driverLicence:DataTypes.TEXT,
    lasdriLicense:DataTypes.TEXT,
    vehicleColour:DataTypes.TEXT,
    vehicleLicenseNumber:DataTypes.TEXT,
    addedBy:DataTypes.INTEGER,
    partnerId:DataTypes.INTEGER,
    status:DataTypes.TEXT,
    paymentStatus:DataTypes.STRING,
    amount:DataTypes.STRING,
    deviceId:DataTypes.TEXT,
    onDuty:DataTypes.STRING,
    

  }, {
    tableName: 'users'
  }

  );

  return Users;
};