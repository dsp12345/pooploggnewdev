"use strict";

//var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var UserRoles = sequelize.define("UserRoles", {
    userRole: DataTypes.STRING,
    status: DataTypes.INTEGER

  }, {
    tableName: 'userRoles'
  }

  );

  return UserRoles;
};