"use strict";

//var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var Areas = sequelize.define("Areas", {
    areaCode: DataTypes.STRING,
    description: DataTypes.STRING,
    status: DataTypes.INTEGER

  }, {
    tableName: 'areas'
  }

  );

  return Areas;
};