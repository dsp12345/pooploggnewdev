"use strict";

//var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var MenuAccess = sequelize.define("MenuAccess", {
  	userRoleId: DataTypes.INTEGER,
    menuId: DataTypes.INTEGER,
    subMenuId: DataTypes.INTEGER
  }, {
    tableName: 'menuAccess'
  }

  );

  return MenuAccess;
};