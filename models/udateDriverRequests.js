"use strict";

//var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var UpdateDriverRequests = sequelize.define("UpdateDriverRequests", {
    driverId:DataTypes.INTEGER,
    username: DataTypes.STRING,
    email:DataTypes.STRING,
    name:DataTypes.STRING,
    mobile:DataTypes.STRING,
    vehicleNumber:DataTypes.STRING,
    profileImage:DataTypes.TEXT,
    driverLicence:DataTypes.TEXT,
    lasdriLicense:DataTypes.TEXT,
    addedBy:DataTypes.INTEGER,
    status:DataTypes.STRING,
    approvedDate:DataTypes.DATE

  }, {
    tableName: 'updateDriverRequests'
  }

  );

  return UpdateDriverRequests;
};