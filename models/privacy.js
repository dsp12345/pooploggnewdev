"use strict";

//var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var Privacy = sequelize.define("Privacy", {
    privacyData: DataTypes.TEXT

  }, {
    tableName: 'privacy'
  }

  );

  return Privacy;
};