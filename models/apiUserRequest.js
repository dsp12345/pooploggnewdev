"use strict";

//var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var ApiUserRequests = sequelize.define("ApiUserRequests", {
    refNo:DataTypes.INTEGER,
  	name: DataTypes.STRING,
    email:DataTypes.STRING,
    numberOfTrips: DataTypes.INTEGER,
    mobile:DataTypes.STRING,
    areaCode:DataTypes.STRING,
    address:DataTypes.STRING,
    status:DataTypes.STRING,
    isAccept:DataTypes.INTEGER,
    aboutPooplogg:DataTypes.STRING,
    dateTime:DataTypes.STRING
    
  }, {
    tableName: 'apiuserRequests'
  }

  );

  return ApiUserRequests;
};