"use strict";

module.exports = function(sequelize, DataTypes) {
  var DrverAttendance = sequelize.define("DrverAttendance", {
    driverId: DataTypes.INTEGER,
    spoterId: DataTypes.INTEGER,
    vehicleNumber:DataTypes.STRING,
    fromTime:DataTypes.DATE,
    toTime:DataTypes.DATE,
    totalHrs:DataTypes.TEXT,
    onDuty:DataTypes.TEXT,
  }, {
    tableName: 'drverAttendance'
  }

  );

  return DrverAttendance;
};