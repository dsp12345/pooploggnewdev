var express=require("express"),
 app = express(),
 mysql = require('mysql'),
 bodyParser=require('body-parser'),
 mysqlModel = require('mysql-model');
 config = require('./config');
 var linkedinStrategy = require('passport-linkedin').Strategy;
 var TwitterStrategy  = require('passport-twitter').Strategy;
 var FacebookStrategy = require('passport-facebook').Strategy;
 var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

 //var FacebookStrategy = require('passport-facebook').Strategy;
 var jwt    = require('jsonwebtoken');
 var flash = require('express-flash');
 var fs = require("fs");
 var session = require('express-session');

var multer = require('multer');

var upload = multer({ dest: '/tmp' });
var moment = require('moment');

 //var Users = require('./../models').Users;
var Users = require('./models').Users;
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/public'));

app.use(session({secret:'XASDASDA'}));
var ssn ;

app.set('view engine', 'ejs');

 passport = require('passport'),
 Strategy = require('passport-local').Strategy;

app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false }));
app.use(bodyParser());

  //app.use(flash());

//app.use(cookieParser('secretString'));
//app.use(session({cookie: { maxAge: 60000 }}));
app.use(flash());


app.get('/',function(req,res){
    res.render('login/login');
})


var setPasswordController = require('./controllers/setPasswordController');
app.use('/setPassword', setPasswordController);

// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());

passport.use(new Strategy(
  function(username, password, cb) {
      Users.find({'where':{"username":username,"status":"Y"}})
      .then(function(user)
      {
          if (!user) { return cb(null, false); }
          if (user.password != password) { return cb(null, false); }
           return cb(null, user);
      })
    .catch(function(err)
    {
      if (err) { return cb(err); }
    })

  }));


passport.serializeUser(function(user, cb) {
  cb(null, user);
  //cb(null, user[0].id);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});



app.get('/',function(req,res){
    res.render('login/login');
})

app.post('/login', 
  passport.authenticate('local', { failureRedirect: '/signin' }),
  function(req, res) {
    res.redirect('/home');
});

app.get('/signin',function(req,res){
      req.flash('error','User can not be authenticated.');
      res.render('login/login');
})


//mobile api
app.post('/api/login', 
  passport.authenticate('local', { failureRedirect: '/api/signin' }),
  function(req, res) {

    //res.redirect('/api/home');

//change code android
  var username = req.user.username;
  var accesstoken = generateToken(username);

  if(accesstoken){
    //req.user.accesstoken;
    var userData = req.user;

    //console.log(userData);

    Users.update({'accesstoken':accesstoken},{'where':{'username':username}})
        .then(function(data){

           Users.find({'where':{"username":username}})
          .then(function(sessUsers)
          { 

          //userData.accesstoken;

              return res.status(200).send({status:'success','data':{'message':'User login successfully.',"user":sessUsers}});
          })
          .catch(function(err){
              var data=[];
              return res.status(400).send({status:'error','data':{'message':'User authentication failed.',"user":data}});
          })

        })
        .catch(function(err){
            var data=[];
            return res.status(400).send({status:'error','data':{'message':'User authentication failed.',"user":data}});
        })
    }else{
            var data=[];
            return res.status(400).send({status:'error','data':{'message':'User accesstoken not found.',"user":data}});
    }
});

app.get('/api/signin',function(req,res){
        return res.status(401).send({status:'error','data':{'message':'User can not be authenticated.'}});
})


app.get('/aa',function(req,res){
    ssn = req.session;
    var aa = ssn.accesstoken;

      //req.flash('error','User can not be authenticated.');
      //res.render('login/login');
})

var registerController = require('./controllers/registerController');
app.use('/signup', registerController);

var providerSignupController = require('./controllers/providerSignupController');
app.use('/partnerSignup', providerSignupController);

var userSignupController = require('./controllers/userSignupController');
app.use('/api/userSignup', userSignupController);


var homeController = require('./controllers/homeController');
app.use('/home', homeController);



//mobile

app.get('/api/home',function(req,res){
    //ssn = req.session;

    console.log("here login");
    //console.log(req.user);

 // var username = req.user.username;
  //var accesstoken = generateToken(username);

  // if(accesstoken){
  //   //req.user.accesstoken;
  //   var userData = req.user;

  //   //console.log(userData);

  //   Users.update({'accesstoken':accesstoken},{'where':{'username':username}})
  //       .then(function(data){

  //          Users.find({'where':{"username":username}})
  //         .then(function(sessUsers)
  //         { 

  //         //userData.accesstoken;

  //             return res.status(200).send({status:'success','data':{'message':'User login successfully.',"user":sessUsers}});
  //         })
  //         .catch(function(err){
  //             var data=[];
  //             return res.status(400).send({status:'error','data':{'message':'User authentication failed.',"user":data}});
  //         })

  //       })
  //       .catch(function(err){
  //           var data=[];
  //           return res.status(400).send({status:'error','data':{'message':'User authentication failed.',"user":data}});
  //       })
  //   }else{
  //           var data=[];
  //           return res.status(400).send({status:'error','data':{'message':'User accesstoken not found.',"user":data}});
  //   }
});


function generateToken(username){
   var payload = {
      user: username 
    };
    var accesstoken = jwt.sign(payload, 'abcd', {
     // expiresInMinutes: 1440 // expires in 24 hours
    });
    return accesstoken;
}



//middleware to authenticate token before displaying user profile
app.use(function(req, res, next) {
            console.log("verifying token");

  var accesstoken = req.body.accesstoken;
console.log(req.session);
    console.log(req.body.accesstoken);
    ssn = req.session;
    var username = ssn.username;
    console.log("verifying token");
    if(username){

          console.log("inside session token");

    
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;

      if(username){
        Users.find({'where':{"username":username}})
        .then(function(result)
        {
          if(result){
              return next();
          }else{
            req.flash('error','Failed to authenticate token.');
            res.redirect('/');

          }
      })
      .catch(function(err)
      {
        req.flash('error','Failed to authenticate token.');
        res.redirect('/');
      })


      }else{
        req.flash('error','Failed to authenticate token.');
        res.redirect('/');

       console.log("here");

      }

     }

     else if(accesstoken){
 console.log("hhhhh");
        Users.find({'where':{"accesstoken":accesstoken}})
          .then(function(result)
          {
            console.log(result);
            if(result){
                return next();
            }else{
              return res.status(200).send({status:'error','data':{'message':'Failed to authenticate token.'}});

            }
        })
        .catch(function(err)
        {
            return res.status(200).send({status:'error','data':{'message':'11Failed to authenticate token.'}});

        })
      }


     else{

      console.log("elseeee");
        req.flash('error','Authentication Failed.');
        res.redirect('/');

     }
});


app.get('/signout',function(req, res){
    req.logout();
     req.flash('error','User logout successfully.');
     res.redirect('/');
  });

function updateUserAccesstoken(username,accesstoken){

}

var loginController = require('./controllers/loginController');
app.use('/login', loginController);

var driverController = require('./controllers/driverController');
app.use('/driver', driverController);

var spotersController = require('./controllers/spotersController');
app.use('/spoters', spotersController);

var providerController = require('./controllers/providerController');
app.use('/partners', providerController);
var vehicleController = require('./controllers/vehicleController');
app.use('/vehicle', vehicleController);

var adminDriverController = require('./controllers/adminDriverController');
app.use('/adminDriver', adminDriverController);

var adminSpoterController = require('./controllers/adminSpoterController');
app.use('/adminSpoter', adminSpoterController);

var driverAttendanceController = require('./controllers/driverAttendanceController');
app.use('/api/driverAttendance', driverAttendanceController);

//api

var apidriverController = require('./controllers/api/driverController');
app.use('/api/driver', apidriverController);


var reportsController = require('./controllers/reportsController');
app.use('/reports', reportsController);

//var searchController = require('./controllers/searchController');
//app.use('/api/search', searchController);

app.listen(8002);

module.exports = app;