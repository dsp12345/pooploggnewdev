window.closeActionForm = true;
(function () {
    function onsingleApiJSFile() {

        var oneSingleApiHtml = '<style>@import url(https://fonts.googleapis.com/css?family=Exo);.clear{clear: both;}#myModal-onesingleapi{display: none; position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; overflow: auto; background-color: rgba(0, 0, 0, 0.9); animation-name: fadeIn; animation-duration: 0.4s; z-index:9999999;}.onesingleapi-callback{position: fixed; right: 0; left: 0; margin: auto; width: 40%; min-width: 700px; height: auto; -webkit-animation-name: slideIn; -webkit-animation-duration: 0.4s; animation-name: slideIn; animation-duration: 0.4s;}.banner-top{margin: 4% auto; width: 30%; padding: 2em 30px; background: #ffffff; font-family: "Exo", sans-serif; border-radius: 4px;}.banner-top h2{text-align: center; font-size: 2em; font-weight: 600; margin: 0 0 6% 0; color: #fff; font-family: "Exo", sans-serif; text-transform: uppercase;}.banner-bottom{margin-top: 3%;}.bnr-left{width: 47%; float: left; margin-right: 3%;}.bnr-right1{width: 50%; float: right;}.bnr-one{height: 80px}.bnr-one:nth-child(1){margin-top: 0;}.bnr-right input[type="text"]{width: 95%; color: #000000; outline: none; font-family: "Exo", sans-serif; font-size: 24px; font-weight: 600; padding: 10px 10px; border: none; -webkit-appearance: none; border-radius: 5px; min-height: 40px; border: 2px solid #44b669;}.cardno{background: url(https://onesingleapi.com/OneSingleApiPlugin/credit-card.png) no-repeat 95.5% 45% #fff; background-size: 40px 40px; cursor: pointer;}.exp-singleapi{background: url(https://onesingleapi.com/OneSingleApiPlugin/calendar.png) no-repeat 95.5% 45% #fff; background-size: 40px 40px; cursor: pointer;}.cvv-singleapi{background: url(https://onesingleapi.com/OneSingleApiPlugin/question.png) no-repeat 95.5% 45% #fff; background-size: 40px 40px; cursor: pointer;}.bnr-btn{text-align: center;}.bnr-btn input[type=submit]{color: #FFFFFF; padding: 7px 25px; font-size: 16px; cursor: pointer; font-weight: 600; border: none; background: #44b669; outline: none; border-radius: 5px;}.bnr-btn input[type=submit]:hover{background: #2f7e49; color: #fff; transition: 0.5s all ease; -webkit-transition: 0.5s all ease; -moz-transition: 0.5s all ease; -o-transition: 0.5s all ease; -ms-transition: 0.5s all ease;}.bnr-right1 input[type="password"]{width: 92%; color: #000000; outline: none; font-family: "Exo", sans-serif; font-size: 20px; padding: 10px 10px; border: none; -webkit-appearance: none; border-radius: 5px; font-weight: 600; min-height: 60px; border: 2px solid #44b669;}.bnr-right1 input[type="text"]{width: 92%; color: #000000; outline: none; font-family: "Exo", sans-serif; font-size: 20px; padding: 10px 10px; border: none; -webkit-appearance: none; border-radius: 5px; font-weight: 600; min-height: 60px; border: 2px solid #44b669;}.bnr-right.img{margin: 60px 20px; min-width: 800px; max-height: 48px; position: absolute;}.bnr-right.img img{max-height: 48px; max-width: 80px; margin: 0 10px; position: static;}.bnr-left input[type="text"]{width: 95%; color: #000000; outline: none; font-family: "Exo", sans-serif; font-size: 20px; padding: 10px 10px; border: none; -webkit-appearance: none; border-radius: 5px; font-weight: 600; min-height: 60px; border: 2px solid #44b669;}.banner-bottom p{font-size: 1em; color: #000000; margin: 1em 0; line-height: 1.8em; margin-top: 30px; margin-bottom: -5px}.banner-top{width: 70%;}.close-singleApi{color: white; float: right; font-size: 28px; font-weight: bold;}.close-singleApi:hover,.close-singleApi:focus{color: #000; text-decoration: none; cursor: pointer;}.info-singleapi{max-width: 40px; min-height: 40px; position: relative; bottom: 50px; margin-left: 80%; cursor: pointer;}@-webkit-keyframes slideIn{from{bottom: -400px; opacity: 0; min-width: 0px; width: 0px; height: 0px;}to{bottom: 40%; opacity: 1}}@keyframes slideIn{from{bottom: -400px; opacity: 0; min-width: 0px; width: 0px; height: 0px;}to{bottom: 40%; opacity: 1}}@-webkit-keyframes fadeIn{from{opacity: 0}to{opacity: 1}}@keyframes fadeIn{from{opacity: 0}to{opacity: 1}}.shake{animation: shake 0.82s cubic-bezier(.36, .07, .19, .97) both; transform: translate3d(0, 0, 0); backface-visibility: hidden; perspective: 1000px;}@keyframes shake{10%, 90%{transform: translate3d(-1px, 0, 0);}20%, 80%{transform: translate3d(2px, 0, 0);}30%, 50%, 70%{transform: translate3d(-4px, 0, 0);}40%, 60%{transform: translate3d(4px, 0, 0);}}[tooltip]{position: relative;}[tooltip]::before,[tooltip]::after{text-transform: none; font-size: .9em; line-height: 1; user-select: none; pointer-events: none; position: absolute; display: none; opacity: 0;}[tooltip]::before{content: ""; border: 5px solid transparent; /* opinion 4 */ z-index: 1001; /* absurdity 1 */}[tooltip]::after{content: attr(tooltip); /* magic! */ /* most of the rest of this is opinion */ font-family: Helvetica, sans-serif; text-align: center; min-width: 3em; max-width: 41em; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; padding: 1ch 1.5ch; border-radius: .3ch; box-shadow: 0 1em 2em -.5em rgba(0, 0, 0, 0.35); background: #333; color: #fff; z-index: 1000; /* absurdity 2 */}/* Make the tooltips respond to hover*/[tooltip]::before,[tooltip]::after{display: block;}/* don"t show empty tooltips */[tooltip=""]::before,[tooltip=""]::after{display: none !important;}/* FLOW: UP */[tooltip]:not([flow])::before,[tooltip][flow^="up"]::before{bottom: 100%; border-bottom-width: 0; border-top-color: #333;}[tooltip]:not([flow])::after,[tooltip][flow^="up"]::after{bottom: calc(100% + 5px);}[tooltip]:not([flow])::before,[tooltip]:not([flow])::after,[tooltip][flow^="up"]::before,[tooltip][flow^="up"]::after{left: 50%; transform: translate(-50%, -.5em);}/* FLOW: DOWN */[tooltip][flow^="down"]::before{top: 100%; border-top-width: 0; border-bottom-color: #333;}[tooltip][flow^="down"]::after{top: calc(100% + 5px);}[tooltip][flow^="down"]::before,[tooltip][flow^="down"]::after{left: 50%; transform: translate(-50%, .5em);}[tooltip][flow^="left"]::before{top: 50%; border-right-width: 0; border-left-color: #333; left: calc(0em - 5px); transform: translate(-.5em, -50%);}[tooltip][flow^="left"]::after{top: 50%; right: calc(100% + 5px); transform: translate(-.5em, -50%);}[tooltip][flow^="right"]::before{top: 50%; border-left-width: 0; border-right-color: #333; right: calc(0em - 5px); transform: translate(.5em, -50%);}[tooltip][flow^="right"]::after{top: 50%; left: calc(100% + 5px); transform: translate(.5em, -50%);}@keyframes tooltips-vert{to{opacity: .9; transform: translate(-50%, 0);}}@keyframes tooltips-horz{to{opacity: .9; transform: translate(0, -50%);}}[tooltip]:not([flow]):hover::before,[tooltip]:not([flow]):hover::after,[tooltip][flow^="up"]:hover::before,[tooltip][flow^="up"]:hover::after,[tooltip][flow^="down"]:hover::before,[tooltip][flow^="down"]:hover::after{animation: tooltips-vert 300ms ease-out forwards;}[tooltip][flow^="left"]:hover::before,[tooltip][flow^="left"]:hover::after,[tooltip][flow^="right"]:hover::before,[tooltip][flow^="right"]:hover::after{animation: tooltips-horz 300ms ease-out forwards;}.customerName-onesingle{text-align: center; font-size: 20px; color:#000}.customerImg-onesingle{max-width: 400px; display: block; margin: auto;height:50px;}@media only screen and (max-width: 600px){.banner-top{width: 60%; margin-left: 10px;}.bnr-right.img img{max-height: 38px; max-width: 60px; margin: 0 10px; position: static;}}@media only screen and (max-width: 500px){.banner-top{margin-left: 20px; width: 50%;}.bnr-right1 input[type="text"]{width: 72%; margin-left: 18%;}.bnr-left input[type="text"]{width: 82%;}.banner-bottom .bnr-right1 p{margin-left: 18%;}.bnr-right.img{margin: 60px -10px;}}@media only screen and (max-width: 480px){.bnr-right.img img{}.banner-top{margin-left: 12px; width: 50%;}}</style><div class="modal" id="myModal-onesingleapi"> <div class="onesingleapi-callback"><span class="close-singleApi" style="">&times;</span> <div class="banner-top"> <img class="customerImg-onesingle" src=""/> <p class="customerName-onesingle"></p><hr> <div class="banner-bottom"> <div class="bnr-one"> <div class="bnr-right"> <p>Card Number</p><input class="cardno cardnum onsinglrapi" maxlength="19" onkeypress="return isNumber(event)" placeholder="0000 0000 0000 0000" required="" type="text" onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false" autocomplete=off> <div class="info-singleapi luhn-failed" tooltip="Wrong card number" flow="right" style="margin-left: 90%;"></div></div><div class="clearfix"></div></div><div class="bnr-one"> <div class="bnr-left"> <p>Expiry Date</p><input class="exp-singleapi" maxlength="7" onkeypress="return isNumber(event)" placeholder="MM / YY" required="" type="text" onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false" autocomplete=off> <div class="info-singleapi exp-check" tooltip="Invalid Expiry Data" flow="right" style="margin-left: 90%;"></div></div><div class="bnr-right1"> <p>CVV Number</p><input class="cvv-singleapi" maxlength="3" onkeypress="return isNumber(event)" placeholder="000" required="" type="password" onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false" autocomplete=off> <div class="info-singleapi" tooltip="CVV is the three or four digit at the back of your card" flow="right"></div></div><div class="clear"></div></div><div class="bnr-one"> <div class="bnr-right img" style=""> <img src="https://onesingleapi.com/OneSingleApiPlugin/master.ico"> <img src="https://onesingleapi.com/OneSingleApiPlugin/visa.png"> <img src="https://onesingleapi.com/OneSingleApiPlugin/verve.png"> <img src="https://onesingleapi.com/OneSingleApiPlugin/psi.png"> </div></div><div class="bnr-btn" style="margin: 50px 50px 0px;"> <form> <input id="submit-singleapi" type="submit" value="Make Payment"> </form> </div></div></div></div></div>';

        var oneSingleApiOk = '<style>.checkOneSingleApi{position:fixed; width:100%; height:100%; top:0px; left:0px; background: rgba(0, 0, 0, 0.9); z-index:9999}.checkOneSingleApi .checkmark-circle {position:absolute; top:40%; left:0; right:0; margin:auto;}.checkmark-circle{width: 150px; height: 150px; position: relative; display: inline-block; vertical-align: top;}.checkmark-circle .background{width: 150px; height: 150px; border-radius: 50%; background: #2EB150; position: absolute;}.checkmark-circle .checkmark{border-radius: 5px;}.checkmark-circle .checkmark.draw:after{-webkit-animation-delay: 100ms; -moz-animation-delay: 100ms; animation-delay: 100ms; -webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s; -webkit-animation-timing-function: ease; -moz-animation-timing-function: ease; animation-timing-function: ease; -webkit-animation-name: checkmark; -moz-animation-name: checkmark; animation-name: checkmark; -webkit-transform: scaleX(-1) rotate(135deg); -moz-transform: scaleX(-1) rotate(135deg); -ms-transform: scaleX(-1) rotate(135deg); -o-transform: scaleX(-1) rotate(135deg); transform: scaleX(-1) rotate(135deg); -webkit-animation-fill-mode: forwards; -moz-animation-fill-mode: forwards; animation-fill-mode: forwards;}.checkmark-circle .checkmark:after{opacity: 1; height: 75px; width: 37.5px; -webkit-transform-origin: left top; -moz-transform-origin: left top; -ms-transform-origin: left top; -o-transform-origin: left top; transform-origin: left top; border-right: 15px solid white; border-top: 15px solid white; border-radius: 2.5px !important; content: ""; left: 25px; top: 75px; position: absolute;}@-webkit-keyframes checkmark{0%{height: 0; width: 0; opacity: 1;}20%{height: 0; width: 37.5px; opacity: 1;}40%{height: 75px; width: 37.5px; opacity: 1;}100%{height: 75px; width: 37.5px; opacity: 1;}}@-moz-keyframes checkmark{0%{height: 0; width: 0; opacity: 1;}20%{height: 0; width: 37.5px; opacity: 1;}40%{height: 75px; width: 37.5px; opacity: 1;}100%{height: 75px; width: 37.5px; opacity: 1;}}@keyframes checkmark{0%{height: 0; width: 0; opacity: 1;}20%{height: 0; width: 37.5px; opacity: 1;}40%{height: 75px; width: 37.5px; opacity: 1;}100%{height: 75px; width: 37.5px; opacity: 1;}}.transactionInfo{position: absolute;top: 30%; width: 500px; margin: auto; left: 0; right: 0; text-align: center; font-size: 30px; color: #ffffff;}</style><div class="checkOneSingleApi"><div class="transactionInfo"></div><div class="checkmark-circle"> <div class="background"></div><div class="checkmark draw"></div></div>';

        var oneSingleApiFailed = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"><style>.container-failed{position:fixed; width:100%; height:100%; top:0px; left:0px; background: rgba(0, 0, 0, 0.9);z-index:9999;text-align:center}.circle-icon{background: #7e7e7e; border-radius: 50%; display: table; margin: 0 auto; height: 6em; width: 6em;}.circle-icon i[class*=" fa"],.circle-icon i[class^="fa"]{color: #fff; display: table-cell; font-size: 3em; text-align: center; vertical-align: middle; width: 100%;}.circle-icon.animate{-webkit-animation: fall-in 0.75s; animation: fall-in 0.75s; position: absolute;top: 40%;right: 0;left: 0;}.circle-icon--error{background: #c84d40;}@-webkit-keyframes fall-in{0%{-webkit-transform: scale(3, 3); transform: scale(3, 3); opacity: 0;}50%{-webkit-transform: scale(1, 1); transform: scale(1, 1); opacity: 1;}60%{-webkit-transform: scale(1.1, 1.1); transform: scale(1.1, 1.1);}100%{-webkit-transform: scale(1, 1); transform: scale(1, 1);}}@keyframes fall-in{0%{-webkit-transform: scale(3, 3); transform: scale(3, 3); opacity: 0;}50%{-webkit-transform: scale(1, 1); transform: scale(1, 1); opacity: 1;}60%{-webkit-transform: scale(1.1, 1.1); transform: scale(1.1, 1.1);}100%{-webkit-transform: scale(1, 1); transform: scale(1, 1);}}.transactionInfo{position: absolute;top: 30%; width: 500px; margin: auto; left: 0; right: 0; text-align: center; font-size: 30px; color: #ffffff;}</style><div class="container-failed"> <div class="circle-icon circle-icon--error animate"> <i class="fa fa-times" aria-hidden="true"></i> </div><div class="transactionInfo"></div></div>';

        var oneSingleApiLoader = '<style>.loaderOneSingleApi{position:fixed; width:100%; height:100%; top:0px; left:0px; background: rgba(0, 0, 0, 0.9);  z-index: 9999;}.loaderOneSingleApi svg {position:absolute; top:40%; left:0; right:0; margin:auto;}.transactionInfoPreL{position: absolute;top: 15%; width: 500px; margin: auto; left: 0; right: 0; text-align: center; font-size: 30px; color: #ffffff;}</style><div class="loaderOneSingleApi"><svg width="106px" height="106px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-ring"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><circle cx="50" cy="50" r="49" stroke-dasharray="200.1194520336698 107.7566280181299" stroke="#d25353" fill="none" stroke-width="2"><animateTransform attributeName="transform" type="rotate" values="0 50 50;180 50 50;360 50 50;" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite" begin="0s"></animateTransform></circle></svg><div class="transactionInfoPreL"></div></div>';

        var te, logoUrl, businessName, phoneNumber, MerchantRef;
        
        (function () {
            function enet(val) {
            console.log("first");
                //alert("ssssss");
                this.elems = document.querySelectorAll(val);
                this.itinerate = function (callback) {
                    if (!NodeList.prototype.forEach) {
                        NodeList.prototype.forEach = Array.prototype.forEach;
                    }
                    this.elems.forEach(callback);
                };
            }

            enet.prototype.hide = function () {
                var local = function (el) {
                    el.style.display = "none";
                }
                this.itinerate(local);
            }

            enet.prototype.show = function () {
                var local = function (el) {
                    el.style.display = "block";
                }
                this.itinerate(local);
            }

            var eventsType = ["click", "keyup", "keypress"];
            eventsType.forEach(function (d) {
                enet.prototype[d] = function (callback) {
                    var local = function (el) {
                        el.addEventListener(d, callback);
                    }
                    this.itinerate(local);
                }
            });

            enet.prototype.append = function (text) {
                var local = function (el) {
                    var wrapper = document.createElement('div');
                    wrapper.innerHTML = text;
                    el.appendChild(wrapper);
                }
                this.itinerate(local);
            }

            enet.prototype.node = function () {
                return this.elems[0];
            }

            enet.prototype.style = function (obj) {
                var local = function (el) {
                    for (var key in obj) {
                        el.style[key] = obj[key];
                    }
                }
                this.itinerate(local);
            }

            enet.prototype.addClass = function (text) {
                var local = function (el) {
                    el.className += text;
                }
                this.itinerate(local);
            }

            enet.prototype.removeClass = function (text) {
                var local = function (el) {
                    el.className = el.className.replace(text, "");
                }
                this.itinerate(local);
            }

            enet.prototype.hasClass = function (text) {
                var state;
                var local = function (el) {
                    if (el.className.indexOf(text) !== -1) state = true;
                    else state = false;
                }
                this.itinerate(local);
                return state;
            }

            enet.prototype.empty = function () {
                var local = function (el) {
                    el.innerHTML = "";
                }
                this.itinerate(local);
            }

            enet.prototype.focus = function () {
                var local = function (el) {
                    el.focus();
                }
                this.itinerate(local);
            }

            enet.prototype.random = function (num) {
                arr = [];
                for (i = 0; i < num; i++) {
                    arr.push(Math.floor(Math.random() * 9));
                }
                return arr.join("");
            }

            enet.prototype.attr = function (attr, prop) {
                var local = function (el) {
                    el.setAttribute(attr, prop);
                }
                this.itinerate(local);
            }

            te = function (identitfier) {
                return new enet(identitfier);
            }

            te.HTTPRequest = function (type, url, postdata, callback) {
                http = new XMLHttpRequest();
                http.open(type, url, true);
                http.onreadystatechange = function () {
                    if (http.readyState == 4 && http.status == 200) {
                        if (http.responseText) {
                            callback(http.responseText);
                        }
                    }
                }
                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                http.send(postdata);
            }

            te.replace = function (replaceThis, withThis, inThis) {
                var RegE = new RegExp(replaceThis, "g");
                var actOn = inThis;
                if (typeof actOn === "string") return actOn.replace(RegE, withThis);
                return actOn;
            }
        }());

        //show modal
        te(".single-api-container").append(oneSingleApiHtml);
        te.HTTPRequest("GET", "https://onesingleapi.com/API/GetVendorDetails?encryptionkey=" + oneSingleApiEncrypKey, null, function (response) {

            console.log("GetVendorDetails");
            var responseJson = JSON.parse(response);
            businessName = responseJson.BusinessName;
            logoUrl = responseJson.Logo_Path;
            console.log(logoUrl + " " + businessName);

            te(".customerImg-onesingle").attr("src", logoUrl);
            document.querySelector(".customerName-onesingle").innerText = businessName;
        });

        function stylesheetCompforMoz(rules) {
            var style = document.createElement('style'), styleSheet;
            document.querySelector(".single-api-container style").appendChild(style);
            styleSheet = style.sheet;
            styleSheet.insertRule(rules, styleSheet.cssRules.length);
        }

        function hideToToolTip(clas) {
            try {
                document.styleSheets[0].addRule(clas + '[tooltip][flow^="right"]::before', 'display: none');
                document.styleSheets[0].addRule(clas + '[tooltip][flow^="right"]::after', 'display: none');
            } catch (err) {
                stylesheetCompforMoz(clas + '[tooltip][flow^="right"]::before' + '{display: none}');
                stylesheetCompforMoz(clas + '[tooltip][flow^="right"]::after' + '{display: none}');
            }
        }

        function ShowToToolTip(clas) {
            try {
                document.styleSheets[0].addRule(clas + '[tooltip][flow^="right"]::before', 'display: block; animation: tooltips-horz 300ms ease-out forwards');
                document.styleSheets[0].addRule(clas + '[tooltip][flow^="right"]::after', 'display: block; animation: tooltips-horz 300ms ease-out forwards');
            } catch (err) {
                stylesheetCompforMoz(clas + '[tooltip][flow^="right"]::before' + '{display: block; animation: tooltips-horz 300ms ease-out forwards}');
                stylesheetCompforMoz(clas + '[tooltip][flow^="right"]::after' + '{display: block; animation: tooltips-horz 300ms ease-out forwards}');
            }
        }
        hideToToolTip('.luhn-failed');
        hideToToolTip('.exp-check');

        var formState = { "cardno": false, "cvv": false, "expDate": false, "amount": false };//record state of form

        te("#btn-singleapi").click(function (event) {

            console.log("make payment call");
            event.preventDefault();
            if (document.getElementById("airtime_phoneno") == null) {
                phoneNumber = "";
            }
            else {
                phoneNumber = document.getElementById("airtime_phoneno").value;
            }
            var amount = te("input#amount-singleapi").node().value;
            var naira = '\u20A6';
            if (amount) formState.amount = true;
            if (amount.length >= 1) {
                te("#myModal-onesingleapi").show();
                te("input#submit-singleapi").node().value = "Make Payment of " + naira + amount;
            }
        });

        te(".close-singleApi").click(function () {
            te("#myModal-onesingleapi").hide();
        });

        window.onclick = function (event) {
            if (event.target == te("#myModal-onesingleapi").node()) {
                te("#myModal-onesingleapi").hide();
            }
        }
        //end show modal

        //validate card number
        te("input.cardnum.onsinglrapi").keyup(function (event) {
            var input = te("input.cardnum.onsinglrapi").node().value;
            if (input === "5") te(".cardno").style({ "background": "url(https://onesingleapi.com/OneSingleApiPlugin/master.ico) no-repeat 95.5% 45% #fff", "background-size": "40px 40px" });
            if (input === "4") te(".cardno").style({ "background": "url(https://onesingleapi.com/OneSingleApiPlugin/visa.ico) no-repeat 95.5% 45% #fff", "background-size": "40px 40px" });
            if (input.length === 0 && (event.keyCode === 8 || event.keyCode === 46)) te(".cardno").style({ "background": "url(https://onesingleapi.com/OneSingleApiPlugin/credit-card.png) no-repeat 95.5% 45% #fff", "background-size": "40px 40px" });
            if (input.length === 19) {
                input = te.replace(" ", "", input);
                var isLuhn = isValidLuhn(input);
                console.log(input);
                if (isLuhn) {
                    te(".cardno").style({ "color": "green" });
                    te(".exp-singleapi").focus();
                    formState.cardno = true;
                } else {
                    te(".cardno").style({ "color": "red" });

                    //show luhn check failed tooltip
                    ShowToToolTip('.luhn-failed')
                    //card failed end

                    if (!te(".cardno").hasClass("shake")) {
                        te(".cardno").addClass(" shake");
                    }
                }
            }
            if (event.keyCode === 8 || event.keyCode === 46) {
                te(".cardno").style({ "color": "#000000" });
                te(".cardno").removeClass(" shake");
                //clear luhn check failed tooltip
                hideToToolTip('.luhn-failed');
                //clear failed end
            }
        });

        te("input.cardnum.onsinglrapi").keypress(function (event) {
            te(".cardno.cardnum.onsinglrapi").node().value = te(".cardno.cardnum.onsinglrapi").node().value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
        });
        //end on validate card number

        //validate expire date
        te("input.exp-singleapi").keyup(function (event) {
            if (te("input.exp-singleapi").node().value.length >= 2) {
                var value = te("input.exp-singleapi").node().value;
                if (parseFloat(value) > 12) {
                    value = "0" + value;
                }
                if (te("input.exp-singleapi").node().value.indexOf(" / ") !== 2) te("input.exp-singleapi").node().value = te("input.exp-singleapi").node().value.substring(0, 2) + " / " + te("input.exp-singleapi").node().value.substring(3, te("input.exp-singleapi").node().value.length);
            }
            if (event.keyCode === 8 || event.keyCode === 46) {
                console.log("b");
                if (te("input.exp-singleapi").node().value.length <= 5) te("input.exp-singleapi").node().value = te("input.exp-singleapi").node().value.replace(" / ", "");
                te("input.exp-singleapi").style({ "color": "#000000" });
                te("input.exp-singleapi").removeClass(" shake");
                //clear luhn check failed tooltip
                hideToToolTip('.exp-check');
                //clear failed end
            }
            if (te("input.exp-singleapi").node().value.length === 7) {
                var year = te("input.exp-singleapi").node().value.substring(5, 7);
                var date = new Date;
                if (parseFloat("20" + year) < date.getFullYear()) {
                    ShowToToolTip(".exp-check");
                    te("input.exp-singleapi").style({ "color": "red" });

                    if (!te("input.exp-singleapi").hasClass("shake")) {
                        te("input.exp-singleapi").addClass(" shake");
                    }
                } else {
                    formState.expDate = true;
                    te(".cvv-singleapi").focus();
                }
            }
        });

        //end of validate expiry date
        te(".cvv-singleapi").keyup(function () {
            if (te(".cvv-singleapi").node().value.length > 2) {
                formState.cvv = true;
            }
        });

        //on submit action
        te("#submit-singleapi").click(function (event) {

            //dsp
            console.log("After card details call");
            event.preventDefault();
            if (formState.cardno && formState.cvv && formState.expDate) {
                var cardno = te.replace(" ", "", te(".cardno.cardnum.onsinglrapi").node().value);
                var expdate = te.replace(" / ", "-20", te("input.exp-singleapi").node().value);
                var cvv = te(".cvv-singleapi").node().value;
                var amount = te("input#amount-singleapi").node().value;
                 var pickUpId = te("input#pickUpId").node().value;
                var refno = te().random(12);
                if (cardno && expdate && cvv && amount && refno && oneSingleApiEncrypKey) {
                    te(".onesingleapi-callback").hide();
                    te(".single-api-container").empty();
                    te(".single-api-container").append(oneSingleApiLoader);
                    sendData(cardno, phoneNumber, purpose, expdate, cvv, amount, merchantref, oneSingleApiEncrypKey,pickUpId);
                }
            } else {
                //te(".modal-content").addClass(" shake");
            }
        });

        function isValidLuhn(number) {
            var digit, k, len1, n, ref1, sum;
            sum = 0;
            ref1 = number.split('').reverse();
            for (n = k = 0, len1 = ref1.length; k < len1; n = ++k) {
                digit = ref1[n];
                digit = +digit;
                if (n % 2) {
                    digit *= 2;
                    if (digit < 10) {
                        sum += digit;
                    } else {
                        sum += digit - 9;
                    }
                } else {
                    sum += digit;
                }
            }
            return sum % 10 === 0;
        }

        function clearModal() {
            setTimeout(function () {
                te(".single-api-container").empty();
                onsingleApiJSFile();
            }, 5000);
        }

        function clearPaymentPageModal() {
            setTimeout(function () {
              //wait for 4 seconds
            }, 4000);
        }

        function showInfoFromResponse(responseJson) {

            //console.log(responseJson);
            //console.log("responseJson");

            console.log("hhhhhhhh");
            te(".transactionInfoPreL").empty();
            postPayment(responseJson);
                te(".single-api-container").empty();
                if (responseJson.ResponseCode == "00") {
                    if (responseJson.RedirectUrl != "")
                    {
                        if (responseJson.SuccessMessage != "")
                        {
                            te(".single-api-container").append(oneSingleApiOk);
                            te(".transactionInfo").append("<p>" + responseJson.SuccessMessage + "</p>");
                            clearPaymentPageModal();
                            window.location = responseJson.RedirectUrl
                        }
                        else
                        {
                            te(".single-api-container").append(oneSingleApiOk);
                            te(".transactionInfo").append("<p>" + responseJson.Message + "</p>");
                            clearPaymentPageModal();
                            window.location = responseJson.RedirectUrl
                        }
                    }
                    else if (responseJson.SuccessMessage != "")
                    {
                        te(".single-api-container").append(oneSingleApiOk);
                        te(".transactionInfo").append("<p>" + responseJson.SuccessMessage + "</p>");
                        clearModal();
                    }
                    te(".single-api-container").append(oneSingleApiOk);
                    te(".transactionInfo").append("<p>" + responseJson.Message + "</p>");
                    clearModal();
                }
                else {
                    te(".single-api-container").append(oneSingleApiFailed);
                    te(".transactionInfo").append("<p>" + responseJson.Message + "</p>");
                    clearModal();
                }
          
        }

        function postPayment(responseJson) {
            var url = responseJson.URL, postFields;
            if (te("#airtime_phoneno").node()) {
                var phoneNumber = te("#airtime_phoneno").node().value;
                postFields = "ResponseCode=" + responseJson.ResponseCode + "&TransactionNo=" + responseJson.TransactionNo + "&Amount=" + responseJson.Amount + "&Message=" + responseJson.Message + "&TokenID=" + responseJson.TokenID + "&MaskedCardNo=" + responseJson.MaskedCardNo + "&PhoneNo=" + phoneNumber + "&OnsRef=" + responseJson.OnsRef;
            } else {
                postFields = "ResponseCode=" + responseJson.ResponseCode + "&TransactionNo=" + responseJson.TransactionNo + "&Amount=" + responseJson.Amount + "&Message=" + responseJson.Message + "&TokenID=" + responseJson.TokenID + "&MaskedCardNo=" + responseJson.MaskedCardNo + "&MerchantRef=" + responseJson.MerchantRef;
            }

            te.HTTPRequest("POST", url, postFields);
        }

        function openPopupWindow(response, auth3D, pareq, closeActionFrom) {
            auth3D.document.write("<iframe style='width:400;height:400;border:none;' src='" + response + "'></iframe><script>" +
                "setInterval(function() {" +
                "var url = 'https://www.onesingleapi.com/API/ConfirmTransactionStatus?transactionno=" + pareq + "';" +
                "http = new XMLHttpRequest();" +
                "http.open('GET', url, true);" +
                "http.onreadystatechange = function() {" +
                "if(http.readyState == 4 && http.status == 200) {" +
                "if (http.responseText){" +
                "console.log(http.responseText);" +
                "var resJson = JSON.parse(http.responseText);" +
                "if(resJson.ResponseCode !== '02') {" +
                "window.opener.postMessage(resJson, '*');" +
                "clearInterval();" +
                "}}}};" +
                "http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');" +
                "http.send(null);" +
                "}, 1000);" +
                "</script>");
            window.addEventListener("message", function (event) {
                console.log(event.data);
                window.closeActionForm = false;
                auth3D.close();
                showInfoFromResponse(event.data);
            }, false);
        }

        function sendData(cardno, phoneNumber, purpose, expdate, cvv, amount, merchantref, encryKey,pickUpId) {
            console.log("check card details call");

            var phoneNumber ='7709411542';
            console.log("cardno : "+cardno);
            console.log("phoneNumber : "+phoneNumber);
            console.log("purpose : "+purpose);
            console.log("expdate : "+expdate);
            console.log("cvv : "+cvv);
            console.log("amount : "+amount);
            console.log("merchantref : "+merchantref);
            console.log("encryKey : "+encryKey);
            console.log("pickUpId : "+pickUpId);

            

            var url = "https://onesingleapi.com/ThreeDSecure/Verification";
            //console.log(cardno, expdate, cvv, amount, refno, encryKey);
            var postFields = "CardNumber=" + cardno + "&PhoneNo=" + phoneNumber + "&Purpose=" + purpose + "&ExpiryDate=" + expdate + "&CVV=" + cvv + "&Amount=" + amount + "&VendorReferenceNumber=" + merchantref + "&EncryptionKey=" + encryKey + "&PaymentPlatform=WEB";
            te.HTTPRequest("POST", url, postFields, function (response) {
                //console.log(response);
                console.log("semd dataaaa");
                try {
                    var responseJson = JSON.parse(response);
                    console.log("responseJson.ResponseCode");
                    var redirectUrl  = '/payment/successTransaction';
                    console.log(responseJson);

                    console.log(responseJson.ResponseCode+' status : '+responseJson.Status);
                    
                    postFields +="&ResponseCode="+responseJson.ResponseCode+"&status="+responseJson.Status+'&pickUpId='+pickUpId;
                    if (responseJson.ResponseCode == "00T") {

                        te.HTTPRequest("POST", redirectUrl, postFields, function (data) {

                            console.log("data");

                            console.log(data);

                            te(".single-api-container").empty();
                            te(".single-api-container").append(oneSingleApiOk);
                            te(".transactionInfo").append("<p>" + responseJson.Status + "</p>");
                            clearModal();

                            //window.location.href = '/'; //Will take you to Google.

                            window.location="/";



                        });
                    } 
                    else if (responseJson.ResponseCode == "200") {
                        te.HTTPRequest("POST", redirectUrl, postFields, function (data) {
                            te(".single-api-container").empty();
                            te(".single-api-container").append(oneSingleApiOk);
                            te(".transactionInfo").append("<p>" + responseJson.Status + "</p>");
                            clearModal();
                        });
                    }
                    else if (responseJson.ResponseCode == "150") {
                        te.HTTPRequest("POST", redirectUrl, postFields, function (data) {
                            te(".single-api-container").empty();
                            te(".single-api-container").append(oneSingleApiFailed);
                            te(".transactionInfo").append("<p>" + responseJson.Status + "</p>");
                            clearModal();
                        });
                    }
                    else if (responseJson.ResponseCode == "09") {
                        te.HTTPRequest("POST", redirectUrl, postFields, function (data) {
                            te(".single-api-container").empty();
                            te(".single-api-container").append(oneSingleApiFailed);
                            te(".transactionInfo").append("<p>" + responseJson.Status + "</p>");
                            clearModal();
                        });
                    }
                    else if (responseJson.ResponseCode == "06") {
                        te.HTTPRequest("POST", redirectUrl, postFields, function (data) {
                            te(".single-api-container").empty();
                            te(".single-api-container").append(oneSingleApiFailed);
                            te(".transactionInfo").append("<p>" + responseJson.Status + "</p>");
                            clearModal();
                        });
                    }
                    else if (responseJson.ResponseCode == "04") {
                        te.HTTPRequest("POST", redirectUrl, postFields, function (data) {
                            te(".single-api-container").empty();
                            te(".single-api-container").append(oneSingleApiFailed);
                            te(".transactionInfo").append("<p>" + responseJson.Status + "</p>");
                            clearModal();
                        });
                    }
                    else if (responseJson.ResponseCode == "07") {
                        te.HTTPRequest("POST", redirectUrl, postFields, function (data) {
                 
                            te(".single-api-container").empty();
                            te(".single-api-container").append(oneSingleApiFailed);
                            te(".transactionInfo").append("<p>" + responseJson.Status + "</p>");
                            clearModal();
                        })
                    }
                    else if (responseJson.ResponseCode == "08") {
                        te.HTTPRequest("POST", redirectUrl, postFields, function (data) {
                        
                            te(".single-api-container").empty();
                            te(".single-api-container").append(oneSingleApiFailed);
                            te(".transactionInfo").append("<p>" + responseJson.Status + "</p>");
                            clearModal();
                        });
                    }
                    else if (responseJson.ResponseCode == "02") {
                        te.HTTPRequest("POST", redirectUrl, postFields, function (data) {
                            te(".single-api-container").empty();
                            te(".single-api-container").append(oneSingleApiFailed);
                            te(".transactionInfo").append("<p>" + responseJson.Status + "</p>");
                            clearModal();
                        });
                    }
                    else if (responseJson.ResponseCode == "00")
                    {
                        te(".transactionInfoPreL").append("<p>Please click the button below to verify card with your bank</p><button style='background-color:#4CAF50;border:none;color:white;padding:10px 12px;text-align:center;text-decoration:none;display:inline-block;font-size:16px;margin:4px 2px;cursor: pointer;'>Click Here To Verify</button>");
                        te(".transactionInfoPreL").click(function () {
                            pareq = responseJson.TransactionNo;
                            auth3D = window.open("", "_blank", "width=400,height=400,toolbar=no,menubar=no,location=no");
                            openPopupWindow(responseJson.URL, auth3D, pareq, closeActionForm);

                            auth3D.onbeforeunload = function (event) {
                                if (window.closeActionForm) {
                                    auth3D.onunload = function (event) {
                                        showInfoFromResponse({ ResponseCode: "99", Message: "Transaction Canceled" });
                                    }
                                }
                            }
                        });
                    }
              
                } catch (err) {

                }
            });
        }
    }
    onsingleApiJSFile();
}());
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}