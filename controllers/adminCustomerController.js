var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var moment = require('moment');
var request = require('request');
var sendmail = require('sendmail')();
var nodemailer = require('nodemailer');

var adminCustomerRoute = require('express').Router();

var Users = require('./../models').Users;

var CustomerPickUpRequests = require('./../models').CustomerPickUpRequests;
var Vehicles = require('./../models').Vehicles;
var DriverLocations = require('./../models').DriverLocations;

var transporter = nodemailer.createTransport({
        host: 'server207.web-hosting.com',
        port: 465,
        secure: true,
        auth: {
            user: 'info@pooplogg.com',
            pass: '#pooplogg2020'
        }
      });

adminCustomerRoute.get('/', function (req, res) {

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    console.log("customerPickUpRequest");

    var currentDateTime = moment().format();   
    CustomerPickUpRequests.findAll({'where':{'status':{$ne:'accepted'}}})

    //Post.findAll({ where: {deletedAt: {$ne: null}, topicId: req.params.id} })

    //CustomerPickUpRequests.findAll({'where':{'status':''}})
    .then(function(pickUpRequestData)
    { 
        var i =1;
        var totPickUpRequestDataLength = pickUpRequestData.length;

        if(pickUpRequestData.length){

            Users.findAll({'where':{"userType":3}})
                .then(function(partnersData)
                { 

                pickUpRequestData.map(function(elm){

                    Users.find({'where':{"id":elm.customerId}})
                    .then(function(usersData)
                    { 
                        elm.customerprofileImage = usersData.profileImage;
                        elm.customerName = usersData.name;
                        elm.customerMobile = usersData.mobile;
                        if(totPickUpRequestDataLength==i)
                        {
                            res.render('customer/customerPickUpRequest',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:pickUpRequestData,partnersData:partnersData});
                        }
                        i++;

                    })
                    .catch(function(err)
                    {
                        var pickUpRequestData=[];
                        var partnersData =[];
                        console.log(err);
                        res.render('customer/customerPickUpRequest',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:pickUpRequestData,partnersData:partnersData});
                    })//usersData


                })//map


            })
            .catch(function(err)
            {
                var pickUpRequestData=[];
                var partnersData = [];
                console.log(err);
                res.render('customer/customerPickUpRequest',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:pickUpRequestData,partnersData:partnersData});
            })//usersData



        }else{
            //error

            var pickUpRequestData=[];
                var partnersData = [];
                console.log(err);
                res.render('customer/customerPickUpRequest',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:pickUpRequestData,partnersData:partnersData});
        }

        

    })
    .catch(function(err)
    {
        var pickUpRequestData=[];
        var partnersData =[];
        console.log(err);
        res.render('customer/customerPickUpRequest',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:pickUpRequestData,partnersData:partnersData});
    })
});


adminCustomerRoute.get('/assignDriver/:pickUpId', function (req, res) {

    console.log("assignDriver11");

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var pickUpId = req.params.pickUpId;

    CustomerPickUpRequests.find({'where':{"id":pickUpId}})
        .then(function(pickUpData)
        { 

        var customerId = pickUpData.customerId;
        Users.find({'where':{"id":customerId}})
            .then(function(customerData)
            { 

            console.log("customerData "+customerData);

            Users.findAll({'where':{"userType":3,status:'Y'}})
                .then(function(partnersData)
                { 
                    res.render('customer/customerDriverAssign',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,customerData:customerData,partnersData:partnersData,pickUpId:pickUpId});
                })
                .catch(function(err)
                {
                    var partnersData =[];
                    var customerData = [];
                    console.log(err);
                    res.render('customer/customerDriverAssign',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,partnersData:partnersData,customerData:customerData,pickUpId:pickUpId});
                })//partnersData
        })
        .catch(function(err)
        {
            var partnersData =[];
            var customerData = [];
            console.log(err);
            res.render('customer/customerDriverAssign',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,partnersData:partnersData,customerData:customerData,pickUpId:pickUpId});
        })//customerData

    })
    .catch(function(err)
    {
        var partnersData =[];
        var customerData = [];
        console.log(err);
        res.render('customer/customerDriverAssign',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,partnersData:partnersData,customerData:customerData,pickUpId:pickUpId});
    })//customerData

});
//assignDriver

adminCustomerRoute.get('/getDriverData/:partnerId', function (req, res) {

    console.log("driverData");
    var partnerId = req.params.partnerId;

    console.log(partnerId);

    Users.findAll({'where':{"userType":4,"addedBy":partnerId,status:'Y'}})
        .then(function(partnersData)
        { 
            res.send(partnersData);
        })
        .catch(function(err)
        {
            var partnersData=[];
            res.send(partnersData);
        })

});

adminCustomerRoute.post('/assignDriver', function (req, res) {
console.log(req.body);
    console.log("post assignDriver");

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var pickUpId = req.body.pickUpId;

    var partnerId = req.body.partnerId;
    var driverId = req.body.driverId;
    var amount = req.body.amount;

    console.log(partnerId);
    console.log(pickUpId);

    CustomerPickUpRequests.find({'where':{"id":pickUpId}})
        .then(function(pickUpData)
        { 
            var customerId = pickUpData.customerId;

        Users.find({'where':{"id":customerId}})
        .then(function(customerData)
        { 

           var custMobile = customerData.mobile;
           var custEmail = customerData.email;
           var custName = customerData.name;

            DriverLocations.update({'isAssign':'Y'},{'where':{'driverId':driverId}})
                .then(function(DriverLocationsData){
                CustomerPickUpRequests.update({'partnerId':partnerId,"driverId":driverId,"amount":amount,"status":"Assign"},{'where':{'id':pickUpId}})
                    .then(function(data){

                        var mailContent ='Hi '+custName+',Your request has been accepted.Please complete the payment procedure.';

                        request({
                              url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLog&messagetext='+mailContent+'.&flash=0&recipients='+custMobile+'',
                              method: 'GET',
                          }, function(error, response, body){
                              if(error) {
                                 req.flash('success', 'Driver Assign failed.');
                                 res.redirect("/customerPickUpRequest");
                              } else {

                                var mailOptions = {
                                  from: 'info@pooplogg.com',
                                  to: custEmail,
                                  subject: 'PoopLogg:Request Accept By Admin',
                                  html: mailContent
                                };

                                transporter.sendMail(mailOptions, function(error, info){
                                      if (error) {
                                                 req.flash('success', 'Driver Assign Successfully.');
                                                 res.redirect("/customerPickUpRequest");
                                      } else {
                                        req.flash('success', 'Driver Assign Successfully.');
                                        res.redirect("/customerPickUpRequest");
                                      }
                                    });

                                // if(1==2){
                                //     sendmail({
                                //             from: 'patil.dilipmg@gmail.com',
                                //             to: custEmail,
                                //             subject: 'PoopLog:Request Accept By Admin',
                                //             html: mailContent,
                                //           }, function(err, reply) {
                                //             if(err){
                                //            // req.flash('error', 'User registerd Successfully.');
                                //             req.flash('success', 'Driver Assign failed.');
                                //             res.redirect("/customerPickUpRequest");
                                //             }
                                //             req.flash('success', 'Driver Assign Successfully.');
                                //             res.redirect("/customerPickUpRequest");

                                //         });

                                // }else{
                                //      req.flash('success', 'Driver Assign Successfully.');
                                //      res.redirect("/customerPickUpRequest");
                                // }

                                 

                              }
                          });
                        //req.flash('success', 'Driver Assign Successfully.');
                        //res.redirect("/customerPickUpRequest");
                })
                .catch(function(err)
                {
                    req.flash('error', err);
                    res.redirect("/customerPickUpRequest");
                })
            })
            .catch(function(err)
            {
                req.flash('error', err);
                res.redirect("/customerPickUpRequest");
            })
        })
        .catch(function(err)
        {
            req.flash('error', err);
            res.redirect("/customerPickUpRequest");
        })
    })
    .catch(function(err)
    {
        req.flash('error', err);
        res.redirect("/customerPickUpRequest");
    })

});



adminCustomerRoute.get('/accepted', function (req, res) {

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    console.log("customerPickUpRequestAccepted");

    var currentDateTime = moment().format();   
    CustomerPickUpRequests.findAll({'where':{"status":'accepted'}})
    .then(function(pickUpRequestData)
    { 
        var i =1;
        var totPickUpRequestDataLength = pickUpRequestData.length;

        if(pickUpRequestData.length){

            Users.findAll({'where':{"userType":3}})
                .then(function(partnersData)
                { 

                pickUpRequestData.map(function(elm){

                    Users.find({'where':{"id":elm.customerId}})
                    .then(function(usersData)
                    { 

                        Users.find({'where':{"id":elm.driverId}})
                        .then(function(driverData)
                        { 
                            elm.customerprofileImage = usersData.profileImage;
                            elm.customerName = usersData.name;
                            elm.customerMobile = usersData.mobile;

                            elm.driverName = driverData.name;
                            elm.driverMobile = driverData.mobile;
                            if(totPickUpRequestDataLength==i)
                            {
                                res.render('customer/customerPickUpRequestAccepted',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:pickUpRequestData,partnersData:partnersData});
                            }
                            i++;

                        })
                        .catch(function(err)
                        {
                            var pickUpRequestData=[];
                            var partnersData =[];
                            console.log(err);
                            res.render('customer/customerPickUpRequestAccepted',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:pickUpRequestData,partnersData:partnersData});
                        })//usersData

                    })
                    .catch(function(err)
                    {
                        var pickUpRequestData=[];
                        var partnersData =[];
                        console.log(err);
                        res.render('customer/customerPickUpRequestAccepted',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:pickUpRequestData,partnersData:partnersData});
                    })//usersData


                })//map


            })
            .catch(function(err)
            {
                var pickUpRequestData=[];
                var partnersData = [];
                console.log(err);
                res.render('customer/customerPickUpRequestAccepted',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:pickUpRequestData,partnersData:partnersData});
            })//usersData



        }else{
            //error
            var pickUpRequestData=[];
                var partnersData = [];
                res.render('customer/customerPickUpRequestAccepted',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:pickUpRequestData,partnersData:partnersData});
        }
        

    })
    .catch(function(err)
    {
        var pickUpRequestData=[];
        var partnersData =[];
        console.log(err);
        res.render('customer/customerPickUpRequestAccepted',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:pickUpRequestData,partnersData:partnersData});
    })
});




module.exports = adminCustomerRoute;
