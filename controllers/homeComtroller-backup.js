var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var jwt    = require('jsonwebtoken');

var multer = require('multer');

var upload = multer({ dest: '/tmp' })

var homeRoute = require('express').Router();

var Users = require('./../models').Users;


homeRoute.get('/',function(req,res){
    ssn = req.session;

    if(req.user){

    var username = req.user.username;

    var accesstoken = generateToken(username);
      ssn.accesstoken=req.user.accesstoken;
      ssn.username=req.user.username;
      ssn.userType=req.user.userType;
      ssn.userId=req.user.id;
      ssn.profileImage=req.user.profileImage;

      var username=ssn.username;
      var userType=ssn.userType;
      var userId=ssn.userId;
      var ssnprofileImage=ssn.profileImage;

      if(userType==1){

        if(accesstoken){
          //req.user.accesstoken;
          var userData = req.user;
          var dataArr = [];
          Users.update({'accesstoken':accesstoken},{'where':{'username':username}})
              .then(function(data){

                var driverType = 4;
                var spoterType= 5;
                var driverLength=1;
                 //Users.findAll({'where':{"userType":driverType,"status":'Y'}})
                 Users.findAll({'where':{"userType":driverType}})
                      .then(function(result)
                      {
                        
                        var totalLength = result.length;

                        if(totalLength){

                          result.map(function(elm){
                            var driverObj={};

                        Users.find({'where':{"userType":spoterType,"status":'Y',"vehicleNumber":elm.vehicleNumber}})
                          .then(function(spoterData)
                          {
                            var spoterObj={};
                            if(spoterData){
                                spoterObj.name=spoterData.name;
                                spoterObj.email=spoterData.email;
                                spoterObj.mobile=spoterData.mobile;
                                spoterObj.profileImage=spoterData.profileImage;
                                spoterObj.licenceNumber=spoterData.licenceNumber;
                                spoterObj.vehicleMakeModel=spoterData.vehicleMakeModel;
                                spoterObj.vehicleNumber=spoterData.vehicleNumber;

                            }

                            //driver obj
                            driverObj.name=elm.name;
                            driverObj.email=elm.email;
                            driverObj.mobile=elm.mobile;
                            driverObj.licenceNumber=elm.licenceNumber;
                            driverObj.vehicleMakeModel=elm.vehicleMakeModel;
                            driverObj.profileImage=elm.profileImage;
                            driverObj.vehicleNumber=elm.vehicleNumber;

                            dataArr.push({"driverObj":driverObj,"spoterObj":spoterObj});
                            if(driverLength==totalLength){

                                console.log(dataArr);
                                res.render('user/index',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:dataArr});
                            }
                            driverLength++;

                        })//spoter
                            .catch(function(err)
                            {
                                var result=[];
                                req.flash('error',err);
                                res.render('user/index',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});
                            })

                        })//map
                        }else{//if drivers not available

                              var result=[];
                               // req.flash('error',err);
                                res.render('user/index',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});

                        }                        
                        

                      })
                    .catch(function(err)
                    {
                        var result=[];
                        req.flash('error',err);
                        res.render('user/index',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});
                    })
                   //res.render('user/index',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
              })
              .catch(function(err){
                req.flash('error','User authentication failed.');
                res.redirect('/');
              })
          }else{
                req.flash('error','User accesstoken not found.');
                res.redirect('/');
          }

      
      }else{


        var result=[];

        if(accesstoken){
          //req.user.accesstoken;
          var userData = req.user;
          Users.update({'accesstoken':accesstoken},{'where':{'username':username}})
              .then(function(data){
                   res.render('user/index',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});
              })
              .catch(function(err){
                req.flash('error','User authentication failed.');
                res.redirect('/');
              })
          }else{
                req.flash('error','User accesstoken not found.');
                res.redirect('/');
          }

      }

        


    }else{//req.user
            req.flash('error','Authentication Failed.');
            res.redirect('/');
    }
});



function generateToken(username){
   var payload = {
      user: username 
    };
    var accesstoken = jwt.sign(payload, 'abcd', {
     // expiresInMinutes: 1440 // expires in 24 hours
    });
    return accesstoken;
}


module.exports = homeRoute;
