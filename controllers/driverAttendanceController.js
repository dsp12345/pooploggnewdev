var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var moment = require('moment');


var driverAttendanceRoute = require('express').Router();

var Users = require('./../models').Users;
var Vehicles = require('./../models').Vehicles;
var DrverAttendance = require('./../models').DrverAttendance;

driverAttendanceRoute.get('/', function (req, res) {
    //console.log(req.userId);

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    console.log("driverAttendanceRoute get");
    
});

driverAttendanceRoute.post('/attendanceLogin', function (req, res) {

    var currentDateTime = moment().format();
    var accesstoken = req.body.accesstoken;
	var fromcurrentDate = moment().format("YYYY-MM-DD 00:00:01");
	var tocurrentDate = moment().format("YYYY-MM-DD 23:59:00");

     Users.find({'where':{"accesstoken":accesstoken}})
        .then(function(user)
        { 
        	if(user){

        		console.log("aaaaaa "+user.id);

        		DrverAttendance.find({'where':{"driverId":user.id,"fromTime":{
			      $gte: fromcurrentDate,$lte: tocurrentDate
			    }}})
			    .then(function(allreadyAttendanceLogin)
			    { 

			    	if(allreadyAttendanceLogin){
			    		return res.status(400).send({status:'error','data':{'message':'Your allready attendance login.',"user":''}});
			    	}

		            Users.find({'where':{"vehicleNumber":user.vehicleNumber,"userType":'5'}})
			        .then(function(spoter)
			        {

			        	var  params = {
			                "driverId":user.id,
			                "spoterId":spoter.id,
			                "vehicleNumber":user.vehicleNumber,
			                "fromTime":currentDateTime,
			                "toTime":'',
			                "totalHrs":'',
			                "onDuty":'Y',
			            };

			            console.log("params");
			            console.log(params);

				     DrverAttendance.create(params)
				        .then(function(result){

				            //req.flash('success', 'Spoter added Successfully.');
				            return res.status(200).send({status:'success','data':{'message':'You are logged in Successfully.',"user":result}});
				        })
				        .catch(function(err)
				        {
				        	var result=[];
				            //req.flash('success', 'Driver Attendance Failed.');
				            return res.status(200).send({status:'success','data':{'message':'Your login is Failed.Please try again',"user":result}});
				        })

			     	 })
			        .catch(function(err)
			        {
			        	var result=[];
			            //req.flash('success', 'Driver Attendance Failed.');
			            return res.status(200).send({status:'error','data':{'message':'Your login is Failed.Please try again',"user":result}});
			        })

		     //return res.status(200).send({status:'error','data':{'message':' AccessToken',"user":user}});
		    	 })//check allready login
		        .catch(function(err)
		        {
		        	var result=[];
		            //req.flash('success', 'Driver Attendance Failed.');
		            return res.status(200).send({status:'error','data':{'message':'Your login is Failed.Please try again',"user":result}});
		        })
		    }else{
		    	var result=[];
		    	return res.status(400).send({status:'error','data':{'message':'Invalid AccessToken',"user":result}});

		    }

	     })
        .catch(function(err)
        {
        	var result=[];
            //req.flash('success', 'Driver Attendance Failed.');
            return res.status(200).send({status:'error','data':{'message':'Your login is Failed.Please try again',"user":result}});
        })
    
});


driverAttendanceRoute.post('/attendanceLogOut', function (req, res) {
	
    var currentDateTime = moment().format();
    var accesstoken = req.body.accesstoken;
	var fromcurrentDate = moment().format("YYYY-MM-DD 00:00:01");
	var tocurrentDate = moment().format("YYYY-MM-DD 23:59:00");

     Users.find({'where':{"accesstoken":accesstoken}})
        .then(function(user)
        { 

        	if(user){

        		DrverAttendance.find({'where':{"fromTime":{
			      $gte: fromcurrentDate,$lte: tocurrentDate
			    }},"driverId":user.id})
			    .then(function(allreadyAttendanceLogin)
			    { 
			    	if(allreadyAttendanceLogin){

			    		var fromTime = allreadyAttendanceLogin.fromTime;

			    		var checkfromTime = moment(fromTime).format("YYYY/MM/DD HH:mm:ss");
			    		var checkCurrentDateTime = moment().format("YYYY/MM/DD HH:mm:ss");

			    		var differenceTime = moment.utc(moment(checkCurrentDateTime,"YYYY-MM-DD HH:mm:ss").diff(moment(checkfromTime,"YYYY-MM-DD HH:mm:ss"))).format("HH:mm:ss")

			    		console.log("aa : "+differenceTime);
			    		DrverAttendance.update({'toTime':currentDateTime,"totalHrs":differenceTime,"onDuty":"N"},{'where':{'id':allreadyAttendanceLogin.id}})
                           .then(function(data){
			    		return res.status(400).send({status:'success','data':{'message':'Your logOut Successfully.',"user":data}});
			    		 })//check allready login
				        .catch(function(err)
				        {
				        	var result=[];
				            //req.flash('success', 'Driver Attendance Failed.');
				            return res.status(200).send({status:'error','data':{'message':'Your login is Failed.Please try again',"user":result}});
				        })
			    	}else{

			    		return res.status(400).send({status:'error','data':{'message':'Today you are not  attendance login.',"user":''}});

			    	}

		           
		     //return res.status(200).send({status:'error','data':{'message':' AccessToken',"user":user}});
		    	 })//check allready login
		        .catch(function(err)
		        {
		        	var result=[];
		            //req.flash('success', 'Driver Attendance Failed.');
		            return res.status(200).send({status:'error','data':{'message':'Your login is Failed.Please try again',"user":result}});
		        })
		    }else{
		    	var result=[];
		    	return res.status(400).send({status:'error','data':{'message':'Invalid AccessToken',"user":result}});

		    }

	     })
        .catch(function(err)
        {
        	var result=[];
            //req.flash('success', 'Driver Attendance Failed.');
            return res.status(200).send({status:'error','data':{'message':'Your login is Failed.Please try again',"user":result}});
        })
    
});
module.exports = driverAttendanceRoute;
