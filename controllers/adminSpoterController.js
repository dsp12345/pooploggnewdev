var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
var fs = require("fs");
var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var sendmail = require('sendmail')();
var nodemailer = require('nodemailer');

var adminspotersRoute = require('express').Router();

var Users = require('./../models').Users;
var Vehicles = require('./../models').Vehicles;
var UpdateDriverRequests = require('./../models').UpdateDriverRequests;


adminspotersRoute.get('/requested', function (req, res) {
    console.log("spotersRoute list");

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    Users.findAll({'where':{"userType":5,"status":'N'}})
      .then(function(result)
      {
        res.render('spoter/requested_spoter_list',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('spoter/requested_spoter_list',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
        //res.render('provider/list_providers',{data:result});
    })

});


adminspotersRoute.get('/', function (req, res) {
    console.log("Approved list");

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    Users.findAll({'where':{"userType":5,"status":'Y'}})
      .then(function(result)
      {
        res.render('spoter/approved_spoter_list',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('spoter/approved_spoter_list',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
        //res.render('provider/list_providers',{data:result});
    })

});


adminspotersRoute.get('/spoterRequest/:id/:status', function(req, res) {

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var id = req.params.id;
    var status =req.params.status;

  Users.findAll({'where':{"id":id,"status":"N"}})
          .then(function(userData)
          {
            var requestedUserEmail = userData[0].email;
            Users.findAll({'where':{"userType":5,"status":"N"}})
              .then(function(result)
              {
              Users.update({'status':status},{'where':{'id':id}})
              .then(function(data){

                 Users.findAll({'where':{"userType":5,"status":"N"}})
                    .then(function(result)
                    {
                      if(status=='Y'){
                        var mailContent='Your request has been approved by admin.<br></br>Please click on this <a href="http://localhost:8002/setPassword/'+id+'">Set Password</a>.</br>To set your               password.';

                        req.flash('success','User Request Accepted.');
                      }else{
                        var mailContent='Your request has been rejected by admin.';
                        req.flash('error','User Request Rejected.');
                      }
                      res.redirect('/adminSpoter/requested');

                      // sendmail({
                      //       from: 'patil.dilipmg@gmail.com',
                      //       to: requestedUserEmail,
                      //       subject: 'Registration Confirmation',
                      //       html: mailContent,
                      //     }, function(err, reply) {
                      //       if(err){
                      //         console.log("Mail sending failed1"+err);
                      //           req.flash('error','Mail sending failed.');
                      //           res.redirect('/adminSpoter/requested');
                      //         }
                      //         console.log("Mail sending failed2"+err);
                      //       res.redirect('/adminSpoter/requested');
                      //   });
            
                      })
                    .catch(function(err)
                    {
                      var result=[];
                      req.flash('error',err);
                      res.redirect('/adminSpoter/requested');

                    })

                })
                .catch(function(err){
                
                req.flash('error',err);
                res.redirect('/adminSpoter/requested');
                })
            })
            .catch(function(err){
              var result=[];
              req.flash('error',err);
              res.redirect('/adminSpoter/requested');
            })
  })
  .catch(function(err){
    var result=[];
    req.flash('error',err);
    res.redirect('/adminSpoter/requested');
    //res.render('provider/requested_partner_list',{data:result,username:username,userType:userType,userId:userId});
  })

})
module.exports = adminspotersRoute;
