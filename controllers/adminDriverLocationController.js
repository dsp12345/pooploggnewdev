var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');

var driverRoute = require('express').Router();

var Users = require('./../models').Users;
var Vehicles = require('./../models').Vehicles;
var DriverLocations = require('./../models').DriverLocations;
var CustomerPickUpRequests = require('./../models').CustomerPickUpRequests;


driverRoute.get('/', function (req, res) {
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    if(userType==1){  

    Users.findAll({'where':{"status":"Y",userType:14}})
     .then(function(partnerData)
      {

      DriverLocations.findAll({})
        .then(function(driverLocationData)
        { 
          var i =1;
          var driverLocationDataLength = driverLocationData.length;

          if(driverLocationDataLength){

            var dataArr = [];
              driverLocationData.map(function(elm){

            var driverId = elm.driverId;

            Users.find({'where':{"id":driverId}})
            .then(function(result)
            {
                dataArr.push({'id':elm.id,'driverId':elm.driverId,'location':elm.location,'latitude':elm.latitude,
                      'longitude':elm.longitude,'isAssign':elm.isAssign,'driverName':result.name,'vehicleNumber':result.vehicleNumber,'onDuty':result.onDuty});

            if(driverLocationDataLength==i)
                {
                  var driverLocationData1 = JSON.stringify(dataArr);
                  res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:partnerData,partnerId:'',driverDataOptionArr:[],driverIdOption:''});
                }
                i++;

                })
                .catch(function(err)
                {
                  var driverLocationData1=[];
                   req.flash('error', err);
                   res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:'',driverDataOptionArr:[],driverIdOption:''});
                })
              })
            }else{
              res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:[],partnerData:partnerData,partnerId:partnerId,driverDataOptionArr:[],driverIdOption:''});
            }
    })
    .catch(function(err)
    {
      var driverLocationData1=[];
       req.flash('error', err);
       res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:'',driverDataOptionArr:[],driverIdOption:''});
    })
  })
    .catch(function(err)
    {
      var driverLocationData1=[];
       req.flash('error', err);
       res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:'',driverDataOptionArr:[],driverIdOption:''});
    })

  }else{

    var partnerId = ssn.userId;


    console.log("partnerId:::"+partnerId)
    Users.findAll({'where':{"partnerId":partnerId}})
        .then(function(resultDriverIds)
        {  

    var driverIdArr =[];
    var driverDataOptionArr = [];
    if(resultDriverIds.length){

      resultDriverIds.map(function(elm){

        driverIdArr.push(elm.id);
        driverDataOptionArr.push({'driverId':elm.id,'driverName':elm.name,'driverEmail':elm.email});

      });

    }


    console.log(driverIdArr)

    DriverLocations.findAll({'where':{driverId:{in:driverIdArr}}})

      .then(function(driverLocationData)
      { 
          var i =1;
          var driverLocationDataLength = driverLocationData.length;

          if(driverLocationDataLength){



            var dataArr = [];
          
              driverLocationData.map(function(elm){

            var driverId = elm.driverId;

            Users.find({'where':{"id":driverId}})
            .then(function(result)
            {
                dataArr.push({'id':elm.id,'driverId':elm.driverId,'location':elm.location,'latitude':elm.latitude,
                      'longitude':elm.longitude,'isAssign':elm.isAssign,'driverName':result.name,
                      'vehicleNumber':result.vehicleNumber,'driverMobile':result.mobile,'onDuty':result.onDuty});

            if(driverLocationDataLength==i)
                {
                  console.log("dataArr")
                  var driverLocationData1 = JSON.stringify(dataArr);

                  res.render('track/partnersDriverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerId:'',driverIdOption:'',driverDataOptionArr:driverDataOptionArr,driverIdOption:''});
                }
                i++;

                })
                .catch(function(err)
                {

                  console.log(err)
                   req.flash('error', err);
                   res.render('track/partnersDriverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:[],partnerData:[],partnerId:'',driverIdOption:'',driverDataOptionArr:driverDataOptionArr,driverIdOption:''});
                })
              })
            }else{

              console.log("no data found")
              res.render('track/partnersDriverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:[],partnerData:partnerData,partnerId:partnerId,driverIdOption:'',driverDataOptionArr:driverDataOptionArr,driverIdOption:''});
            }
    })
    .catch(function(err)
    {

      console.log(err)
       req.flash('error', err);
       res.render('track/partnersDriverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:[],partnerData:[],partnerId:'',driverIdOption:'',driverDataOptionArr:driverDataOptionArr,driverIdOption:''});
    })
  })

  }



});





driverRoute.get('/:partnerId', function (req, res) {
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    var partnerId = req.params.partnerId;


    Users.findAll({'where':{"status":"Y",userType:14}})
     .then(function(partnerData)
      {

        //this query not working
        Users.findAll({'where':{"partnerId":partnerId}})
        .then(function(resultDriverIds)
        {  

          var driverIdArr =[];
          var driverDataOptionArr = [];
          if(resultDriverIds.length){

            resultDriverIds.map(function(elm){

              driverIdArr.push(elm.id);

              driverDataOptionArr.push({'driverId':elm.id,'driverName':elm.name,'driverEmail':elm.email});

            });

          }

        DriverLocations.findAll({'where':{driverId:{in:driverIdArr}}})
          .then(function(driverLocationData)
          { 
            var i =1;
            var driverLocationDataLength = driverLocationData.length;

            if(driverLocationDataLength){

              var dataArr = [];
            
                driverLocationData.map(function(elm){

              var driverId = elm.driverId;

              Users.find({'where':{"id":driverId}})
              .then(function(result)
              {
                  dataArr.push({'id':elm.id,'driverId':elm.driverId,'location':elm.location,'latitude':elm.latitude,
                        'longitude':elm.longitude,'isAssign':elm.isAssign,'driverName':result.name,'vehicleNumber':result.vehicleNumber,'driverMobile':result.mobile,'onDuty':result.onDuty});

              if(driverLocationDataLength==i)
                  {
                    var driverLocationData1 = JSON.stringify(dataArr);
                    res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:partnerData,partnerId:partnerId,driverDataOptionArr:driverDataOptionArr,driverIdOption:''});
                  }
                  i++;

                  })
                  .catch(function(err)
                  {
                    var driverLocationData1=[];
                     req.flash('error', err);
                     res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[],driverIdOption:''});
                  })
                })
              }else{
                var driverLocationData1=[];
                res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:partnerData,partnerId:partnerId,driverDataOptionArr:driverDataOptionArr,driverIdOption:''});
              }
      })
      .catch(function(err)
      {
        var driverLocationData1=[];
         req.flash('error', err);
         res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[],driverIdOption:''});
      })

    })
    .catch(function(err)
    {
      var driverLocationData1=[];
       req.flash('error', err);
       res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[],driverIdOption:''});
    })
  })
    .catch(function(err)
    {
      var driverLocationData1=[];
       req.flash('error', err);
       res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[]});
    })
});




driverRoute.get('/:partnerId/:driverId', function (req, res) {
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    var partnerId = req.params.partnerId;
    var driverId = req.params.driverId;


    Users.findAll({'where':{"status":"Y",userType:14}})
     .then(function(partnerData)
      {

        //this query not working
        Users.findAll({'where':{"partnerId":partnerId}})
        .then(function(resultDriverIds)
        {  

          var driverIdArr =[];
          var driverDataOptionArr = [];
          if(resultDriverIds.length){

            resultDriverIds.map(function(elm){

              driverIdArr.push(elm.id);

              driverDataOptionArr.push({'driverId':elm.id,'driverName':elm.name,'driverEmail':elm.email});

            });

          }

        DriverLocations.findAll({'where':{driverId:driverId}})
          .then(function(driverLocationData)
          { 
            var i =1;
            var driverLocationDataLength = driverLocationData.length;

            if(driverLocationDataLength){

              var dataArr = [];
            
                driverLocationData.map(function(elm){

              var driverId = elm.driverId;

              Users.find({'where':{"id":driverId}})
              .then(function(result)
              {
                  dataArr.push({'id':elm.id,'driverId':elm.driverId,'location':elm.location,'latitude':elm.latitude,
                        'longitude':elm.longitude,'isAssign':elm.isAssign,'driverName':result.name,'vehicleNumber':result.vehicleNumber,'driverMobile':result.mobile,'onDuty':result.onDuty});

              if(driverLocationDataLength==i)
                  {
                    var driverLocationData1 = JSON.stringify(dataArr);
                    res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:partnerData,partnerId:partnerId,driverDataOptionArr:driverDataOptionArr,driverIdOption:driverId});
                  }
                  i++;

                  })
                  .catch(function(err)
                  {
                    var driverLocationData1=[];
                     req.flash('error', err);
                     res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[],driverIdOption:driverId});
                  })
                })
              }else{
                res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:[],partnerData:partnerData,partnerId:partnerId,driverDataOptionArr:driverDataOptionArr,driverIdOption:driverId});
              }
      })
      .catch(function(err)
      {
        var driverLocationData1=[];
         req.flash('error', err);
         res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[],driverIdOption:driverId});
      })

    })
    .catch(function(err)
    {
      var driverLocationData1=[];
       req.flash('error', err);
       res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[],driverIdOption:driverId});
    })
  })
    .catch(function(err)
    {
      var driverLocationData1=[];
       req.flash('error', err);
       res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[],driverIdOption:driverId});
    })
});




driverRoute.get('/driverCustomerRoute/:driverId', function (req, res) {
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var driverId = req.params.driverId;


    DriverLocations.find({'driverId':driverId})
      .then(function(driverLocationData)
      { 

        var driverLatitude = driverLocationData.latitude;
        var driverLongitude = driverLocationData.longitude;

        console.log(driverId);

          CustomerPickUpRequests.find({'driverId':driverId,'status':'Assign'})
            .then(function(driverCustomerLocationData)
            {              


              var driverLatLongObj = {'driverLatitude':driverLatitude,"driverLongitude":driverLongitude};
              var driverCustomerLocationData = JSON.stringify(driverCustomerLocationData);

              var driverLatLongObj = JSON.stringify(driverLatLongObj);

              res.render('track/driverCustomerTracking',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,
                driverCustomerLocationData:driverCustomerLocationData,driverLatLongObj:driverLatLongObj});

          })
        .catch(function(err)
        {

          console.log(err);
           // req.flash('error', err);
           // res.redirect("/customerPickUpRequest");
        })

   })
  .catch(function(err)
  {

    console.log(err);
     // req.flash('error', err);
     // res.redirect("/customerPickUpRequest");
  })
});







module.exports = driverRoute;
