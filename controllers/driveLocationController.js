var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');


var driverLocationRoute = require('express').Router();

var Users = require('./../models').Users;
var Vehicles = require('./../models').Vehicles;
var DriverLocations = require('./../models').DriverLocations;
var CustomerPickUpRequests = require('./../models').CustomerPickUpRequests;

driverLocationRoute.get('/:partnerId', function (req, res) {
    //ssn = req.session;
    //var accesstoken = ssn.accesstoken;
    var partnerId = req.params.partnerId;
    var dataArr = [];

    Users.findAll({'where':{"partnerId":partnerId}})
        .then(function(resultDriverIds)
        {  

    var driverIdArr =[];
    if(resultDriverIds.length){

      resultDriverIds.map(function(elm){

        driverIdArr.push(elm.id);

      });

    }
    DriverLocations.findAll({'where':{driverId:{in:driverIdArr}}})

      .then(function(driverLocationData)
      { 
        var i =1;
        var driverLocationDataLength = driverLocationData.length;

        if(driverLocationDataLength){
        
          driverLocationData.map(function(elm){

          var driverId = elm.driverId;

          Users.find({'where':{"id":driverId}})
          .then(function(result)
          {
              dataArr.push({'id':elm.id,'driverId':elm.driverId,'location':elm.location,'latitude':elm.latitude,
                    'longitude':elm.longitude,'isAssign':elm.isAssign,'driverName':result.name,
                    'driverMobile':result.mobile,'vehicleNumber':result.vehicleNumber,
                    'onDuty':result.onDuty,'updatedAt':result.updatedAt});

          if(driverLocationDataLength==i)
              {
                return res.status(200).send({status:'success','data':{'message':'success.',"data":dataArr}});

                //res.send(dataArr);
                //var driverLocationData1 = JSON.stringify(dataArr);
                //res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverLocationData:driverLocationData1});
              }
              i++;

              })
              .catch(function(err)
              {

                console.log(err);
                return res.status(401).send({status:'error','data':{'message':'Driver location data could not be fetched.',"data":[]}});
                 //req.flash('error', err);
                 //res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverLocationData:driverLocationData1});
              })
            })
          }else{
            return res.status(401).send({status:'error','data':{'message':'Driver location data not found.',"data":[]}});
            //res.render('track/driverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverLocationData:driverLocationData1});
          }


      })
    .catch(function(err)
    {

      console.log("error");
      console.log(err);
      return res.status(401).send({status:'error','data':{'message':'Driver location data could not be fetched.',"data":[]}});
      
    })

          
  })
  .catch(function(err)
  {

    console.log("error");
    console.log(err);
    return res.status(401).send({status:'error','data':{'message':'Driver location data could not be fetched.',"data":[]}});

  })
});


module.exports = driverLocationRoute;
