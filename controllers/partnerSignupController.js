

var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
var partnerSignupRoute = require('express').Router();

var fs = require("fs");
var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var sendmail = require('sendmail')();
var nodemailer = require('nodemailer');
var bulk = require('node-bulk-sms');
var request = require('request');
var randomstring = require("randomstring");

var Users = require('../models').Users;
console.log("databaseee");
console.log(config.dbname);



var transporter = nodemailer.createTransport({
        host: 'server207.web-hosting.com',
        port: 465,
        secure: true,
        auth: {
            user: 'info@pooplogg.com',
            pass: '#pooplogg2020'
        }
      });

partnerSignupRoute.get('/', function (req, res) {

    console.log("Add Partner get");

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

      res.render('partner/partner',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});

})

var cpUpload = upload.fields([{ name: 'certificateOfIncorporation', maxCount: 1 }, { name: 'proofOfOwnership', maxCount: 1 }])


partnerSignupRoute.post('/', cpUpload, function (req, res, next) {

    console.log("Partner added");



    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var today = new Date();
    var email = req.body.email;
    var userType =14;
    var profileImage = 'default';
    var partnerType=req.body.partnerType;
    var certificateOfIncorporation = req.files['certificateOfIncorporation'][0].originalname;
    var proofOfOwnership = req.files['proofOfOwnership'][0].originalname;
    var name = req.body.name;
    var mobile = req.body.mobile;

    var randomPassword = randomstring.generate({
                          length: 6,
                          charset: 'alphabetic'
                        });

    var password = randomPassword;

    if(partnerType=='Corporate'){

        var  params = {
                "username":email,
                "email":req.body.email,
                "name":req.body.name,
                "directors":req.body.directors,
                "certificateOfIncorporation":certificateOfIncorporation,
                "proofOfOwnership":proofOfOwnership,
                "address":req.body.address,
                "mobile":req.body.mobile,
                "password":password,
                "userType":userType,
                "profileImage":profileImage,
                "partnerType":partnerType,
                "status":'Y',
                "addedBy":userId,
                "createdAt":today,
                "updatedAt":today
            };

    }else{

        var  params = {
                "username":email,
                "email":req.body.email,
                "name":req.body.name,
                "directors":req.body.directors,
                "idProof":certificateOfIncorporation,
                "proofOfOwnership":proofOfOwnership,
                "address":req.body.address,
                "mobile":req.body.mobile,
                "password":password,
                "userType":userType,
                "profileImage":profileImage,
                "partnerType":partnerType,
                "status":'Y',
                "addedBy":userId,
                "createdAt":today,
                "updatedAt":today
            };

    }
    
    //Users.findAll({'where':{"username":email}})
    Users.findAll({'where':{$or:[{"mobile":mobile},{"username":email}]}})
    .then(function(result)
    {       
        if(result.length){
            req.flash('error','Email Already Exist.');
            res.redirect('/partner');            //return res.render('register/register');

            
        }else{
            var filesArray=[];

                filesArray.push(req.files['certificateOfIncorporation'][0],req.files['proofOfOwnership'][0]);

                async.each(filesArray,function(file,eachcallback){
                    async.waterfall([
                    function (callback) {
                      fs.readFile(file.path, (err, data) => {
                        if (err) {
                          console.log("err ocurred", err);
                          req.flash('error', err);
                          res.redirect('/partner');                          }
                        else {
                          callback(null,data);
                        }
                        });
                    },
                    function (data, callback) {
                    
                    if(partnerType=='Corporate'){
                        if(file.fieldname=='certificateOfIncorporation'){
                            var targetPath = path.resolve('./public/corporatePartnerDoc/certificateOfIncorporation/'+file.originalname);
                        }else{
                            var targetPath = path.resolve('./public/corporatePartnerDoc/proofOfOwnership/'+file.originalname);

                        }
                    }else{

                        if(file.fieldname=='certificateOfIncorporation'){
                            var targetPath = path.resolve('./public/indivisualPartnerDoc/idProof/'+file.originalname);
                        }else{
                            var targetPath = path.resolve('./public/indivisualPartnerDoc/proofOfOwnership/'+file.originalname);

                        }
                    }

                    fs.writeFile(targetPath, data, (err) => {
                      if (err) {

                        console.log(err);
                        console.log("!!!!!!!!!!!!!!!");
                            req.flash('error', err);
                            res.redirect('/partner');
                      }
                      else {
                      callback(null, 'done');
                      }
                      });
                    }
                    ], function (err, result) {
                      // result now equals 'done'
                      //pass final callback to async each to move on to next file
                      eachcallback();
                    });
                    },function(err){
                      if(err){

                        console.log(err);
                        console.log("111111111111111111");
                          req.flash('error', err);
                          res.redirect('/partner');
                      }
                      else{

                        Users.create(params)
                            .then(function(result){

                                // if(1==1){
                                //     var mailContent='Thank you for showing interest for being a partner with pooplog.We are commited to serving our customers in the best possible way and follow a stringent registration progress while on boarding partners and their drivers.Please follow the steps given below and also ensure you have a credit card ready to complete registration and payment of security deposit.<br></br>Please pay your deposite click on this <a href="http://admin.pooplogg.com/payment/'+email+'">Payment</a>.</br>';
                                // }else{
                                //     var mailContent='Thank you for showing interest for being a partner with pooplog.We are commited to serving our customers in the best possible way and follow a stringent registration progress while on boarding partners and their drivers.Please follow the steps given below and also ensure you have a credit card ready to complete registration and payment of security deposit.<br></br>Please pay your deposite click on this <a href="http://localhost:8002/payment/'+email+'">Payment</a>.</br>';
                                // }

                                var mailContent="Welcome "+name+", <br></br>Following Your Login Credentials.<br></br>UserName : "+email+"<br></br> Password : "+password;
                                var smsContent='Welcome '+name+',Following Your Login Credentials. UserName : '+email+','+' Password : '+password;

                                var mailOptions = {
                                  from: 'info@pooplogg.com',
                                  to: email,
                                  subject: 'PoopLogg:Account Created',
                                  html: mailContent
                                };

                                request({
                                    url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLog&messagetext='+smsContent+'.&flash=0&recipients='+mobile+'',
                                    method: 'GET',
                                }, function(error, response, body){
                                    if(error) {
                                       req.flash('success', 'Partner Created Successfully.');
                                        res.redirect('/partner');

                                    } else {

                                        transporter.sendMail(mailOptions, function(error, info){
                                              if (error) {
                                                        req.flash('success','Partner Created Successfully.');
                                                        res.redirect('/partner');
                                              } else {
                                                req.flash('success','Partner Created Successfully.');
                                                res.redirect('/partner');
                                              }
                                            });
                                    }
                                });
                               
                            })
                            .catch(function(err)
                            {
                                console.log(err);
                                req.flash('error', 'Partner Could Not Be Created.');
                                res.redirect('/partner');
                            })
                        }
                      });
            }

    })
    .catch(function(err)
    {
        console.log(err);
                        console.log("222222222222222")
        req.flash('error', err);
        res.redirect('/partner');
    })

          
});


partnerSignupRoute.get('/partnerList', function (req, res) {

    console.log("Add Partner get");

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;
   
    Users.findAll({'where':{"userType":14,"status":'Y'}})
      .then(function(result)
      {
        res.render('partner/partner_list',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('partner/partner_list',{data:[],username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
        //res.render('provider/list_providers',{data:result});
    })

})




module.exports = partnerSignupRoute;
