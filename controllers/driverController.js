var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');

var nodemailer = require('nodemailer');
var bulk = require('node-bulk-sms');
var request = require('request');
var randomstring = require("randomstring");

var driverRoute = require('express').Router();

var Users = require('./../models').Users;
var Vehicles = require('./../models').Vehicles;
var UpdateDriverRequests = require('./../models').UpdateDriverRequests;
var Areas = require('./../models').Areas;


var transporter = nodemailer.createTransport({
        host: 'server207.web-hosting.com',
        port: 465,
        secure: true,
        auth: {
            user: 'info@pooplogg.com',
            pass: '#pooplogg2020'
        }
      });

driverRoute.get('/', function (req, res) {

  console.log("dilip patil driver");
    //console.log(req.userId);

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

   Areas.findAll({'where':{"status":1}})
      .then(function(areaData)
      {

        Users.findAll({'where':{"status":"Y",userType:14}})
        .then(function(partnerData)
        { 

          res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,areaData:areaData,partnerData:partnerData,menuArr:menuArr});
        })
        .catch(function(err)
        {
            var vehiclesData=[];
            req.flash('error', err);
            return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,areaData:areaData,partnerData:partnerData,menuArr:menuArr});
        })

    })
    .catch(function(err)
    {
        var vehiclesData=[];
        req.flash('error', err);
        return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,areaData:areaData,menuArr:menuArr});
    })
});


driverRoute.get('/driverList', function (req, res) {

  console.log("dilip patil driver");
    //console.log(req.userId);

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    Users.findAll({'where':{"status":"Y",userType:7}})
    .then(function(driversData)
    { 

      res.render('driver/list_drivers',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:driversData,menuArr:menuArr});
    })
    .catch(function(err)
    {
        var driversData=[];
        req.flash('error', err);
        return res.render('driver/list_drivers',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,data:driversData,menuArr:menuArr});
    })
});

var cpUpload = upload.fields([{ name: 'driverLicence', maxCount: 1 }, { name: 'lasdriLicense', maxCount: 1 }, { name: 'profileImage', maxCount: 1 }])

driverRoute.post('/',cpUpload, function (req, res) {
    console.log("post driver");
    var today = new Date();
    var email = req.body.email;
    var partnerId = req.body.partnerId;
    //var profileImage =req.file.filename;
    var addUserType = 7;
    console.log("profileImage");
    var userId = req.userId;

    var driverLicence = req.files['driverLicence'][0].originalname;
    var lasdriLicense = req.files['lasdriLicense'][0].originalname;
    var profileImage = req.files['profileImage'][0].originalname;

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var randomPassword = randomstring.generate({
                          length: 6,
                          charset: 'alphabetic'
                        });

    var password = randomPassword;
    var mobile = req.body.mobile;

    var  params = {
                "username":req.body.email,
                "email":req.body.email,
                "name":req.body.name,
                "mobile":req.body.mobile,
                "password":password,
                "userType":addUserType,
                "areaCode":req.body.areaCode,
                "vehicleNumber":req.body.vehicleNumber,
                "driverLicence":driverLicence,
                "lasdriLicense":lasdriLicense,
                "profileImage":profileImage,
                "addedBy":userId,
                "partnerId":partnerId,
                "createdAt":today,
                "updatedAt":today,
                "status":"Y"
            };

    //Users.findAll({'where':{"username":email}})
    Users.findAll({'where':{$or:[{"mobile":mobile},{"username":email}]}})
    .then(function(result)
    {       
        if(result.length){
            req.flash('error','Username allready exist.');
            res.redirect('/driver');
            //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});

        }else{

            var filesArray=[];

                filesArray.push(req.files['driverLicence'][0],req.files['lasdriLicense'][0],req.files['profileImage'][0]);


                async.each(filesArray,function(file,eachcallback){
                    async.waterfall([
                    function (callback) {
                      fs.readFile(file.path, (err, data) => {
                        if (err) {
                          console.log("err ocurred", err);
                          req.flash('error', err);
                          res.redirect('/driver');
                          //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                          }
                        else {
                          callback(null,data);
                        }
                        });
                    },
                    function (data, callback) {
                    
                        if(file.fieldname=='driverLicence'){
                            var targetPath = path.resolve('./public/driverLicence/'+file.originalname);
                        }else if(file.fieldname=='lasdriLicense'){
                            var targetPath = path.resolve('./public/lasdriLicense/'+file.originalname);

                        }else{
                            var targetPath = path.resolve('./public/user_images/'+file.originalname);

                        }
                    

                    fs.writeFile(targetPath, data, (err) => {
                      if (err) {
                        console.log("eee1111");
                        console.log(err);
                            req.flash('error',err);
                            res.redirect('/driver');
                            //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                      }
                      else {
                      callback(null, 'done');
                      }
                      });
                    }
                    ], function (err, result) {
                      // result now equals 'done'
                      //pass final callback to async each to move on to next file
                      eachcallback();
                    });
                    },function(err){
                      if(err){
                         console.log("eee2222222");
                        console.log(err);
                          req.flash('error',err);
                          res.redirect('/driver');
                           //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                      }
                      else{

                        Users.create(params)
                            .then(function(result){

                        var driverName = req.body.name;
                        var driverMobile = req.body.mobile;
                        var driverEmail = req.body.email;

                        var mailContent="Welcome "+driverName+", <br></br>Following Your Login Credentials.<br></br>UserName : "+driverEmail+"<br></br> Password : "+password;

                        var smsContent='Welcome '+driverName+',Following Your Login Credentials. UserName : '+driverEmail+','+' Password : '+password;

                        request({
                                    url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLog&messagetext='+smsContent+'.&flash=0&recipients='+driverMobile+'',
                                    method: 'GET',
                                }, function(error, response, body){
                                    if(error) {
                                       req.flash('success', 'Driver added Successfully.');
                                        res.redirect('/driver');

                                    } else {

                                      if(driverEmail){
                            
                                          var mailOptions = {
                                                    from: 'info@pooplogg.com',
                                                    to: driverEmail,
                                                    subject: 'PoopLogg:Account Created',
                                                    html: mailContent
                                                  };

                                          transporter.sendMail(mailOptions, function(error, info){
                                                if (error) {
                                                   req.flash('success', 'Driver added Successfully.');
                                                    res.redirect('/driver');

                                                } else {
                                                    req.flash('success', 'Driver added Successfully.');
                                                     res.redirect('/driver');
                  }
                                              });
                                        }else{
                                            req.flash('success', 'Driver added Successfully.');
                                            res.redirect('/driver');

                                        }

                                    }//sms
                                  })//sms
                                 // req.flash('success', 'Driver added Successfully.');
                                 // res.redirect('/driver');
                                 //dddd
                                
                            })
                            .catch(function(err)
                            {
                                 console.log("eee33333");
                                console.log(err);
                                req.flash('error', 'Driver Could Not Be Added.');
                                res.redirect('/driver');
                                //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                            })
                        }
                      });
            }

    })
    .catch(function(err)
    {
       console.log("eee44444444");
      console.log(err);
        req.flash('error', err);
        res.redirect('/driver');
        //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })

});

driverRoute.get('/:providerId', function (req, res) {

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;


    console.log("Driver list");
    console.log(req.params.providerId);
    var providerId = req.params.providerId;

    Users.findAll({'where':{"addedBy":providerId,"userType":4}})
      .then(function(result)
      {
        console.log(result);
        res.render('driver/list_drivers',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('driver/list_drivers',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })

});



driverRoute.get('/download/:id/:columnName/:fileName', function (req, res) {

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var directoryName = req.params.columnName;
    var fileName= req.params.fileName;
    var targetPath = path.resolve('./public/'+directoryName+'/'+fileName);

    var filePath = targetPath; // Or format the path using the `id` rest param
    var savedfileName = fileName; // file name 


    res.download(filePath, savedfileName,function(err,data) {
        if(err){
            //req.flash('error',err);
            //res.redirect('/vehicle/'+userId);

        }
        //req.flash('success',"Documents Downloaded Successfully.");
        //res.redirect('/vehicle/'+userId);

    });
   

});



driverRoute.get('/driversAssistantAssociation/:partnerId',function(req,res){
    ssn = req.session;

    if(req.user){

    var username = req.user.username;

      ssn.accesstoken=req.user.accesstoken;
      ssn.username=req.user.username;
      ssn.userType=req.user.userType;
      ssn.userId=req.user.id;
      ssn.profileImage=req.user.profileImage;
      ssn.accesstoken = req.user. accesstoken;


      var username=ssn.username;
      var userType=ssn.userType;
      var userId=ssn.userId;
      var ssnprofileImage=ssn.profileImage;
      var accesstoken = ssn.accesstoken;

      if(userType==3){

        if(accesstoken){
          //req.user.accesstoken;
          var userData = req.user;
          var dataArr = [];
          // Users.update({'accesstoken':accesstoken},{'where':{'username':username}})
          //     .then(function(data){

                var driverType = 4;
                var spoterType= 5;
                var driverLength=1;
                 //Users.findAll({'where':{"userType":driverType,"status":'Y'}})
                 Users.findAll({'where':{"userType":driverType}})
                      .then(function(result)
                      {
                        
                        var totalLength = result.length;

                        if(totalLength){

                          result.map(function(elm){
                            var driverObj={};

                        Users.find({'where':{"userType":spoterType,"status":'Y',"vehicleNumber":elm.vehicleNumber}})
                          .then(function(spoterData)
                          {
                            var spoterObj={};
                            if(spoterData){
                                spoterObj.name=spoterData.name;
                                spoterObj.email=spoterData.email;
                                spoterObj.mobile=spoterData.mobile;
                                spoterObj.profileImage=spoterData.profileImage;
                                spoterObj.licenceNumber=spoterData.licenceNumber;
                                spoterObj.vehicleMakeModel=spoterData.vehicleMakeModel;
                                spoterObj.vehicleNumber=spoterData.vehicleNumber;

                            }

                            //driver obj
                            driverObj.name=elm.name;
                            driverObj.email=elm.email;
                            driverObj.mobile=elm.mobile;
                            driverObj.licenceNumber=elm.licenceNumber;
                            driverObj.vehicleMakeModel=elm.vehicleMakeModel;
                            driverObj.profileImage=elm.profileImage;
                            driverObj.vehicleNumber=elm.vehicleNumber;

                            dataArr.push({"driverObj":driverObj,"spoterObj":spoterObj});
                            if(driverLength==totalLength){

                                console.log(dataArr);
                                res.render('driver/driversAssistantAssociation',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:dataArr});
                            }
                            driverLength++;

                        })//spoter
                            .catch(function(err)
                            {
                                var result=[];
                                req.flash('error',err);
                                res.render('driver/driversAssistantAssociation',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});
                            })

                        })//map
                        }else{//if drivers not available

                              var result=[];
                               // req.flash('error',err);
                                res.render('driver/driversAssistantAssociation',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});

                        }                        
                        

                      })
                    .catch(function(err)
                    {
                        var result=[];
                        req.flash('error',err);
                        res.render('driver/driversAssistantAssociation',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});
                    })
                   //res.render('user/index',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
             
          }else{
                req.flash('error','User accesstoken not found.');
                res.redirect('/');
          }
      
      }else{
         req.flash('error','Authentication Failed.');
         res.redirect('/');
      }
    }else{//req.user
            req.flash('error','Authentication Failed.');
            res.redirect('/');
    }
});


driverRoute.get('/update/:driverId',function(req,res){
  console.log("update................");

      ssn.accesstoken=req.user.accesstoken;
      ssn.username=req.user.username;
      ssn.userType=req.user.userType;
      ssn.userId=req.user.id;
      ssn.profileImage=req.user.profileImage;
      ssn.accesstoken = req.user. accesstoken;
      var menuArr = ssn.menuArr;


      var username=ssn.username;
      var userType=ssn.userType;
      var userId=ssn.userId;
      var ssnprofileImage=ssn.profileImage;
      var accesstoken = ssn.accesstoken;

  var driverId = req.params.driverId;

  Users.find({'where':{"id":driverId}})
      .then(function(result)
      {
        console.log(result);
        res.render('driver/update_drivers',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr});
      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('driver/update_drivers',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr});
    })

});




var cpUpload = upload.fields([{ name: 'driverLicence', maxCount: 1 }, { name: 'lasdriLicense', maxCount: 1 }, { name: 'profileImage', maxCount: 1 }])


driverRoute.post('/update', cpUpload, function (req, res, next) {

    var today = new Date();
    var email = req.body.email;
    var id=req.body.id;

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    Users.find({'where':{"id":id}})
        .then(function(userData)
        {   
        if(userData){  

        var profileImage = userData.profileImage; 
        var driverLicence = userData.driverLicence;
        var lasdriLicense = userData.lasdriLicense;

      var documentsObjCheck = JSON.stringify(req.files);

      if(documentsObjCheck=='{}'){

        var  params = {
                  //"driverId":id,
                  //"email":req.body.email,
                  //"username":req.body.email,
                  "name":req.body.name,
                  "mobile":req.body.mobile,
                  "vehicleNumber":req.body.vehicleNumber,
                  "profileImage":profileImage,
                  "driverLicence":driverLicence,
                  "lasdriLicense":lasdriLicense,
                  //"addedBy":userId,
                  //"status":"P",
                  //"approvedDate":'',
                  "updatedAt":today
              };

          Users.findAll({'where':{"username":email,id:{'$ne':id }}})
          .then(function(result)
          {       
              if(result.length){
                  req.flash('error','Email already exist.');
                  res.redirect('/driver/update/'+id);
              }else{

                //UpdateDriverRequests.create(params)
                 // .then(function(result){

            Users.update(params,{'where':{'id':id}})
            .then(function(data){
                      req.flash('success', 'Driver Updated Successfully.');
                      res.redirect('/driver');

                   
                  })
                  .catch(function(err)
                  {
                      
                      req.flash('error', 'Driver Update Request could not be send to Admin.');
                      res.redirect('/driver/update/'+id);
                  })
              }
          })
          .catch(function(err)
          {
              console.log(err);
          res.redirect('/drivers/update/'+id);
          })

      }else{

        var driverLicence = userData.driverLicence;
        var lasdriLicense = userData.lasdriLicense;
        var profileImage = userData.profileImage;

      // console.log(req.files['certificateOfIncorporation']);
        var filesArray=[];

        if(req.files['driverLicence']){
          var driverLicence = req.files['driverLicence'][0].originalname;
          filesArray.push(req.files['driverLicence'][0]);

        }
        if(req.files['lasdriLicense']){
          var lasdriLicense = req.files['lasdriLicense'][0].originalname;
          filesArray.push(req.files['lasdriLicense'][0]);
        }

        if(req.files['profileImage']){
          var profileImage = req.files['profileImage'][0].originalname;
          filesArray.push(req.files['profileImage'][0]);
        }

         var  params = {
                  //"driverId":id,
                  //"email":req.body.email,
                  //"username":req.body.email,
                  "name":req.body.name,
                  "mobile":req.body.mobile,
                  "vehicleNumber":req.body.vehicleNumber,
                  "profileImage":profileImage,
                  "driverLicence":driverLicence,
                  "lasdriLicense":lasdriLicense,
                  //"addedBy":userId,
                  //"status":"P",
                   //"approvedDate":'',
                  "updatedAt":today
              };
      
      Users.findAll({'where':{"username":email,id:{'$ne':id }}})
      .then(function(result)
      {       
          if(result.length){
              req.flash('error','Email allready exist.');
        res.redirect('/driver/update/'+id);
          }else{
                  async.each(filesArray,function(file,eachcallback){
                      async.waterfall([
                      function (callback) {
                        fs.readFile(file.path, (err, data) => {
                          if (err) {
                            console.log("err ocurred", err);
                            req.flash('error', err);
                            res.redirect('/driver/update/'+id);
                            }
                          else {
                            callback(null,data);
                          }
                          });
                      },
                      function (data, callback) {
                    
                          if(file.fieldname=='driverLicence'){
                            var targetPath = path.resolve('./public/driverLicence/'+file.originalname);
                        }else if(file.fieldname=='lasdriLicense'){
                            var targetPath = path.resolve('./public/lasdriLicense/'+file.originalname);

                        }else{
                            var targetPath = path.resolve('./public/user_images/'+file.originalname);
                        }
                      

                      fs.writeFile(targetPath, data, (err) => {
                        if (err) {
                          console.log("catch wrote");
                              req.flash('error', err);
                              res.redirect('/driver/update/'+id);

                        }
                        else {
                        callback(null, 'done');
                        }
                        });
                      }
                      ], function (err, result) {
                        // result now equals 'done'
                        //pass final callback to async each to move on to next file
                        eachcallback();
                      });
                      },function(err){
                        if(err){

                          console.log("catch file");
                            req.flash('error', err);
                            res.redirect('/driver/update/'+id);

                        }
                        else{
                          Users.update(params,{'where':{'id':id}})
                          .then(function(data){
                           // UpdateDriverRequests.create(params)
                              //.then(function(result){
                                
                              //req.flash('success', 'Driver Updated Request has been send to Admin.');
                              req.flash('success', 'Driver Updated Successfully.');
                              res.redirect('/driver');
                              })
                              .catch(function(err)
                              {
                                  console.log(err);
                                  req.flash('error', 'Driver Update Request could not be send to Admin.');
                                  res.redirect('/driver/update/'+id);

                              })
                          }
                        });
              }

        })
        .catch(function(err)
        {
            req.flash('error', err);
            res.redirect('/driver/update/'+id);

        })

      } 
    }else{
          req.flash('error', "Driver not found.");
          res.redirect('/driver/update/'+id);

    }  

    })
    .catch(function(err)
    {

      console.log("catch aaaaa "+err);
        req.flash('error', err);
        res.redirect('/driver/update/'+id);

    })
});



driverRoute.get('/updateRequestedDrivers/:partnerId',function(req,res){
  console.log("updateRequestedDrivers................");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;


    console.log("Driver list");
    console.log(req.params.partnerId);
    var partnerId = req.params.partnerId;

    UpdateDriverRequests.findAll({'where':{"addedBy":partnerId,"status":"P"}})
      .then(function(result)
      {
        console.log(result);
        res.render('driver/updateRequestedDrivers',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('driver/updateRequestedDrivers',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })

});


module.exports = driverRoute;
