var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
var fs = require("fs");
var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var download = require('image-downloader');
var request = require('request');
var vehicleRoute = require('express').Router();

var Vehicles = require('./../models').Vehicles;
var Users = require('./../models').Users;
var UserRequests = require('./../models').UserRequests;
var Areas = require('./../models').Areas;

vehicleRoute.get('/download/:id/:columnName/:fileName', function (req, res) {


    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var directoryName = req.params.columnName;
    var fileName= req.params.fileName;
    var targetPath = path.resolve('./public/vehicle/'+directoryName+'/'+fileName);

    var filePath = targetPath; // Or format the path using the `id` rest param
    var savedfileName = fileName; // file name 


    res.download(filePath, savedfileName,function(err,data) {
        if(err){
            //req.flash('error',err);
            //res.redirect('/vehicle/'+userId);

        }
        //req.flash('success',"Documents Downloaded Successfully.");
        //res.redirect('/vehicle/'+userId);

    });
   

});
vehicleRoute.get('/', function (req, res) {
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    Areas.findAll({'where':{"status":1}})
      .then(function(areaData)
      {

          res.render('partner/add_vehicle',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,areaData:areaData,menuArr:menuArr});

    })
    .catch(function(err)
    {
        req.flash('error', err);
          res.render('partner/add_vehicle',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,areaData:areaData,menuArr:menuArr});
    })
});


var cpUpload = upload.fields([{ name: 'vehicleLicense', maxCount: 1 }, { name: 'hackneyPermit', maxCount: 1 },
 { name: 'roadworthiness', maxCount: 1 },{ name: 'swadanCertificate', maxCount: 1 },{ name: 'insurance', maxCount: 1 },
 { name: 'lagosStatePermit', maxCount: 1 },{ name: 'poopLogInspectionCert', maxCount: 1 }])


vehicleRoute.post('/',cpUpload, function (req, res) {
    console.log("post vehicle");
    var today = new Date();
    //var userId = req.userId;

    var vehicleLicense = req.files['vehicleLicense'][0].originalname;
    var hackneyPermit = req.files['hackneyPermit'][0].originalname;
    var roadworthiness = req.files['roadworthiness'][0].originalname;
    var swadanCertificate = req.files['swadanCertificate'][0].originalname;
    var insurance = req.files['insurance'][0].originalname;
    var lagosStatePermit = req.files['lagosStatePermit'][0].originalname;
    var poopLogInspectionCert = req.files['poopLogInspectionCert'][0].originalname;

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var  params = {
                "userId":userId,
                "vehicleNumber":req.body.vehicleNumber,
                "areaCode":req.body.areaCode,
                "vehicleColour":req.body.vehicleColour,
                "vehicleLicense":vehicleLicense,
                "hackneyPermit":hackneyPermit,
                "roadworthiness":roadworthiness,
                "swadanCertificate":swadanCertificate,
                "insurance":insurance,
                "lagosStatePermit":lagosStatePermit,
                "poopLogInspectionCert":poopLogInspectionCert,
                "createdAt":today,
                "updatedAt":today
            };
            var filesArray=[];

                filesArray.push(req.files['vehicleLicense'][0],req.files['hackneyPermit'][0],req.files['roadworthiness'][0],
                    req.files['swadanCertificate'][0],req.files['insurance'][0],req.files['lagosStatePermit'][0]);

                async.each(filesArray,function(file,eachcallback){
                    async.waterfall([
                    function (callback) {
                      fs.readFile(file.path, (err, data) => {
                        if (err) {
                          console.log("err ocurred", err);
                          req.flash('error', err);
                          res.redirect('/partnersVehicle');
                          //return res.render('vehicle/add_vehicle',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                          }
                        else {
                          callback(null,data);
                        }
                        });
                    },
                    function (data, callback) {
                    
                        if(file.fieldname=='vehicleLicense'){
                            var targetPath = path.resolve('./public/vehicle/vehicleLicense/'+file.originalname);
                        }else if(file.fieldname=='hackneyPermit'){
                            var targetPath = path.resolve('./public/vehicle/hackneyPermit/'+file.originalname);
                        }else if(file.fieldname=='roadworthiness'){
                            var targetPath = path.resolve('./public/vehicle/roadworthiness/'+file.originalname);
                        }else if(file.fieldname=='swadanCertificate'){
                            var targetPath = path.resolve('./public/vehicle/swadanCertificate/'+file.originalname);
                        }else if(file.fieldname=='insurance'){
                            var targetPath = path.resolve('./public/vehicle/insurance/'+file.originalname);
                        }else if(file.fieldname=='lagosStatePermit'){
                            var targetPath = path.resolve('./public/vehicle/lagosStatePermit/'+file.originalname);
                        }else if(file.fieldname=='poopLogInspectionCert'){
                            var targetPath = path.resolve('./public/vehicle/poopLogInspectionCert/'+file.originalname);
                        }

                    fs.writeFile(targetPath, data, (err) => {
                      if (err) {
                            req.flash('error',err);
                            res.redirect('/partnersVehicle');
                            //return res.render('vehicle/add_vehicle',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                      }
                      else {
                      callback(null, 'done');
                      }
                      });
                    }
                    ], function (err, result) {
                      // result now equals 'done'
                      //pass final callback to async each to move on to next file
                      eachcallback();
                    });
                    },function(err){
                      if(err){
                          req.flash('error',err);
                          res.redirect('/partnersVehicle');
                           //return res.render('vehicle/add_vehicle',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                      }
                      else{

                        Vehicles.create(params)
                            .then(function(result){
                                req.flash('success', 'Vehicle Added Successfully.');
                                res.redirect('/partnersVehicle/viewVehicles');
                                //return res.render('vehicle/add_vehicle',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                            })
                            .catch(function(err)
                            {
                                console.log(err);
                                req.flash('error', 'Vehicle Could Not Be Added.');
                                res.redirect('/partnersVehicle');
                            })
                        }
                      });
            


});


vehicleRoute.get('/viewVehicles', function (req, res) {

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    console.log("Driver list");


console.log(userId);

console.log("userId")
    Vehicles.findAll({'where':{"userId":userId}})
      .then(function(result)
      {
        res.render('partner/assign_list_vehicles',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
      })
    .catch(function(err)
    {
        req.flash('error',err);
        res.render('partner/assign_list_vehicles',{data:[],username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
    })

});

vehicleRoute.get('/admin/allVehicles', function (req, res) {

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var vehiclesLength=1;

    Vehicles.findAll()
      .then(function(result)
      {

        var totalLength= result.length;
        if(totalLength){

            result.map(function(elm){

                Users.find({'where':{"id":elm.userId}})
                  .then(function(users)
                  {
                    elm.partnerName= users.name;

                    console.log(result);
                     if(vehiclesLength==totalLength){
                        res.render('vehicle/admin_list_vehicles',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                     }
                     vehiclesLength++;

                  })
                  .catch(function(err)
                    {
                        var result=[];
                        req.flash('error',err);
                        res.render('vehicle/admin_list_vehicles',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                    })


            });

        }else{
            var result=[];

            res.render('vehicle/admin_list_vehicles',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});

        }
        
        
      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('vehicle/admin_list_vehicles',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })

});


vehicleRoute.get('/admin/vehicleDriver/:vehicleId', function (req, res) {

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;

    var vehicleId = req.params.vehicleId;
    var dataArr=[];

    console.log("vehicleId : "+vehicleId);

    console.log("hereeeeee");
    console.log(userId);

    
    // Users.findAll({'where':{userType:{in:[7,8]},"vehicleNumber": {
    //     $eq: null
    //   }}})

    Users.findAll({'where':{userType:{in:[7,8]},"vehicleNumber": {
        $eq: null
      },"partnerId":userId}})
     // Users.findAll({'where':{userType:{'$ne':3 },"vehicleNumber": {
     //    $eq: null
     //  }}})
      .then(function(assignUsers)
      {  

        console.log(assignUsers);
        console.log("Test  aaaaaaaaaaaaaaa");
        Vehicles.find({'where':{"id":vehicleId}})
          .then(function(result)
          {

            var vehicleNumber = result.vehicleNumber;
            var usersLength=1;            

                    Users.findAll({'where':{"vehicleNumber":vehicleNumber,userType:{in:[7,8]}}})
                      .then(function(users)
                      {
                        var totalLength = users.length;
                        console.log(totalLength);
                       if(totalLength){


                        console.log("inside");
                        var driverObj={};
                        var spoterObj={};
                        users.map(function(elm){
                        var userType = elm.userType;
                        
                            if(userType==8){
                                    spoterObj.name=elm.name;
                                    spoterObj.email=elm.email;
                                    spoterObj.mobile=elm.mobile;
                                    spoterObj.profileImage=elm.profileImage;
                                    spoterObj.licenceNumber=elm.licenceNumber;
                                    spoterObj.vehicleMakeModel=elm.vehicleMakeModel;
                                    spoterObj.vehicleNumber=elm.vehicleNumber;

                                }else if(userType==7){
                                    driverObj.name=elm.name;
                                    driverObj.email=elm.email;
                                    driverObj.mobile=elm.mobile;
                                    driverObj.licenceNumber=elm.licenceNumber;
                                    driverObj.vehicleMakeModel=elm.vehicleMakeModel;
                                    driverObj.profileImage=elm.profileImage;
                                    driverObj.vehicleNumber=elm.vehicleNumber;
                                }

                             if(usersLength==totalLength){
                                dataArr.push({"driverObj":driverObj,"spoterObj":spoterObj,assignUsers:assignUsers});
                                console.log(dataArr);
                                res.send(dataArr);

                             }
                             usersLength++;

                        })
                    }else{
                        dataArr.push({"driverObj":{},"spoterObj":{},assignUsers:assignUsers});
                        console.log("outside");
                        res.send(dataArr);
                    }
                        

              })
              .catch(function(err)
                {
                    var dataArr=[];
                    res.send(dataArr);
                    //res.render('vehicle/admin_list_vehicles',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                })
            
          })
        .catch(function(err)
        {
            var dataArr=[];
            res.send(dataArr);
        })

    })
    .catch(function(err)
    {
        var dataArr=[];
        res.send(dataArr);
    })

});

vehicleRoute.post('/assign/driver/:vehicleId/:driverId/:assignUserType', function (req, res) {


     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var assignUserType=req.params.assignUserType;
    if(assignUserType=='D'){

    var vehicleId = req.params.vehicleId;
    var driverId = req.params.driverId;

    Vehicles.find({'where':{"id":vehicleId}})
      .then(function(result)
      {

        if(result){

            Vehicles.update({'isDriverAssign':'Y','driverId':driverId},{'where':{'id':vehicleId}})
             .then(function(data){

                 Users.update({'vehicleNumber':result.vehicleNumber},{'where':{'id':driverId}})
                    .then(function(data){
                    res.send({"status":"success","msg":"Driver assigned successfully."});
                })
                .catch(function(err)
                {
                    console.log(err);
                    res.send({"status":"failed","msg":"Driver could not be assigned."});
                    //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                })

             })
             .catch(function(err)
            {
                res.send({"status":"failed","msg":"Driver could not be assigned."});
                //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
            })
         }else{

            res.send({"status":"failed","msg":"vehicle Not found."});

         }
    })
     .catch(function(err)
    {
        res.send({"status":"failed","msg":"Driver could not be assigned."});
        //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })
   }else{

       console.log("spoterrrrrr");
      console.log(req.params.vehicleId);
      console.log(req.params.driverId);

      var vehicleId = req.params.vehicleId;
      var spoterId =  req.params.driverId;;

      Vehicles.find({'where':{"id":vehicleId}})
        .then(function(result)
        {

          if(result){

              Vehicles.update({'isSpoterAssign':'Y','spoterId':spoterId},{'where':{'id':vehicleId}})
               .then(function(data){

                   Users.update({'vehicleNumber':result.vehicleNumber},{'where':{'id':spoterId}})
                      .then(function(data){
                      res.send({"status":"success","msg":"Spoter Boy assigned successfully."});
                  })
                  .catch(function(err)
                  {
                      console.log(err);
                      res.send({"status":"failed","msg":"Spoter Boy could not be assigned."});
                      //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                  })

               })
               .catch(function(err)
              {
                  res.send({"status":"failed","msg":"Spoter Boy could not be assigned."});
                  //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
              })
           }else{

              res.send({"status":"failed","msg":"vehicle Not found."});

           }
      })
       .catch(function(err)
      {
          res.send({"status":"failed","msg":"Spoter Boy could not be assigned."});
          //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
      })


   }

});


vehicleRoute.post('/assign/spoter/:vehicleId/:spoterId', function (req, res) {


     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;

    console.log("spoterrrrrr");
    console.log(req.params.vehicleId);
    console.log(req.params.spoterId);

    var vehicleId = req.params.vehicleId;
    var spoterId = req.params.spoterId;

    Vehicles.find({'where':{"id":vehicleId}})
      .then(function(result)
      {

        if(result){

            Vehicles.update({'isSpoterAssign':'Y','spoterId':spoterId},{'where':{'id':vehicleId}})
             .then(function(data){

                 Users.update({'vehicleNumber':result.vehicleNumber},{'where':{'id':driverId}})
                    .then(function(data){
                    res.send({"status":"success","msg":"Driver assigned successfully."});
                })
                .catch(function(err)
                {
                    console.log(err);
                    res.send({"status":"failed","msg":"Driver could not be assigned."});
                    //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                })

             })
             .catch(function(err)
            {
                res.send({"status":"failed","msg":"Driver could not be assigned."});
                //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
            })
         }else{

            res.send({"status":"failed","msg":"vehicle Not found."});

         }
    })
     .catch(function(err)
    {
        res.send({"status":"failed","msg":"Driver could not be assigned."});
        //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })

});


vehicleRoute.get('/admin/allVehicles/:providerId', function (req, res) {

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    Vehicles.findAll({'where':{"userId":providerId}})
      .then(function(result)
      {
        console.log(result);
        res.render('vehicle/admin_list_vehicles',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('vehicle/admin_list_vehicles',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })

});

vehicleRoute.get('/assignVehicle/:providerId', function (req, res) {

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    console.log("assignVehicle list");
    console.log(req.params.providerId);
    var providerId = req.params.providerId;

    Vehicles.findAll({'where':{"userId":providerId}})
      .then(function(result)
      {
        console.log(result);
        res.render('vehicle/assign_list_vehicles',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('vehicle/assign_list_vehicles',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })
});



module.exports = vehicleRoute;
