var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');


var driverRoute = require('express').Router();

var Users = require('./../models').Users;
var Vehicles = require('./../models').Vehicles;

driverRoute.get('/', function (req, res) {
    //console.log(req.userId);

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    Vehicles.findAll({'where':{"userId":userId,isDriverAssign: {'$ne':'Y' } }})
    .then(function(vehiclesData)
    { 

    res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,vehiclesData:vehiclesData});
    })
    .catch(function(err)
    {
        var vehiclesData=[];
        req.flash('error', err);
        return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,vehiclesData:vehiclesData});
    })
});

driverRoute.post('/aa',upload.single('profileImage'), function (req, res) {
    console.log("post driver");
    var today = new Date();
    var email = req.body.email;
    var password = "test123";
    var profileImage =req.file.filename;
    var addUserType = 4;
    console.log("profileImage");
    console.log(profileImage);
    var userId = req.userId;

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;


    var  params = {
                "username":req.body.email,
                "email":req.body.email,
                "name":req.body.name,
                "mobile":req.body.mobile,
                "password":password,
                "userType":addUserType,
                "profileImage":profileImage,
                "licenceNumber":req.body.licenceNumber,
                "vehicleMakeModel":req.body.vehicleMakeModel,
                "vehicleColour":req.body.vehicleColour,
                "vehicleLicenseNumber":req.body.vehicleLicenseNumber,
                "addedBy":userId,
                "createdAt":today,
                "updatedAt":today,
                "status":"Y"
            };


    if(req.file){

    //var originalname = req.file.originalname;

    var targetPath = path.resolve('./public/user_images/'+req.file.filename);
        fs.rename(req.file.path,targetPath, function(err) {
          if (err) {
            req.flash('error',err);
            return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
          } else {
                Users.findAll({'where':{"username":email}})
                .then(function(result)
                {       
                    if(result.length){
                        req.flash('error','Username allready exist.');
                        return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                    }else{
                         Users.create(params)
                        .then(function(result){
                            req.flash('success', 'Driver added Successfully.');
                            return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                        })
                        .catch(function(err)
                        {
                            req.flash('error', 'Driver Could Not Be Added.');
                            return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                        })
                    }
                })
                .catch(function(err)
                {
                 console.log(err);
                  return res.status(401).send({status:'error','data':{'message':'Driver Could Not Be Added'}});
                })

          }
        
        });
    }else{//profile empty
            Users.findAll({'where':{"username":email}})
                .then(function(result)
                {       
                    if(result.length){
                        req.flash('error','Username allready exist.');
                        return res.render('driver/add_driver');
                    }else{
                         Users.create(params)
                        .then(function(result){
                            req.flash('success', 'Driver added Successfully.');
                            return res.render('driver/add_driver');
                        })
                        .catch(function(err)
                        {
                            req.flash('error', 'Driver Could Not Be Added.');
                            return res.render('driver/add_driver');
                        })
                    }
                })
                .catch(function(err)
                {
                 console.log(err);
                  return res.status(401).send({status:'error','data':{'message':'Driver Could Not Be Added'}});
                })
    }
});

var cpUpload = upload.fields([{ name: 'driverLicence', maxCount: 1 }, { name: 'lasdriLicense', maxCount: 1 }, { name: 'profileImage', maxCount: 1 }])

driverRoute.post('/',cpUpload, function (req, res) {
    console.log("post driver");
    var today = new Date();
    var email = req.body.email;
    var password = "test123";
    //var profileImage =req.file.filename;
    var addUserType = 4;
    console.log("profileImage");
    var userId = req.userId;

    var driverLicence = req.files['driverLicence'][0].originalname;
    var lasdriLicense = req.files['lasdriLicense'][0].originalname;
    var profileImage = req.files['profileImage'][0].originalname;

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var  params = {
                "username":req.body.email,
                "email":req.body.email,
                "name":req.body.name,
                "mobile":req.body.mobile,
                "password":password,
                "userType":addUserType,
                "vehicleNumber":req.body.vehicleNumber,
                "driverLicence":driverLicence,
                "lasdriLicense":lasdriLicense,
                "profileImage":profileImage,
                "addedBy":userId,
                "createdAt":today,
                "updatedAt":today,
                "status":"Y"
            };

    Users.findAll({'where':{"username":email}})
    .then(function(result)
    {       
        if(result.length){
            req.flash('error','Username allready exist.');
            res.redirect('/driver');
            //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});

        }else{

            var filesArray=[];

                filesArray.push(req.files['driverLicence'][0],req.files['lasdriLicense'][0],req.files['profileImage'][0]);


                async.each(filesArray,function(file,eachcallback){
                    async.waterfall([
                    function (callback) {
                      fs.readFile(file.path, (err, data) => {
                        if (err) {
                          console.log("err ocurred", err);
                          req.flash('error', err);
                          res.redirect('/driver');
                          //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                          }
                        else {
                          callback(null,data);
                        }
                        });
                    },
                    function (data, callback) {
                    
                        if(file.fieldname=='driverLicence'){
                            var targetPath = path.resolve('./public/driverLicence/'+file.originalname);
                        }else if(file.fieldname=='lasdriLicense'){
                            var targetPath = path.resolve('./public/lasdriLicense/'+file.originalname);

                        }else{
                            var targetPath = path.resolve('./public/user_images/'+file.originalname);

                        }
                    

                    fs.writeFile(targetPath, data, (err) => {
                      if (err) {
                            req.flash('error',err);
                            res.redirect('/driver');
                            //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                      }
                      else {
                      callback(null, 'done');
                      }
                      });
                    }
                    ], function (err, result) {
                      // result now equals 'done'
                      //pass final callback to async each to move on to next file
                      eachcallback();
                    });
                    },function(err){
                      if(err){
                          req.flash('error',err);
                          res.redirect('/driver');
                           //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                      }
                      else{

                        Users.create(params)
                            .then(function(result){

                                Vehicles.update({'isDriverAssign':'Y'},{'where':{'vehicleNumber':req.body.vehicleNumber}})
                                    .then(function(data){
                                    req.flash('success', 'Driver added Successfully.');
                                    res.redirect('/driver');
                                    //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});

                                })
                                .catch(function(err)
                                {
                                    console.log(err);
                                    req.flash('error', 'Driver Could Not Be Added.');
                                    res.redirect('/driver');
                                    //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                                })
                            })
                            .catch(function(err)
                            {
                                console.log(err);
                                req.flash('error', 'Driver Could Not Be Added.');
                                res.redirect('/driver');
                                //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
                            })
                        }
                      });
            }

    })
    .catch(function(err)
    {
        req.flash('error', err);
        res.redirect('/driver');
        //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })

});

driverRoute.get('/:providerId', function (req, res) {

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;


    console.log("Driver list");
    console.log(req.params.providerId);
    var providerId = req.params.providerId;

    Users.findAll({'where':{"addedBy":providerId,"userType":4}})
      .then(function(result)
      {
        console.log(result);
        res.render('driver/list_drivers',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('driver/list_drivers',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })

});



driverRoute.get('/download/:id/:columnName/:fileName', function (req, res) {

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var directoryName = req.params.columnName;
    var fileName= req.params.fileName;
    var targetPath = path.resolve('./public/'+directoryName+'/'+fileName);

    var filePath = targetPath; // Or format the path using the `id` rest param
    var savedfileName = fileName; // file name 


    res.download(filePath, savedfileName,function(err,data) {
        if(err){
            //req.flash('error',err);
            //res.redirect('/vehicle/'+userId);

        }
        //req.flash('success',"Documents Downloaded Successfully.");
        //res.redirect('/vehicle/'+userId);

    });
   

});


driverRoute.get('/admin/approvedDrivers', function (req, res) {
    console.log("driver list");
});





module.exports = driverRoute;
