var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var moment = require('moment');
var PDF = require('pdfkit');            //including the pdfkit module
var conversion = require("phantom-html-to-pdf")();


var reportsRoute = require('express').Router();

var Users = require('./../models').Users;
var Vehicles = require('./../models').Vehicles;
var DrverAttendance = require('./../models').DrverAttendance;
var UserRequests = require('./../models').UserRequests;

reportsRoute.get('/driverAttendanceReport', function (req, res) {


// 	var text = '<table><tr><td>aaaaaaaaaaaaaa</td></tr></table>';




// var doc = new PDF();                        //creating a new PDF object
// doc.pipe(fs.createWriteStream('node1.pdf'));  //creating a write stream 
//             //to write the content on the file system
// doc.text(text, 100, 100);             //adding the text to be written, 
//             // more things can be added here including new pages
// doc.end(); //we end the document writing.




    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var driverAttendance = [];
    var fromDate ='';
    var toDate = '';

    return res.render('reports/driverAttendance',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverAttendance:driverAttendance,moment:moment,fromDate:fromDate,toDate:toDate});

    
});

reportsRoute.get('/driverAttendanceReport/:fromDate/:toDate', function (req, res) {
    //console.log(req.userId);

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var fromDate = req.params.fromDate;
    var toDate = req.params.toDate;

    // console.log("driverAttendanceRoute get driverAttendanceReport");

    // console.log("fromDate "+fromDate);
    // console.log("toDate "+toDate);

    // 	var fromDate = moment(fromDate).format("YYYY-MM-DD 00:00:01");

    // 	var toDate = moment(toDate).format("YYYY-MM-DD 00:00:01");


	console.log("fromDate1 "+fromDate);
	console.log("toDate1 "+toDate);
	var driverAttendanceLegth=1;

    DrverAttendance.findAll({'where':{"fromTime":{
          $gte: fromDate,$lte: toDate
        }}})
        .then(function(driverAttendance)
        { 
        	var totLength=driverAttendance.length;
        	if(totLength){
	        	driverAttendance.map(function(elm){

	        		 Users.find({'where':{"id":elm.driverId}})
	                    .then(function(user)
	                    {
	                    	elm.driverName = user.name;
			        		if(totLength==driverAttendanceLegth){
			        			return res.render('reports/driverAttendance',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverAttendance:driverAttendance,moment:moment,fromDate:fromDate,toDate:toDate});

			        		}
			        		driverAttendanceLegth++;

			        	})
				      .catch(function(err){

				      	var driverAttendance = [];

				        req.flash('error',err);
				        return res.render('reports/driverAttendance',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverAttendance:driverAttendance,moment:moment,fromDate:fromDate,toDate:toDate});
				      })

	        	})//map
	        }else{
	        	var driverAttendance=[];
	        	req.flash('error',"No data found.");

	        	return res.render('reports/driverAttendance',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverAttendance:driverAttendance,moment:moment,fromDate:fromDate,toDate:toDate});

	        }

        	
        
        console.log(driverAttendance);

        })
      .catch(function(err){

      	console.log("error");
        req.flash('error',err);
        var driverAttendance=[];

	     return res.render('reports/driverAttendance',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverAttendance:driverAttendance,moment:moment,fromDate:fromDate,toDate:toDate});
      })

    //return res.render('reports/driverAttendance',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});

    
});

/* Pdf generato*/
/*
reportsRoute.get('/driverAttendanceReport/pdf/:fromDate/:toDate', function (req, res) {



	ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var fromDate = req.params.fromDate;
    var toDate = req.params.toDate;

	console.log("fromDate1 "+fromDate);
	console.log("toDate1 "+toDate);
	var driverAttendanceLegth=1;



    DrverAttendance.findAll({'where':{"fromTime":{
          $gte: fromDate,$lte: toDate
        }}})
        .then(function(driverAttendance)
        { 
        	var totLength=driverAttendance.length;
        	if(totLength){
        	var tableData = "<div><img src='./user_images/dilip.jpg'></div>";
        	 tableData += "<table style='width:100%;border-collapse:collapsed;' border='1' cellspacing='0'><th>From Date Time</th><th>To Date Time</th><th>Name</th><th>Vehicle Number</th><th>Total Hrs</th>";

	        	driverAttendance.map(function(elm){

	        		 Users.find({'where':{"id":elm.driverId}})
	                    .then(function(user)
	                    {
	                    	elm.driverName = user.name;

	                    	var driverName = user.name;
	                    	tableData += "<tr><td>"+elm.fromTime+"</td><td>"+elm.toTime+"</td><td>"+driverName+"</td><td>"+elm.vehicleNumber+"</td><td>"+elm.totalHrs+"</td></tr>";
			        		if(totLength==driverAttendanceLegth){

			        		tableData += "</table>";

							conversion({ html: tableData }, function(err, pdf) {
							  var output = fs.createWriteStream('output1.pdf')
							  console.log(pdf.logs);
							  console.log(pdf.numberOfPages);
								// since pdf.stream is a node.js stream you can use it
								// to save the pdf to a file (like in this example) or to
								// respond an http request.
							  pdf.stream.pipe(output);
							});
			        			return res.render('reports/driverAttendance',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverAttendance:driverAttendance,moment:moment,fromDate:fromDate,toDate:toDate});

			        		}
			        		driverAttendanceLegth++;

			        	})
				      .catch(function(err){

				      	var driverAttendance = [];

				        req.flash('error',err);
				        return res.render('reports/driverAttendance',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverAttendance:driverAttendance,moment:moment,fromDate:fromDate,toDate:toDate});
				      })

	        	})//map
	        }else{
	        	var driverAttendance=[];
	        	req.flash('error',"No data found.");

	        	return res.render('reports/driverAttendance',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverAttendance:driverAttendance,moment:moment,fromDate:fromDate,toDate:toDate});

	        }

        	
        
        console.log(driverAttendance);

        })
      .catch(function(err){

      	console.log("error");
        req.flash('error',err);
        var driverAttendance=[];

	     return res.render('reports/driverAttendance',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driverAttendance:driverAttendance,moment:moment,fromDate:fromDate,toDate:toDate});
      })

});

*/



reportsRoute.get('/datewiseRequestReport', function (req, res) {

console.log("datewiseRequestReport Admin");

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;


    var requestData = [];
    var fromDate ='';
    var toDate = '';

    return res.render('reports/datewiseRequestReport',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,requestData:requestData,moment:moment,fromDate:fromDate,toDate:toDate,menuArr:menuArr});

    
});



reportsRoute.get('/datewiseRequestReport/:fromDate/:toDate', function (req, res) {
    //console.log(req.userId);

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    var fromDate = req.params.fromDate;
    var toDate = req.params.toDate;

    // console.log("driverAttendanceRoute get driverAttendanceReport");

    // console.log("fromDate "+fromDate);
    // console.log("toDate "+toDate);

    //  var fromDate = moment(fromDate).format("YYYY-MM-DD 00:00:01");

    //  var toDate = moment(toDate).format("YYYY-MM-DD 00:00:01");


    console.log("fromDate1 "+fromDate);
    console.log("toDate1 "+toDate);



    // var fromSortDate = fromDate.split('-');
    // var toSortDate = toDate.split('-');

    // console.log(fromSortDate);
    // console.log(toSortDate);

    // var formPickUpDateTime = fromSortDate[0]+'/'+fromSortDate[1]+'/'+fromSortDate[2];
    // var toPickUpDateTime = toSortDate[0]+'/'+toSortDate[1]+'/'+toSortDate[2];


    // console.log(toPickUpDateTime)

    var requestDataLegth=1;


    UserRequests.findAll({'where':{status:{in:['EX','R']},"createdAt":{
          $gte: fromDate,$lte: toDate
        }}},{order: [
                        ['createdAt', 'DESC']
                    ]})

    // UserRequests.findAll({'where':{"createdAt":{
    //       $gte: fromDate,$lte: toDate
    //     }}})
        .then(function(requestData)
        { 
            console.log(requestData);
            var totLength=requestData.length;
            if(totLength){
                requestData.map(function(elm){

                     Users.find({'where':{"id":elm.driverId}})
                        .then(function(driverData)
                        {


                            if(driverData){
                                elm.driverName = driverData.name;
                                elm.vehicleNumber = driverData.vehicleNumber;
                            }
                             else{

                                elm.driverName = '';
                                elm.vehicleNumber = '';
                            }

                            if(driverData)
                                var partnerId = driverData.partnerId;
                             else
                                partnerId = '';

                            Users.find({'where':{"id":partnerId}})
                            .then(function(partnerData)
                            {

                            if(partnerData)
                                elm.partnerName = partnerData.name;
                             else
                                elm.partnerName = '';

                                if(totLength==requestDataLegth){
                                    return res.render('reports/datewiseRequestReport',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,requestData:requestData,moment:moment,fromDate:fromDate,toDate:toDate,menuArr:menuArr});

                                }
                                requestDataLegth++;


                            })
                          .catch(function(err){


                            req.flash('error',err);
                            return res.render('reports/datewiseRequestReport',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,requestData:[],moment:moment,fromDate:fromDate,toDate:toDate,menuArr:menuArr});
                          })

                        })
                      .catch(function(err){


                        req.flash('error',err);
                        return res.render('reports/datewiseRequestReport',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,requestData:[],moment:moment,fromDate:fromDate,toDate:toDate,menuArr:menuArr});
                      })

                })//map
            }else{
                req.flash('error',"No data found.");

                return res.render('reports/datewiseRequestReport',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,requestData:[],moment:moment,fromDate:fromDate,toDate:toDate,menuArr:menuArr});

            }

            
        

        })
      .catch(function(err){

        console.log("error");
        req.flash('error',err);
        var driverAttendance=[];

         return res.render('reports/datewiseRequestReport',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,requestData:[],moment:moment,fromDate:fromDate,toDate:toDate,menuArr:menuArr});
      })

    //return res.render('reports/driverAttendance',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});

    
});







reportsRoute.get('/partnerdatewiseRequestReport', function (req, res) {

console.log("partnerdatewiseRequestReport Admin");

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;


    var requestData = [];
    var fromDate ='';
    var toDate = '';

    return res.render('reports/partnerdatewiseRequestReport',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,requestData:requestData,moment:moment,fromDate:fromDate,toDate:toDate,menuArr:menuArr});

    
});



reportsRoute.get('/partnerdatewiseRequestReport/:fromDate/:toDate', function (req, res) {
    //console.log(req.userId);

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    var fromDate = req.params.fromDate;
    var toDate = req.params.toDate;

    // console.log("driverAttendanceRoute get driverAttendanceReport");

    // console.log("fromDate "+fromDate);
    // console.log("toDate "+toDate);

    //  var fromDate = moment(fromDate).format("YYYY-MM-DD 00:00:01");

    //  var toDate = moment(toDate).format("YYYY-MM-DD 00:00:01");


    console.log("fromDate1 "+fromDate);
    console.log("toDate1 "+toDate);


            var requestDataLegth=1;


            UserRequests.findAll({'where':{status:{in:['EX','R']},"createdAt":{
                  $gte: fromDate,$lte: toDate
                },"partnerId":userId}},{order: [
                                ['createdAt', 'DESC']
                ]})
                .then(function(requestData)
                { 
                    //console.log(requestData);
                    var totLength=requestData.length;
                    if(totLength){
                        requestData.map(function(elm){

                             Users.find({'where':{"id":elm.driverId}})
                                .then(function(driverData)
                                {

                                    if(driverData){
                                        elm.driverName = driverData.name;
                                        elm.vehicleNumber = driverData.vehicleNumber;
                                    }
                                     else{

                                        elm.driverName = '';
                                        elm.vehicleNumber = '';
                                    }


                                    if(totLength==requestDataLegth){
                                        return res.render('reports/partnerdatewiseRequestReport',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,requestData:requestData,moment:moment,fromDate:fromDate,toDate:toDate,menuArr:menuArr});

                                    }
                                    requestDataLegth++;
                                    
                                })
                              .catch(function(err){

                                req.flash('error',err);
                                return res.render('reports/partnerdatewiseRequestReport',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,requestData:[],moment:moment,fromDate:fromDate,toDate:toDate,menuArr:menuArr});
                              })

                        })//map
                    }else{
                        req.flash('error',"No data found.");

                        return res.render('reports/partnerdatewiseRequestReport',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,requestData:[],moment:moment,fromDate:fromDate,toDate:toDate,menuArr:menuArr});

                    }
            })
          .catch(function(err){

            console.log("error");
            req.flash('error',err);

             return res.render('reports/partnerdatewiseRequestReport',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,requestData:[],moment:moment,fromDate:fromDate,toDate:toDate,menuArr:menuArr});
          })      
    
});

module.exports = reportsRoute;
