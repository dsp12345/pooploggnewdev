var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var nodemailer = require('nodemailer');
var randomstring = require("randomstring");
var request = require('request');


var managerRoute = require('express').Router();

var Users = require('./../models').Users;
var UserRoles = require('./../models').UserRoles;

var transporter = nodemailer.createTransport({
        host: 'server207.web-hosting.com',
        port: 465,
        secure: true,
        auth: {
            user: 'info@pooplogg.com',
            pass: '#pooplogg2020'
        }
      });

managerRoute.get('/', function (req, res) {
    console.log("driver approvedDrivers list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;


    //userRoles.findAll({'where':{"userId":userId,isDriverAssign: {'$ne':'Y' } }})

    UserRoles.findAll({'where':{id:{$notIn:[1,7,8,9]},status:1}})
    .then(function(userRolesData)
    { 
      res.render('manager/createManager',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:userRolesData,menuArr:menuArr});
    })
    .catch(function(err)
    {
        var vehiclesData=[];
        req.flash('error', err);
        res.render('manager/createManager',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:userRolesData,menuArr:menuArr});

    })
    
   
});


managerRoute.post('/', function (req, res) {

  ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

  var email = req.body.email;
  var name = req.body.name;

  Users.findAll({'where':{"username":email}})
    .then(function(result)
    {       
        if(result.length){
            req.flash('error','Username already exist.');
            res.redirect('/manager');
            //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});

        }else{

     var randomPassword = randomstring.generate({
                          length: 6,
                          charset: 'alphabetic'
                        });

      var password = randomPassword;

      var params = {
        name:req.body.name,
        username:req.body.email,
        email:req.body.email,
        mobile:req.body.mobile,
        password:password,
        userType:req.body.userRole,
        addedBy:userId,
        status:'Y'

      };

      var email = req.body.email;
      var mobile = req.body.mobile;

      Users.create(params)
          .then(function(result){

            var mailContent='Welcome '+name+',<br></br>Following Your Login Credentials.<br></br>UserName : '+email+'<br></br>'+' Password : '+password;
            var smsContent='Welcome '+name+',Following Your Login Credentials. UserName : '+email+','+' Password : '+password;

          request({
                  url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLog&messagetext='+smsContent+'.&flash=0&recipients='+mobile+'',
                  method: 'GET',
              }, function(error, response, body){
                  if(error) {
                     req.flash('success', 'Manager Added Successfully.');
                    res.redirect('/manager/managerList'); 
                  } else {

            if(email){

                var mailOptions = {
                          from: 'info@pooplogg.com',
                          to: req.body.email,
                          subject: 'PoopLogg:Account Created',
                          html: mailContent
                        };

                transporter.sendMail(mailOptions, function(error, info){
                      if (error) {
                         req.flash('success', 'Manager Added Successfully.');
                          res.redirect('/manager/managerList'); 
                      } else {
                          req.flash('success', 'Manager Added Successfully.');
                          res.redirect('/manager/managerList');                      }
                    });
              }else{
                  req.flash('success', 'Manager Added Successfully.');
                  res.redirect('/manager/managerList'); 

              }

              }//sms
            })//sms

        })
        .catch(function(err)
        {

          console.log(err);
            req.flash('error', 'Manager Could Not Be Added.');
            res.redirect('/manager'); 
        })


      }
    })
    .catch(function(err)
    {
      console.log(err);
        req.flash('error', 'Manager Could Not Be Added.');
        res.redirect('/manager'); 
    })


});


managerRoute.get('/managerList', function (req, res) {
    console.log("driver approvedDrivers list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    Users.findAll({'where':{"addedBy":'1',status:"Y"}})
      .then(function(result)
      {

        var managerLength = result.length;
        var i=1;

        if(managerLength){

          result.map(function(elm){

         console.log(elm.userType+'userType');


            UserRoles.find({'where':{"id":elm.userType}})
            .then(function(userRolesData)
            { 

              console.log(userRolesData);
                elm.userRole=userRolesData.userRole;
                if(managerLength==i){
                      res.render('manager/managerList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

                }
                i++;

              })
            .catch(function(err)
            {
                var result=[];
                req.flash('error',err);
                res.render('manager/managerList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});
            })

          })//map
        }else{

              res.render('manager/managerList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});

        }

        

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('manager/managerList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});
    })

    
    //res.render('userRequest/createUserRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
   
});




module.exports = managerRoute;
