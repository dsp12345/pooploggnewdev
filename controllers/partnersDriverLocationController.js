var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');

var driverRoute = require('express').Router();

var Users = require('./../models').Users;
var Vehicles = require('./../models').Vehicles;
var DriverLocations = require('./../models').DriverLocations;
var CustomerPickUpRequests = require('./../models').CustomerPickUpRequests;


//
driverRoute.get('/:driverId', function (req, res) {
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    var partnerId = userId;
    var driverId = req.params.driverId;


    Users.findAll({'where':{"status":"Y",userType:14}})
     .then(function(partnerData)
      {

        //this query not working
        Users.findAll({'where':{"partnerId":partnerId}})
        .then(function(resultDriverIds)
        {  

          var driverIdArr =[];
          var driverDataOptionArr = [];
          if(resultDriverIds.length){

            resultDriverIds.map(function(elm){

              driverIdArr.push(elm.id);

              driverDataOptionArr.push({'driverId':elm.id,'driverName':elm.name,'driverEmail':elm.email});

            });

          }

        DriverLocations.findAll({'where':{driverId:driverId}})
          .then(function(driverLocationData)
          { 
            var i =1;
            var driverLocationDataLength = driverLocationData.length;

            if(driverLocationDataLength){

              var dataArr = [];
            
                driverLocationData.map(function(elm){

              var driverId = elm.driverId;

              Users.find({'where':{"id":driverId}})
              .then(function(result)
              {
                  dataArr.push({'id':elm.id,'driverId':elm.driverId,'location':elm.location,'latitude':elm.latitude,
                        'longitude':elm.longitude,'isAssign':elm.isAssign,'driverName':result.name,'vehicleNumber':result.vehicleNumber,'driverMobile':result.mobile,'onDuty':result.onDuty});

              if(driverLocationDataLength==i)
                  {
                    var driverLocationData1 = JSON.stringify(dataArr);
                    res.render('track/partnersDriverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:partnerData,partnerId:partnerId,driverDataOptionArr:driverDataOptionArr,driverIdOption:driverId});
                  }
                  i++;

                  })
                  .catch(function(err)
                  {
                    var driverLocationData1=[];
                     req.flash('error', err);
                     res.render('track/partnersDriverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[],driverIdOption:driverId});
                  })
                })
              }else{
                res.render('track/partnersDriverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:[],partnerData:partnerData,partnerId:partnerId,driverDataOptionArr:driverDataOptionArr,driverIdOption:driverId});
              }
      })
      .catch(function(err)
      {
        var driverLocationData1=[];
         req.flash('error', err);
         res.render('track/partnersDriverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[],driverIdOption:driverId});
      })

    })
    .catch(function(err)
    {
      var driverLocationData1=[];
       req.flash('error', err);
       res.render('track/partnersDriverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[],driverIdOption:driverId});
    })
  })
    .catch(function(err)
    {
      var driverLocationData1=[];
       req.flash('error', err);
       res.render('track/partnersDriverLocation',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,driverLocationData:driverLocationData1,partnerData:[],partnerId:partnerId,driverDataOptionArr:[],driverIdOption:driverId});
    })
});







module.exports = driverRoute;
