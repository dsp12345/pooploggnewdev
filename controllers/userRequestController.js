var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var nodemailer = require('nodemailer');
var bulk = require('node-bulk-sms');
var request = require('request');
var FCM = require('fcm-push');


var userRequestRoute = require('express').Router();

var Users = require('./../models').Users;
var UserRequests = require('./../models').UserRequests;
var Areas = require('./../models').Areas;
var CustomerPickUpRequests = require('./../models').CustomerPickUpRequests;
var ApiUserRequests = require('./../models').ApiUserRequests;

var transporter = nodemailer.createTransport({
        host: 'server207.web-hosting.com',
        port: 465,
        secure: true,
        auth: {
            user: 'info@pooplogg.com',
            pass: '#pooplogg2020'
        }
      });

var serverKey = 'AIzaSyAcdysa4IDh_Mi_JaZUJA9Qxx-UnUFa2V0';
var fcm = new FCM(serverKey);


userRequestRoute.get('/mobileUserRequest', function (req, res) {
    console.log("driver approvedDrivers list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    ApiUserRequests.findAll({'where':{"isAccept":0}})
        .then(function(apiResult)
        {
          res.render('userRequest/mobileUserRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,apiResult:apiResult});
        })
      .catch(function(err)
      {
          req.flash('error',err);
          res.render('userRequest/createUserRequest',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,apiResult:[]});

      });
   
});

userRequestRoute.get('/', function (req, res) {
    console.log("driver approvedDrivers list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    Areas.findAll({'where':{"status":1}})
        .then(function(areaData)
        {
          res.render('userRequest/createUserRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,areaData:areaData});
        })
      .catch(function(err)
      {
          req.flash('error',err);
          res.render('userRequest/createUserRequest',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,areaData:[]});

      });
   
});


userRequestRoute.post('/', function (req, res) {


    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    //var refNo=Math.floor(Math.random()*9999999);

    var refNo = Math.floor(100000 + Math.random() * 900000);
    console.log(Math.floor(100000 + Math.random() * 900000));


    var dateTime = req.body.dateVal+' '+req.body.timeVal;


    var mailSendDateTime = new Date(dateTime).toString(); // "Sat Feb 14 2015 00:00:00 GMT-0500 (EST)"


    console.log(mailSendDateTime);


  var params = {
    refNo:refNo,
    title:req.body.title,
    name:req.body.name,
    email:req.body.email,
    mobile:req.body.mobile,
    numberOfTrips:req.body.numberOfTrips,
    areaCode:req.body.areaCode,
    address:req.body.address,
    status:'P',
    callCenterMgrId:userId,
    aboutPooplogg:req.body.aboutPooplogg,
    dateTime:dateTime

  };

  var email = req.body.email;

  UserRequests.create(params)
      .then(function(result){


        //sms code
         //stop sms /mail

        // var custMobile = req.body.mobile;
        // var custName = req.body.name;
        // var mailContent="Dear "+custName+", Your order has been successfully created. Please find below your order summary:<br></br><b>Service Summary</b><br></br> Evacuation details: "+mailSendDateTime+"";

        // var smsContent="Dear "+custName+", Your order has been successfully created. Please find below your order summary: Service Summary : Evacuation details: "+mailSendDateTime+"";

        // request({
        //               url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+smsContent+'.&flash=0&recipients='+custMobile+'',
        //               method: 'GET',
        //           }, function(error, response, body){
        //               if(error) {
        //                  req.flash('success', 'Order Request Successfully Created.');
        //                   res.redirect('/home');
        //               } else {

        //                 if(email){
              
        //                     var mailOptions = {
        //                               from: 'pooplogginfo@gmail.com',
        //                               to: req.body.email,
        //                               subject: 'PoopLogg:PickUp Request',
        //                               html: mailContent
        //                             };

        //                     transporter.sendMail(mailOptions, function(error, info){
        //                           if (error) {
        //                              req.flash('success', 'Order Request Successfully Created.');
        //                               res.redirect('/home');
        //                           } else {
        //                               req.flash('success', 'Order Request Successfully Created.');
        //                               res.redirect('/home');                     
        //                             }
        //                         });
        //                   }else{
        //                       req.flash('success', 'Order Request Successfully Created.');
        //                       res.redirect('/home');

        //                   }

        //               }//sms
        //             })//sms

        req.flash('success', 'Order Request Successfully Created.');
        res.redirect('/home');

       
    })
    .catch(function(err)
    {
        req.flash('error', 'User Request Could Not Be Added.');
        res.redirect('/home');
    })


});


userRequestRoute.get('/userRequestPendingList', function (req, res) {
    console.log("driver approvedDrivers list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;


    //UserRequests.findAll({'where':{"status":'P'}})
    UserRequests.findAll()
      .then(function(result)
      {

        var totalLength = result.length;
        var i=1;
        if(totalLength){

            result.map(function(elm){

                Users.find({'where':{"id":elm.callCenterMgrId}})
                  .then(function(users)
                  {

                  Users.find({'where':{"id":elm.managerId}})
                  .then(function(takenByMgr)
                  {
                    elm.callCenterMgrName= users.name;

                    if(takenByMgr)
                        elm.takenByMgrName=takenByMgr.name;
                      else
                        elm.takenByMgrName='';

                     if(totalLength==i){

                        res.render('userRequest/userRequestPendingList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

                     }
                     i++;

                  })
                  .catch(function(err)
                  {
                      var result=[];
                      req.flash('error',err);
                      res.render('userRequest/userRequestPendingList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});
                  })


                   })
                  .catch(function(err)
                  {
                      var result=[];
                      req.flash('error',err);
                      res.render('userRequest/userRequestPendingList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});
                  })

                })//map
          }else{
                      res.render('userRequest/userRequestPendingList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});

          }

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/userRequestPendingList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
    })

    
    //res.render('userRequest/createUserRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
   
});




userRequestRoute.get('/userPendingRequest', function (req, res) {
    console.log("userPendingRequest list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;


    UserRequests.findAll({'where':{status:{in:['PR','CO','AC']},managerId:userId}})
    .then(function(checkRequest)
      {
        var alreadyTakingRequest = checkRequest.length;

        console.log("checkRequest");

        console.log(alreadyTakingRequest);


    UserRequests.findAll({'where':{"status":'P'}})
      .then(function(result)
      {

        var totalLength = result.length;
        var i=1;
        if(totalLength){

            result.map(function(elm){

                Users.find({'where':{"id":elm.callCenterMgrId}})
                  .then(function(users)
                  {
                    elm.callCenterMgrName= users.name;

                     if(totalLength==i){

                        res.render('userRequest/userPendingRequestList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr,alreadyTakingRequest:alreadyTakingRequest});

                     }
                     i++;


                   })
                  .catch(function(err)
                  {
                      req.flash('error',err);
                      res.render('userRequest/userPendingRequestList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr,alreadyTakingRequest:alreadyTakingRequest});
                  })

                })//map
          }else{
            res.render('userRequest/userPendingRequestList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr,alreadyTakingRequest:alreadyTakingRequest});

          }



      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/userPendingRequestList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,result:result,alreadyTakingRequest:''});
    })

     })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/userPendingRequestList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,result:result,alreadyTakingRequest:''});
    })

    
    //res.render('userRequest/createUserRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
   
});


userRequestRoute.get('/userSuspendedRequest', function (req, res) {
    console.log("userSuspendedRequest list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    UserRequests.findAll({'where':{"status":'H'}})
      .then(function(result)
      {
            res.render('userRequest/userSuspendedRequestList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/userSuspendedRequestList',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
    })

    
    //res.render('userRequest/createUserRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
   
});



//Pending Flow
userRequestRoute.get('/assignDriver/:pickUpId', function (req, res) {
 // userRequestRoute.get('/assignDriver/:pickUpId/:areaCode', function (req, res) {


    console.log("assignDriver11");

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var pickUpId = req.params.pickUpId;
    //var areaCode = req.params.areaCode;
    var menuArr = ssn.menuArr;

    UserRequests.find({'where':{"id":pickUpId}})
        .then(function(pickUpRequestData)
        { 

            Areas.findAll({'where':{"status":1}})
            .then(function(areaData)
            {

              res.render('customer/customerDriverAssign',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driversData:[],areaData:areaData,pickUpRequestData:pickUpRequestData,pickUpId:pickUpId,menuArr:menuArr});

              //driver data

              // Users.findAll({'where':{"userType":7,status:'Y'}})
              // //Users.findAll({'where':{"userType":7,"areaCode":areaCode,status:'Y'}})
              //     .then(function(driversData)
              //     { 

              //         res.render('customer/customerDriverAssign',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driversData:driversData,pickUpRequestData:pickUpRequestData,pickUpId:pickUpId,menuArr:menuArr});
              //     })
              //     .catch(function(err)
              //     {
              //         console.log("errrrrrrr");
              //         console.log(err);
              //         res.render('customer/customerDriverAssign',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driversData:[],pickUpRequestData:[],pickUpId:pickUpId,menuArr:menuArr});
              //     })//driversData
              })
              .catch(function(err)
              {
                  console.log("errrrrrrr");
                  console.log(err);
                  res.render('customer/customerDriverAssign',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driversData:[],areaData:[],pickUpRequestData:[],pickUpId:pickUpId,menuArr:menuArr});
              })//areaData
        })
        .catch(function(err)
        {
            console.log("errrrrrr222222222r");

            console.log(err);
            res.render('customer/customerDriverAssign',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,driversData:[],areaData:[],pickUpRequestData:[],pickUpId:pickUpId,menuArr:menuArr});
        })//customerData

   

});




userRequestRoute.get('/acceptRequest/:pickUpId/:status', function (req, res) {

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var pickUpId = req.params.pickUpId;
    var areaCode = req.params.areaCode;
    var menuArr = ssn.menuArr;

    UserRequests.find({'where':{"id":pickUpId}})
        .then(function(pickUpRequestData)
        { 
           
            res.render('userRequest/userRequestProcessing',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,pickUpRequestData:pickUpRequestData,pickUpId:pickUpId,menuArr:menuArr});
               
        })
        .catch(function(err)
        {
            console.log(err);
            res.render('userRequest/userRequestProcessing',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,pickUpRequestData:[],pickUpId:pickUpId,menuArr:menuArr});
        })//customerData

   

});




userRequestRoute.post('/customerFare', function (req, res) {

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    
    var menuArr = ssn.menuArr;

    var pickUpId = req.body.pickUpId;
    var amount = req.body.amount;
    var paymentType = '';
    var status = 'AC';

    UserRequests.find({'where':{'id':pickUpId}})
      .then(function(pickUpData)
      { 

      UserRequests.update({'status':'AC','amount':amount,'managerId':userId,paymentType:paymentType},{'where':{'id':pickUpId}})
          .then(function(UserRequestsData){

        var custMobile = pickUpData.mobile;
        var custName = pickUpData.name;
        var email = pickUpData.email;
        var numberOfTrips = pickUpData.numberOfTrips;

        Users.find({'where':{'email':email}})
        .then(function(usersData)
        { 

          var deviceToken ='';
            if(usersData)
              deviceToken = usersData.deviceId;

          //var deviceToken = usersData.deviceId;

          console.log(deviceToken);

          if(deviceToken){
            var messageText = "Your order today costs ₦ "+amount+". Kindly expect a call  to confirm mode of payment.Best regards,The PoopLogg Team";
            //var messageText="Dear "+custName+", Thank you for placing an order.The service cost  ₦ "+amount+" For "+numberOfTrips+" trip."; 

            var title = 'Thank you for placing order';
            sendPushNotification(deviceToken,messageText,title);
          }

         //var mailContent = "Dear "+custName+", Your order today costs ₦ "+amount+".Kindly expect a call  to confirm mode of payment.Best regards,The PoopLogg Team";

         var mailContent = "Your order today costs ₦ "+amount+". Kindly expect a call  to confirm mode of payment.Best regards,The PoopLogg Team.";
         var smsContent = "Your order today costs "+amount+" Naira. Kindly expect a call  to confirm mode of payment.Best regards,The PoopLogg Team";

        request({
                      url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+smsContent+'.&flash=0&recipients='+custMobile+'',
                      method: 'GET',
                  }, function(error, response, body){
                      if(error) {
                         req.flash('success', 'User Request has been approved Successfully.');
                              res.redirect('/userRequest/managerAcceptedRequest');
                      } else {

                        if(email){
              
                            var mailOptions = {
                                      from: 'info@pooplogg.com',
                                      to: email,
                                      subject: 'PoopLogg:PickUp Request Approved',
                                      html: mailContent
                                    };

                            transporter.sendMail(mailOptions, function(error, info){
                                  if (error) {
                                     req.flash('success', 'User request has been approved Successfully.');
                                     res.redirect('/home');
                                      //res.redirect('/userRequest/managerAcceptedRequest');
                                  } else {
                                     req.flash('success', 'User request has been approved Successfully.');
                                      res.redirect('/home');
                                      }
                                });
                          }else{
                              req.flash('success', 'User request has been approved Successfully.');
                              res.redirect('/home');

                          }

                      }//sms
                    })//sms



           })
        .catch(function(err)
        {
            console.log(err);
            console.log("first");
            res.redirect('/userRequest/acceptRequest/'+pickUpId+'/'+status);

            
            //res.render('userRequest/userRequestProcessing',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,pickUpRequestData:[],pickUpId:pickUpId,menuArr:menuArr});
        })//customerData

        })
        .catch(function(err)
        {
            console.log(err);
            console.log("first");
            res.redirect('/userRequest/acceptRequest/'+pickUpId+'/'+status);

            
            //res.render('userRequest/userRequestProcessing',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,pickUpRequestData:[],pickUpId:pickUpId,menuArr:menuArr});
        })//customerData

        })
        .catch(function(err)
        {
            console.log(err);

            console.log("ending");
            res.redirect('/userRequest/acceptRequest/'+pickUpId+'/'+status);

            
            //res.render('userRequest/userRequestProcessing',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,pickUpRequestData:[],pickUpId:pickUpId,menuArr:menuArr});
        })//customerData

   

});




userRequestRoute.get('/paymentType/:pickUpId/:paymentType', function (req, res) {

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    
    var menuArr = ssn.menuArr;

    var pickUpId = req.params.pickUpId;
    var paymentType = req.params.paymentType;

    UserRequests.find({'where':{'id':pickUpId}})
      .then(function(pickUpData)
      { 

      UserRequests.update({paymentType:paymentType},{'where':{'id':pickUpId}})
          .then(function(UserRequestsData){


            Users.find({'where':{'email':pickUpData.email}})
            .then(function(usersData)
            { 

        var custMobile = pickUpData.mobile;
        var custName = pickUpData.name;
         var email = pickUpData.email;
         var amount = pickUpData.amount;
         
         if(paymentType=='cash'){

                   var smsContent = "You have selected cash on delivery. Kindly pay cash to the truck driver.Best regards,The PoopLogg Team";
                   var notificationContent = "You have selected cash on delivery. Kindly pay cash to the truck driver.Best regards,The PoopLogg Team.";
                   var mailContent = "You have selected cash on delivery. Kindly pay cash to the truck driver.Best regards,The PoopLogg Team.";

                    //var smsContent= "Dear "+custName+", You have selected cash on delivery. Please make a payment of "+amount+" Naira to the truck driver after the service has been completed. Call 0700 0700211 for any question PoopLogg";
                    //var notificationContent="Dear "+custName+", You have selected cash on delivery. Please make a payment of ₦ "+amount+" to the truck driver after the service has been completed. Call 0700 0700211 for any question PoopLogg";
                    //var mailContent = "Dear "+custName+", You have selected cash on delivery. Please make a payment of ₦ "+amount+" to the truck driver after the service has been completed. Call 0700 0700211 for any question PoopLogg";
         }else if(paymentType=='card'){

          if(1==1){
                 
                var smsContent="Your pickup request has been approved and the fare charges of your recent trip is "+amount+' Naira Please pay your amount click on this "http://admin.pooplogg.com/payment/'+pickUpId+'/'+amount+'"';
                var notificationContent="Your pickup request has been approved and the fare charges of your recent trip is ₦ "+amount+ 'Please pay your amount click on this "http://admin.pooplogg.com/payment/'+pickUpId+'/'+amount+'"';
                var mailContent="Your pickup request has been approved and the fare charges of your recent trip is ₦ "+amount+' Please pay your amount click on this <a href="http://admin.pooplogg.com/payment/'+pickUpId+'/'+amount+'">Payment</a>.</br>';
              }
              else{
                  var smsContent="Your pickup request has been approved and the fare charges of your recent trip is "+amount+' Naira Please pay your amount click on this <a href="http://admin.pooplogg.com/payment/'+pickUpId+'/'+amount+'">Payment</a>.</br>';

                  var notificationContent="Your pickup request has been approved and the fare charges of your recent trip is ₦ "+amount+' Please pay your amount click on this <a href="http://admin.pooplogg.com/payment/'+pickUpId+'/'+amount+'">Payment</a>.</br>';
                  var mailContent="Your pickup request has been approved and the fare charges of your recent trip is ₦ "+amount+' Please pay your amount click on this <a href="http://localhost:8002/payment/'+pickUpId+'/'+amount+'">Payment</a>.</br>';
                }

         }else{ //Bank transfer

                var smsContent = "Please make payment of "+amount+" Naira to:Account Number: 0257911356 Bank: Guaranty Trust Bank (GTB).Best regards,The PoopLogg Team";
                var notificationContent = "Please make payment of ₦ "+amount+" to:Account Number: 0257911356 Bank: Guaranty Trust Bank (GTB). Best regards,The PoopLogg Team.";
                var mailContent = "Please make payment of ₦ "+amount+" to:Account Number: 0257911356 <br></br>Bank: Guaranty Trust Bank (GTB).<br></br>Best regards,<br></br>The PoopLogg Team.";
/*
                var smsContent= "Dear "+custName+", Please make  payment of  "+amount+' Naira into the below account: Account Number: 0257911356 Bank: Guarantee Trust Bank (GTB) Call 0700 0700211 for any questions PoopLogg';
                var notificationContent="Hi "+custName+", Your pickup request has been approved and the fare charges of your recent trip is ₦ "+amount+' Please pay your amount in account.</br>';
                var mailContent="Dear "+custName+", Your pickup request has been approved and the fare charges of your recent trip is ₦ "+amount+' Please pay your amount in account.</br>';
*/
         }
         


         //var deviceToken = usersData.deviceId;
         var deviceToken ='';
            if(usersData)
              deviceToken = usersData.deviceId;

          console.log(deviceToken);

          if(deviceToken){
            var messageText = notificationContent;
            var title = 'Your pickup request has been approved';
            sendPushNotification(deviceToken,messageText,title);
          }


        request({
                      url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+smsContent+'.&flash=0&recipients='+custMobile+'',
                      method: 'GET',
                  }, function(error, response, body){
                      if(error) {
                         req.flash('success', 'User Request Send To Fullfillment Manager.');
                              //res.redirect('/userRequest/userRequestPendingList');
                              res.redirect('/home');
                      } else {

                        if(email){
              
                            var mailOptions = {
                                      from: 'info@pooplogg.com',
                                      to: email,
                                      subject: 'PoopLogg:PickUp Request',
                                      html: mailContent
                                    };

                            transporter.sendMail(mailOptions, function(error, info){
                                  if (error) {
                                     req.flash('success', 'User Request Send To Fullfillment Manager.');
                                      res.redirect('/home');
                                  } else {
                                      req.flash('success', 'User Request Send To Fullfillment Manager.');
                                      res.redirect('/home');
                                      }
                                });
                          }else{
                              req.flash('success', 'User Request Send To Fullfillment Manager.');
                              res.redirect('/home');

                          }

                      }//sms
                    })//sms



            })
          .catch(function(err)
          {
              console.log(err);
              req.flash('success', 'Invalid User.');

              res.redirect('/home');

              
              //res.render('userRequest/userRequestProcessing',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,pickUpRequestData:[],pickUpId:pickUpId,menuArr:menuArr});
          })//customerData
        })
        .catch(function(err)
        {
            console.log(err);
            req.flash('success', 'User Request Could Not Be Sent To Fullfillment Manager.');

            res.redirect('/home');

            
            //res.render('userRequest/userRequestProcessing',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,pickUpRequestData:[],pickUpId:pickUpId,menuArr:menuArr});
        })//customerData

        })
        .catch(function(err)
        {
            console.log(err);
            req.flash('success', 'User Request Could Not Be Sent To Fullfillment Manager.');
            res.redirect('/home');

            
            //res.render('userRequest/userRequestProcessing',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,pickUpRequestData:[],pickUpId:pickUpId,menuArr:menuArr});
        })//customerData

   

});






userRequestRoute.get('/managerAcceptedRequest', function (req, res) {
    console.log("userSuspendedRequest list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    UserRequests.findAll({'where':{"status":'AC'}})
      .then(function(result)
      {
        var totalLength = result.length;
        var i=1;
        if(totalLength){

            result.map(function(elm){

                Users.find({'where':{"id":elm.managerId}})
                  .then(function(users)
                  {

                    Users.find({'where':{"id":elm.callCenterMgrId}})
                      .then(function(callcenter)
                      {
                    elm.callCenterMgrName= callcenter.name;

                    elm.takenByMgrName =users.name;

                     if(totalLength==i){

                        res.render('userRequest/managerAcceptedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

                     }
                     i++;

                  })
                  .catch(function(err)
                  {
                      req.flash('error',err);
                      res.render('userRequest/managerAcceptedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});
                  })


                   })
                  .catch(function(err)
                  {
                      req.flash('error',err);
                      res.render('userRequest/managerAcceptedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});
                  })

                })//map
          }else{
              res.render('userRequest/managerAcceptedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});

          }

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/managerAcceptedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
    })

    
    //res.render('userRequest/createUserRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
   
});





userRequestRoute.get('/action/:pickUpId/:status',function(req,res){

        ssn = req.session;
        var accesstoken = ssn.accesstoken;
        var username = ssn.username;
        var userType = ssn.userType;
        var userId = ssn.userId;
        var ssnprofileImage = ssn.profileImage;
        var menuArr = ssn.menuArr;

        var pickUpId = req.params.pickUpId;
        var status = req.params.status;

        UserRequests.find({'where':{'id':pickUpId}})
          .then(function(customerData)
          { 
            var custEmail = customerData.email;

            Users.find({'where':{"email":custEmail}})
              .then(function(usersData)
              {

            var paymentStatus ='';
            // if(status=='COMPL'){
            //    paymentStatus ='Paid';
            // }

          //UserRequests.update({'status':status,'paymentStatus':paymentStatus},{'where':{'id':pickUpId}})
          UserRequests.update({'status':status,'paymentStatus':paymentStatus},{'where':{'id':pickUpId}})
            .then(function(data){

              var custName = customerData.name;
              var custEmail = customerData.email;
              var custMobile = customerData.mobile;
                
                if(status=='P'){
                  req.flash('success', 'Customer Request Resumed Successfully.');
                  res.redirect('/userRequest/userPendingRequest');

                }else if(status=='R'){

                 // var mailContent="Hi "+custName+", Your pickup request cancelled.";

                  var mailContent="Your order has been successfully cancelled. Thank you for choosing PoopLogg. Best regards,<br></br>The PoopLogg Team";
                  var smsContent="Your order has been successfully cancelled. Thank you for choosing PoopLogg. Best regards,The PoopLogg Team";


                  var deviceToken ='';
                  if(usersData)
                    deviceToken = usersData.deviceId;

                   //var deviceToken = usersData.deviceId;

                  console.log(deviceToken);

                  if(deviceToken){
                    var messageText = mailContent;
                    var title = 'Your pickup request cancelled';
                    sendPushNotification(deviceToken,messageText,title);
                  }


                  request({
                            url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+smsContent+'.&flash=0&recipients='+custMobile+'',
                            method: 'GET',
                        }, function(error, response, body){
                            if(error) {
                               req.flash('error', 'Customer Request Cancelled Successfully.');
                              res.redirect('/home');
                              //res.redirect('/userRequest/rejectedRequest');
                            } else {

                              if(custEmail){
                    
                                  var mailOptions = {
                                            from: 'info@pooplogg.com',
                                            to: custEmail,
                                            subject: 'PoopLogg:PickUp Request Cancelled',
                                            html: mailContent
                                          };

                                  transporter.sendMail(mailOptions, function(error, info){
                                        if (error) {
                                           req.flash('error', 'Customer Request Cancelled Successfully.');
                                            res.redirect('/home');
                                        } else {
                                            req.flash('error', 'Customer Request Cancelled Successfully.');
                                            res.redirect('/home');                  }
                                      });
                                }else{
                                    req.flash('error', 'Customer Request Cancelled Successfully.');
                                    res.redirect('/home');

                                }

                            }//sms
                          })//sms





                  // req.flash('error', 'Customer Request Cancelled Successfully.');
                  // res.redirect('/userRequest/rejectedRequest');
                    
                }else if(status=='COMPL'){
                  var mailContent="Hi "+custName+", Your pickup request successfully completed.Thank you.";


                request({
                            url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+mailContent+'.&flash=0&recipients='+custMobile+'',
                            method: 'GET',
                        }, function(error, response, body){
                            if(error) {
                               req.flash('success', 'Customer Request Completed Successfully.');
                                res.redirect('/userRequest/userCompletedRequest');
                            } else {

                              if(custEmail){
                    
                                  var mailOptions = {
                                            from: 'info@pooplogg.com',
                                            to: custEmail,
                                            subject: 'PoopLogg:PickUp Completed',
                                            html: mailContent
                                          };

                                  transporter.sendMail(mailOptions, function(error, info){
                                        if (error) {
                                           req.flash('success', 'Customer Request Completed Successfully.');
                                            res.redirect('/userRequest/userCompletedRequest');
                                        } else {
                                            req.flash('success', 'Customer Request Completed Successfully.');
                                            res.redirect('/userRequest/userCompletedRequest');                      }
                                      });
                                }else{
                                    req.flash('success', 'Customer Request Completed Successfully.');
                                    res.redirect('/userRequest/userCompletedRequest');

                                }

                            }//sms
                          })//sms
                }
                else {
                  req.flash('error', 'Customer Request Suspended Successfully.');
                  res.redirect('/userRequest/userPendingRequest');
                }

              })
            .catch(function(err)
            {
                
                if(status=='P'){
                  req.flash('error', 'Customer Request Could Not Be Resume.');
                  res.redirect('/userRequest/userPendingRequest');

                }else if(status=='COMPL'){
                  req.flash('error', 'Customer Request  Could Not Be Completed.');
                  res.redirect('/userRequest/confirmed');
                }else{
                  req.flash('error', 'Customer Request Could Not Be Suspend.');
                  res.redirect('/userRequest/userPendingRequest');
                }

            });


        })
        .catch(function(err)
        {
              req.flash('error', 'Invalid User.');
              res.redirect('/home');
            
        });
        


        })
        .catch(function(err)
        {

          if(status=='P'){
              req.flash('error', 'Customer Request Could Not Be Resume.');
              res.redirect('/userRequest/userPendingRequest');

            }else if(status=='COMPL'){
              req.flash('error', 'Customer Request  Could Not Be Completed.');
              res.redirect('/userRequest/confirmed');
            }else{
              req.flash('error', 'Customer Request Could Not Be Suspend.');
              res.redirect('/userRequest/userPendingRequest');
            }
        });
        
});



userRequestRoute.get('/execute/:pickUpId/:status',function(req,res){

        ssn = req.session;
        var accesstoken = ssn.accesstoken;
        var username = ssn.username;
        var userType = ssn.userType;
        var userId = ssn.userId;
        var ssnprofileImage = ssn.profileImage;
        var menuArr = ssn.menuArr;

        var pickUpId = req.params.pickUpId;
        var status = req.params.status;

        var paymentStatus='Paid';

        UserRequests.find({'where':{'id':pickUpId}})
          .then(function(customerData)
          {            

            if(customerData){
              
               var custName = customerData.name;
              var custEmail = customerData.email;
              var custMobile = customerData.mobile;

              Users.find({'where':{"email":custEmail}})
              .then(function(usersData)
              {

               // var mailContent="Hi "+custName+", Your pickup request successfully executed.Thank you.";
                var mailContent="Dear "+custName+", Your order has been successfully executed.Thank you for choosing The PoopLogg Team.";

                var smsContent="Dear "+custName+", Your order has been successfully executed.Thank you for choosing The PoopLogg Team";

                var deviceToken ='';
              if(usersData)
                deviceToken = usersData.deviceId;

               //var deviceToken = usersData.deviceId;

              console.log(deviceToken);

              if(deviceToken){
                var messageText = mailContent;
                var title = 'Your pickup request has been approved';
                sendPushNotification(deviceToken,messageText,title);
              }

            // UserRequests.update({'status':status},{'where':{'id':pickUpId}})
            //   .then(function(data){

                UserRequests.update({'status':status,'paymentStatus':paymentStatus},{'where':{'id':pickUpId}})
                  .then(function(data){
                 


                  request({
                              url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+smsContent+'.&flash=0&recipients='+custMobile+'',
                              method: 'GET',
                          }, function(error, response, body){
                              if(error) {
                                 req.flash('success', 'Customer Request Executed Successfully.');
                                  res.redirect('/home');
                              } else {

                                if(custEmail){
                      
                                    var mailOptions = {
                                              from: 'info@pooplogg.com',
                                              to: custEmail,
                                              subject: 'PoopLogg:PickUp Executed',
                                              html: mailContent
                                            };

                                    transporter.sendMail(mailOptions, function(error, info){
                                          if (error) {
                                             req.flash('success', 'Customer Request Executed Successfully.');
                                              res.redirect('/home');
                                          } else {
                                              req.flash('success', 'Customer Request Executed Successfully.');
                                              res.redirect('/home');                   }
                                        });
                                  }else{
                                      req.flash('success', 'Customer Request Executed Successfully.');
                                      res.redirect('/home');

                                  }

                              }//sms
                            })//sms


                  })
              .catch(function(err)
              {

                console.log(err);
                  
                  req.flash('error', 'Invalid User.');
                  res.redirect('/home');

              });
                 

                })
              .catch(function(err)
              {
                console.log(err);
                  
                  req.flash('error', 'Customer Request  Could Not Be Executed.');
                  res.redirect('/home');

              });

               
            }else{

              req.flash('error', 'Customer Request  PickUp Id Not Found.');
                  res.redirect('/home');

            }

        })
        .catch(function(err)
        {

               req.flash('error', 'Customer Request  Could Not Be Executed.');
                res.redirect('/home');
            
        });
        
});




userRequestRoute.get('/pauseResume/:pickUpId/:status',function(req,res){

        ssn = req.session;
        var accesstoken = ssn.accesstoken;
        var username = ssn.username;
        var userType = ssn.userType;
        var userId = ssn.userId;
        var ssnprofileImage = ssn.profileImage;
        var menuArr = ssn.menuArr;

        var pickUpId = req.params.pickUpId;
        var status = req.params.status;

        if(status=='PS'){
          var messgage = 'Customer Request Suspended Successfully.';
          var statusText = 'Suspended';
        }else{
          var messgage = 'Customer Request Resumed Successfully.';
          var statusText = 'Resumed';

        }

        UserRequests.find({'where':{'id':pickUpId}})
          .then(function(customerData)
          { 

          UserRequests.update({'status':status},{'where':{'id':pickUpId}})
            .then(function(data){
                  //req.flash('error', messgage);
                  //res.redirect('/userRequest/userRequestPendingList');

                  var custName = customerData.name;
                  var custMobile = customerData.mobile;
                  var custEmail = customerData.email;

                  var mailContent="Hi "+custName+", Your pickup request successfully "+statusText+".Please contact to pooplogg.";

                      request({
                                  url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+mailContent+'.&flash=0&recipients='+custMobile+'',
                                  method: 'GET',
                              }, function(error, response, body){
                                  if(error) {
                                     req.flash('error', messgage);
                                      res.redirect('/home');
                                      //res.redirect('/userRequest/userRequestPendingList');
                                  } else {

                                    if(custEmail){
                          
                                        var mailOptions = {
                                                  from: 'info@pooplogg.com',
                                                  to: custEmail,
                                                  subject: 'PoopLogg:PickUp Request '+statusText,
                                                  html: mailContent
                                                };

                                        transporter.sendMail(mailOptions, function(error, info){
                                              if (error) {
                                                 req.flash('error', messgage);
                                                  res.redirect('/home');
                                              } else {
                                                 req.flash('error', messgage);
                                                  res.redirect('/home');                  }
                                            });
                                      }else{
                                          req.flash('error', messgage);
                                          res.redirect('/home');

                                      }

                                  }//sms
                                })//sms
              })
            .catch(function(err)
            {
                req.flash('error', 'Customer Request Could Not Be '+statusText+'.');
                res.redirect('/home');
            });
        })
        .catch(function(err)
        {
            req.flash('error', 'Customer Request Could Not Be '+statusText+'.');
            res.redirect('/home');
        });
        
});




userRequestRoute.get('/cancelRequest/:pickUpId/:status',function(req,res){

        ssn = req.session;
        var accesstoken = ssn.accesstoken;
        var username = ssn.username;
        var userType = ssn.userType;
        var userId = ssn.userId;
        var ssnprofileImage = ssn.profileImage;
        var menuArr = ssn.menuArr;

        var pickUpId = req.params.pickUpId;
        var status = req.params.status;

        var messgage = 'Customer Request Cancelled Successfully.';

        UserRequests.find({'where':{'id':pickUpId}})
          .then(function(customerData)
          { 

          UserRequests.update({'status':status},{'where':{'id':pickUpId}})
            .then(function(data){
                  //req.flash('error', messgage);
                  //res.redirect('/userRequest/userRequestPendingList');

                  var custName = customerData.name;
                  var custMobile = customerData.mobile;
                  var custEmail = customerData.email;

                  //var mailContent="Dear "+custName+", Your pickup request successfully cancelled.Please contact to pooplogg.";
                  //var smsContent="Dear "+custName+", Your order has been successfully cancelled. Thank you for choosing PoopLogg.";"

                  var mailContent="Dear "+custName+", Your order has been successfully cancelled.Thank you for choosing PoopLogg.Best regards,The PoopLogg Team.";
                  var smsContent="Dear "+custName+", Your order has been successfully cancelled.Thank you for choosing PoopLogg.Best regards,The PoopLogg Team";

                      request({
                                  url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+smsContent+'.&flash=0&recipients='+custMobile+'',
                                  method: 'GET',
                              }, function(error, response, body){
                                  if(error) {
                                     req.flash('error', messgage);
                                      res.redirect('/home');
                                      //res.redirect('/userRequest/userRequestPendingList');
                                  } else {

                                    if(custEmail){
                          
                                        var mailOptions = {
                                                  from: 'info@pooplogg.com',
                                                  to: custEmail,
                                                  subject: 'PoopLogg:PickUp Request Cancelled',
                                                  html: mailContent
                                                };

                                        transporter.sendMail(mailOptions, function(error, info){
                                              if (error) {
                                                 req.flash('error', messgage);
                                                  res.redirect('/home');
                                              } else {
                                                 req.flash('error', messgage);
                                                  res.redirect('/home');                }
                                            });
                                      }else{
                                          req.flash('error', messgage);
                                          res.redirect('/home');

                                      }

                                  }//sms
                                })//sms
              })
            .catch(function(err)
            {
                req.flash('error', 'Customer Request Could Not Be Cancelled');
                res.redirect('/home');
            });
        })
        .catch(function(err)
        {
            req.flash('error', 'Customer Request Could Not Be Cancelled.');
            res.redirect('/home');
        });
        
});





userRequestRoute.get('/getMobileAboutPoopLoggInfo/:pickUpId',function(req,res){

        ssn = req.session;
        var accesstoken = ssn.accesstoken;
        var username = ssn.username;
        var userType = ssn.userType;
        var userId = ssn.userId;
        var ssnprofileImage = ssn.profileImage;
        var menuArr = ssn.menuArr;

        var pickUpId = req.params.pickUpId;
        var status = req.params.status;
        var dataArr =[];
        ApiUserRequests.find({'where':{'id':pickUpId}})
          .then(function(customerData)
          { 
            
              var aboutPooplogg = customerData.aboutPooplogg;

              dataArr.push({'aboutPooplogg':aboutPooplogg});

              res.send(dataArr);

             
        })
          .catch(function(err)
        {
             res.send(dataArr);
        });

})

userRequestRoute.get('/getCustomerInfo/:pickUpId',function(req,res){

        ssn = req.session;
        var accesstoken = ssn.accesstoken;
        var username = ssn.username;
        var userType = ssn.userType;
        var userId = ssn.userId;
        var ssnprofileImage = ssn.profileImage;
        var menuArr = ssn.menuArr;

        var pickUpId = req.params.pickUpId;
        var status = req.params.status;
        var dataArr =[];
        UserRequests.find({'where':{'id':pickUpId}})
          .then(function(customerData)
          { 
            var managerId = customerData.managerId;
            var callCenterMgrId = customerData.callCenterMgrId;
            var driverId = customerData.driverId;

              Users.find({'where':{"id":callCenterMgrId}})
                  .then(function(takenByMgr)
                  {
                    

                    Users.find({'where':{"id":managerId}})
                      .then(function(users)
                      {

                        Users.find({'where':{"id":driverId}})
                        .then(function(driversData)
                        {
                          var takenByMgrName='';
                          if(users){
                             takenByMgrName = users.name;
                          }

                          var driverName='';
                          var driverMobile ='';
                          if(driversData){
                             driverName = driversData.name;
                             driverMobile = driversData.mobile;
                          }

                          var name = customerData.name;
                          var title = customerData.title;
                          var email = customerData.email;
                          var areaCode = customerData.areaCode;
                          var address = customerData.address;
                          var createdByMgrName = takenByMgr.name;
                          var paymentType = customerData.paymentType;
                          var paymentStatus = customerData.paymentStatus;
                          var amount = customerData.amount;
                          var dateTime = customerData.dateTime;
                          var numberOfTrips = customerData.numberOfTrips;
                          var aboutPooplogg = customerData.aboutPooplogg;

                          dataArr.push({'title':title,'name':name,'email':email,'areaCode':areaCode,'address':address,'createdByMgrName':createdByMgrName,'takenByMgrName':takenByMgrName,'paymentType':paymentType,
                                      'amount':amount,'driverName':driverName,'driverMobile':driverMobile,'dateTime':dateTime,'numberOfTrips':numberOfTrips,'paymentStatus':paymentStatus,'aboutPooplogg':aboutPooplogg});

                          res.send(dataArr);

                          })
                          .catch(function(err)
                        {
                            //var customerData=[];
                             res.send(dataArr);
                        });
                      })
                        .catch(function(err)
                      {
                          //var customerData=[];
                           res.send(dataArr);
                      });
                })
                  .catch(function(err)
                {
                    //var customerData=[];
                     res.send(dataArr);
                });


            //})

             
        })
          .catch(function(err)
        {
             res.send(dataArr);
        });

})



userRequestRoute.get('/getCustomerMobileInfo/:pickUpId',function(req,res){

        ssn = req.session;
        var accesstoken = ssn.accesstoken;
        var username = ssn.username;
        var userType = ssn.userType;
        var userId = ssn.userId;
        var ssnprofileImage = ssn.profileImage;
        var menuArr = ssn.menuArr;

        var pickUpId = req.params.pickUpId;
        var dataArr =[];
        ApiUserRequests.find({'where':{'id':pickUpId}})
          .then(function(customerData)
          { 
            

                  var name = customerData.name;
                  var email = customerData.email;
                  var areaCode = customerData.areaCode;
                  var address = customerData.address;
                  var createdByMgrName = '';
                  var paymentType = customerData.paymentType;
                  var amount = customerData.amount;
                  var driverName ='';
                  var driverMobile = '';
                  var takenByMgrName = '';
                  var paymentType = '';
                  var dateTime = customerData.dateTime;
                   var numberOfTrips = customerData.numberOfTrips;

                  dataArr.push({'name':name,'email':email,'areaCode':areaCode,'address':address,'createdByMgrName':createdByMgrName,'takenByMgrName':takenByMgrName,'paymentType':paymentType,
                              'amount':amount,'driverName':driverName,'driverMobile':driverMobile,'dateTime':dateTime,'numberOfTrips':numberOfTrips});

                  res.send(dataArr);

             
        })
          .catch(function(err)
        {
             res.send(dataArr);
        });

})


userRequestRoute.get('/abandonedRequest/:pickUpId/:status',function(req,res){

        ssn = req.session;
        var accesstoken = ssn.accesstoken;
        var username = ssn.username;
        var userType = ssn.userType;
        var userId = ssn.userId;
        var ssnprofileImage = ssn.profileImage;
        var menuArr = ssn.menuArr;

        var pickUpId = req.params.pickUpId;
        var status = req.params.status;

        UserRequests.find({'where':{'id':pickUpId}})
          .then(function(customerData)
          { 

          UserRequests.update({'status':status},{'where':{'id':pickUpId}})
            .then(function(data){
                  req.flash('error', 'Customer Request Abandoned Successfully.');
                  res.redirect('/userRequest/abandonedRequest');

                  var custName = customerData.name;

                  var custMobile = customerData.mobile;
                  var custEmail = customerData.email;

                  var mailContent="Hi "+custName+", Your pickup request successfully abandoned.Please contact to pooplogg.";

                      request({
                                  url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+mailContent+'.&flash=0&recipients='+custMobile+'',
                                  method: 'GET',
                              }, function(error, response, body){
                                  if(error) {
                                     req.flash('error', 'Customer Request Abandoned Successfully.');
                                      res.redirect('/userRequest/abandonedRequest');
                                  } else {

                                    if(custEmail){
                          
                                        var mailOptions = {
                                                  from: 'info@pooplogg.com',
                                                  to: custEmail,
                                                  subject: 'PoopLogg:PickUp Request Abandoned',
                                                  html: mailContent
                                                };

                                        transporter.sendMail(mailOptions, function(error, info){
                                              if (error) {
                                                 req.flash('error', 'Customer Request Abandoned Successfully.');
                                                  res.redirect('/userRequest/abandonedRequest');
                                              } else {
                                                 req.flash('error', 'Customer Request Abandoned Successfully.');
                                                  res.redirect('/userRequest/abandonedRequest');                    }
                                            });
                                      }else{
                                          req.flash('error', 'Customer Request Abandoned Successfully.');
                                          res.redirect('/userRequest/abandonedRequest');

                                      }

                                  }//sms
                                })//sms
              })
            .catch(function(err)
            {
                req.flash('error', 'Customer Request Could Not Be Abandoned.');
                res.redirect('/userRequest/abandonedRequest');
            });
        })
        .catch(function(err)
        {
            req.flash('error', 'Customer Request Could Not Be Abandoned.');
            res.redirect('/userRequest/abandonedRequest');
        });
        
});




userRequestRoute.get('/abandonedRequest', function (req, res) {
    console.log("abandonedRequest list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    UserRequests.findAll({'where':{"status":'AB'}})
      .then(function(result)
      {

        var totalLength = result.length;
        var i=1;
        if(totalLength){

            result.map(function(elm){

                Users.find({'where':{"id":elm.managerId}})
                  .then(function(users)
                  {
                    elm.takenByMgrName= users.name;

                     if(totalLength==i){

                        res.render('userRequest/abandonedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

                     }
                     i++;


                   })
                  .catch(function(err)
                  {
                      req.flash('error',err);
                      res.render('userRequest/abandonedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});
                  })

                })//map
          }else{
            res.render('userRequest/abandonedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

          }




      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/abandonedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
    })
   
});

//Assign Driver

userRequestRoute.post('/assignDriver', function (req, res) {
    console.log("post assignDriver");

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var pickUpId = req.body.pickUpId;

    var driverId = req.body.driverId;
    var areaCode = req.body.areaCode;

    if(areaCode==''){
      req.flash('error', 'Please Select Area Code');
     return res.redirect("/userRequest/assignDriver/"+pickUpId);

    }

    if(driverId==''){
      req.flash('error', 'Please Select Driver');
     return res.redirect("/userRequest/assignDriver/"+pickUpId);

    }

    Users.find({'where':{"id":driverId}})
        .then(function(driversData)
        { 


        Users.find({'where':{"id":driversData.partnerId}})
        .then(function(partnerData)
        { 

          var partnerId = driversData.partnerId;
          var vehicleNumber = driversData.vehicleNumber;
          var partnerEmail = partnerData.email;
          var partnerMobile = partnerData.mobile;
          var partnerName = partnerData.name;

        UserRequests.find({'where':{"id":pickUpId}})
            .then(function(pickUpData)
            { 
               var custMobile = pickUpData.mobile;
               var custEmail = pickUpData.email;
               var custName = pickUpData.name;
               var pickUpDateTime = pickUpData.dateTime;
               var chkPaymentType = pickUpData.paymentType;
               var custAddress = pickUpData.address;

               var paymentType = '';
               if(chkPaymentType){

                if(chkPaymentType=='cash'){
                  paymentType ='Collect Cash';

                }else if(chkPaymentType=='card'){
                  paymentType ='Online Payment';

                }else if(chkPaymentType=='bank'){
                  paymentType ='Bank Transfer';
                }
               }

               var checkDriverAlreadyAssigned = pickUpData.driverId;

                var driverName = driversData.name;
                var driverEmail = driversData.email;
                var driverMobile = driversData.mobile;
                var driverVehicleNumber = driversData.vehicleNumber;

            Users.find({'where':{"email":custEmail}})
              .then(function(usersData)
              {
                console.log(usersData);
                console.log("tttttt");
                var deviceToken ='';

                if(usersData){
                  deviceToken = usersData.deviceId;
                }

                //var smsContent="Dear "+custName+", A sewage truck has been deployed for your order. Driver Name : "+driverName+", Truck Number : "+driverVehicleNumber+" Kindly reach us for any questions.";

                if(checkDriverAlreadyAssigned){
                    var smsContent="Dear "+custName+", Your order has been reassigned. New details below: Driver's Name : "+driverName+". Truck Number : "+driverVehicleNumber+". We apologize for any inconvenience caused. Best regards,The PoopLogg Team.";
                }else{
                      var smsContent="Sewage truck assigned to your order.Driver's Name : "+driverName+". Truck Number : "+driverVehicleNumber+". For any enquiries kindly reach us on 0700 0700 211. Best regards,The PoopLogg Team";

                }

              if(deviceToken){
                var messageText = smsContent;
                var title = 'Driver Assigned';
                sendPushNotification(deviceToken,messageText,title);
              }

                
                    UserRequests.update({"driverId":driverId,"partnerId":partnerId,
                          "vehicleNumber":vehicleNumber,"status":"CO"},{'where':{'id':pickUpId}})
                        .then(function(data){

                      if(checkDriverAlreadyAssigned){
                        var smsContent="Dear "+custName+", Your order has been reassigned. New details below: Driver's Name : "+driverName+". Truck Number : "+driverVehicleNumber+". We apologize for any inconvenience caused. Best regards,The PoopLogg Team"; 
                        var mailContent="Dear "+custName+", Your order has been reassigned. New details below:<br></br> Driver's Name : "+driverName+". <br></br>Truck Number : "+driverVehicleNumber+". <br></br> We apologize for any inconvenience caused. <br></br>Best regards,<br></br>The PoopLogg Team.";

                      }else{
                        var mailContent="Sewage truck assigned to your order.Driver's Name : "+driverName+".<br></br> Truck Number : "+driverVehicleNumber+"<br></br> For any enquiries kindly reach us on 0700 0700 211.<br></br> Best regards,<br></br> The PoopLogg Team.";
                        var smsContent="Sewage truck assigned to your order.Driver's Name : "+driverName+". Truck Number : "+driverVehicleNumber+". For any enquiries kindly reach us on 0700 0700 211. Best regards,The PoopLogg Team";

                      }

                      var driverSmsContent="Dear "+driverName+", the job below has been assigned to you:.Customer Name : "+custName+". Customer Address : "+custAddress+". Pick Date/Time : "+pickUpDateTime+". Payment : "+paymentType+". Contact Number : 09072693955";
                      var driverMailContent="Dear "+driverName+", the job below has been assigned to you:.Customer Name : "+custName+". Customer Address : "+custAddress+". Pick Date/Time : "+pickUpDateTime+". Payment : "+paymentType+". Contact Number : 09072693955";


                      var partnerSmsContent="Dear "+partnerName+", Driver Name : "+driverName+" the job below has been assigned to you:.Customer Name : "+custName+". Customer Address : "+custAddress+". Pick Date/Time : "+pickUpDateTime+". Payment : "+paymentType+". Contact Number : 09072693955";
                      var partnerMailContent="Dear "+partnerName+", Driver Name : "+driverName+" the job below has been assigned to you:.Customer Name : "+custName+". Customer Address : "+custAddress+". Pick Date/Time : "+pickUpDateTime+". Payment : "+paymentType+". Contact Number : 09072693955";
                       //var smsContent="Dear "+custName+", A sewage truck has been deployed for your order. Driver Name : "+driverName+", Truck Number : "+driverVehicleNumber+" Kindly reach us for any questions.";

                      request({
                                  url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+smsContent+'.&flash=0&recipients='+custMobile+'',
                                  method: 'GET',
                              }, function(error, response, body){
                                  if(error) {
                                     req.flash('success', 'Driver Assigned Successfully.');
                                     res.redirect("/home");
                                     //res.redirect("/userRequest/confirmed");
                                  } else {

                                    request({
                                        url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+driverSmsContent+'.&flash=0&recipients='+driverMobile+'',
                                        method: 'GET',
                                    }, function(error, response, body){
                                        if(error) {
                                           req.flash('success', 'Driver Assigned Successfully.');
                                           res.redirect("/home");
                                           //res.redirect("/userRequest/confirmed");
                                        } else {


                                          request({
                                              url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+partnerSmsContent+'.&flash=0&recipients='+partnerMobile+'',
                                              method: 'GET',
                                          }, function(error, response, body){
                                              if(error) {
                                                 req.flash('success', 'Driver Assigned Successfully.');
                                                 res.redirect("/home");
                                                 //res.redirect("/userRequest/confirmed");
                                              } else {

                                            if(custEmail){
                                  
                                                var mailOptions = {
                                                          from: 'info@pooplogg.com',
                                                          to: custEmail,
                                                          subject: 'PoopLogg:Driver Assigned',
                                                          html: mailContent
                                                        };

                                                     var driverMailOptions = {
                                                          from: 'info@pooplogg.com',
                                                          to: driverEmail,
                                                          subject: 'PoopLogg:Driver Assigned',
                                                          html: driverMailContent
                                                        };

                                                    var partnerMailOptions = {
                                                          from: 'info@pooplogg.com',
                                                          to: partnerEmail,
                                                          subject: 'PoopLogg:Driver Assigned',
                                                          html: partnerMailContent
                                                        };

                                                transporter.sendMail(mailOptions, function(error, info){
                                                      if (error) {
                                                         req.flash('success', 'Driver Assigned Successfully.');
                                                         res.redirect("/home");
                                                        //res.redirect("/userRequest/confirmed"); 
                                                      } else {
                                                          //req.flash('success', 'Driver Assigned Successfully.');
                                                          //res.redirect("/home");


                                                          transporter.sendMail(driverMailOptions, function(error, info){
                                                            if (error) {
                                                               req.flash('success', 'Driver Assigned Successfully.');
                                                               res.redirect("/home");
                                                              //res.redirect("/userRequest/confirmed"); 
                                                            } else {
                                                                transporter.sendMail(partnerMailOptions, function(error, info){
                                                                  if (error) {
                                                                     req.flash('success', 'Driver Assigned Successfully.');
                                                                     res.redirect("/home");
                                                                    //res.redirect("/userRequest/confirmed"); 
                                                                  } else {
                                                                      req.flash('success', 'Driver Assigned Successfully.');
                                                                      res.redirect("/home");
                                                                      //res.redirect("/userRequest/confirmed");                    
                                                                    }
                                                                });
                                                                //res.redirect("/userRequest/confirmed");                    
                                                              }
                                                          });

                                                        }
                                                    });
                                              }else{
                                                  req.flash('success', 'Driver Assigned Successfully.');
                                                  res.redirect("/home");
                                              }//mail

                                              //changes multiple sms


                                        }// driver sms else
                                       })//driver sms

                                     }// driver sms else
                                    })//driver sms

                                  }//sms else
                                })//sms
                          //       
    ///
                                 
                            
                    })
                    .catch(function(err)
                    {
                        req.flash('error', err);
                        res.redirect("/userRequest/assignDriver/"+pickUpId);
                        //res.redirect("/userRequest/assignDriver/"+pickUpId+"/"+areaCode);
                    })



          })
          .catch(function(err)
          {

            console.log(1);

            console.log(err);
              req.flash('error', err);
              res.redirect("/userRequest/assignDriver/"+pickUpId);
              
          })
               
           
          })
          .catch(function(err)
          {

            console.log(2);

            console.log(err);
              req.flash('error', err);
              res.redirect("/userRequest/assignDriver/"+pickUpId);
              
          })

         })//partner data
          .catch(function(err)
          {

            console.log(2);

            console.log(err);
              req.flash('error', err);
              res.redirect("/userRequest/assignDriver/"+pickUpId);
              
          })

    })
    .catch(function(err)
    {

      console.log(3);

        console.log(err);
        req.flash('error', err);
        res.redirect("/userRequest/assignDriver/"+pickUpId);
        
    })

});



userRequestRoute.get('/confirmed', function (req, res) {
    console.log("userSuspendedRequest list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    UserRequests.findAll({'where':{"status":'CO'}})
      .then(function(result)
      {

        var totalLength = result.length;
        var i=1;
        if(totalLength){

            result.map(function(elm){

                Users.find({'where':{"id":elm.managerId}})
                  .then(function(users)
                  {
                    elm.takenByMgrName= users.name;

                     if(totalLength==i){

                      res.render('userRequest/managerConfirmedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

                     }
                     i++;


                   })
                  .catch(function(err)
                  {
                      req.flash('error',err);
                      res.render('userRequest/managerConfirmedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});
                  })

                })//map
          }else{
            res.render('userRequest/managerConfirmedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});

          }

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/managerConfirmedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
    })

    
    //res.render('userRequest/createUserRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
   
});



userRequestRoute.get('/rejectedRequest', function (req, res) {
    console.log("rejectedRequest list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    UserRequests.findAll({'where':{"status":'R'}})
      .then(function(result)
      {

        var totalLength = result.length;
        var i=1;
        if(totalLength){

            result.map(function(elm){

                Users.find({'where':{"id":elm.managerId}})
                  .then(function(users)
                  {
                    elm.takenByMgrName= users.name;

                     if(totalLength==i){

                        res.render('userRequest/rejectedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

                     }
                     i++;


                   })
                  .catch(function(err)
                  {
                      req.flash('error',err);
                      res.render('userRequest/rejectedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});
                  })

                })//map
          }else{
            res.render('userRequest/rejectedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

          }




      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/rejectedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
    })

    
    //res.render('userRequest/createUserRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
   
});






userRequestRoute.get('/userCompletedRequest', function (req, res) {
    console.log("userSuspendedRequest list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    UserRequests.findAll({'where':{"status":'COMPL'}})
      .then(function(result)
      {

         var totalLength= result.length;

         var i =1;
        if(totalLength){

            result.map(function(elm){

                Users.find({'where':{"id":elm.driverId}})
                  .then(function(users)
                  {

                    Users.find({'where':{"id":elm.managerId}})
                      .then(function(takenByMgr)
                      {
                      elm.driverName= users.name;
                      elm.takenByMgrName = takenByMgr.name;

                       if(totalLength==i){

                        res.render('userRequest/managerCompletedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

                       }
                       i++;

                    })
                    .catch(function(err)
                    {
                        var result=[];
                        req.flash('error',err);
                        res.render('userRequest/managerCompletedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
                    })


                   })
                  .catch(function(err)
                  {
                      var result=[];
                      req.flash('error',err);
                      res.render('userRequest/managerCompletedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
                  })

                })//map

          }else{
                res.render('userRequest/managerCompletedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});


          }

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/managerCompletedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
    })

    
   
});





// userRequestRoute.get('/paymentStatus', function (req, res) {
//     console.log("paymentStatus list");
//     ssn = req.session;
//     var accesstoken = ssn.accesstoken;
//     var username = ssn.username;
//     var userType = ssn.userType;
//     var userId = ssn.userId;
//     var ssnprofileImage = ssn.profileImage;
//     var menuArr = ssn.menuArr;

//     UserRequests.findAll()
//       .then(function(result)
//       {
//             res.render('userRequest/paymentStatus',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

//       })
//     .catch(function(err)
//     {
//         var result=[];
//         req.flash('error',err);
//         res.render('userRequest/paymentStatus',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
//     })

    
   
// });




userRequestRoute.get('/paymentStatus', function (req, res) {
    console.log("driver paymentStatus list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;


    //UserRequests.findAll({'where':{"status":'P'}})
    UserRequests.findAll()
      .then(function(result)
      {

        var totalLength = result.length;
        var i=1;
        if(totalLength){

            result.map(function(elm){

                Users.find({'where':{"id":elm.callCenterMgrId}})
                  .then(function(users)
                  {

                  Users.find({'where':{"id":elm.managerId}})
                  .then(function(takenByMgr)
                  {
                    elm.callCenterMgrName= users.name;

                    if(takenByMgr)
                        elm.takenByMgrName=takenByMgr.name;
                      else
                        elm.takenByMgrName='';

                     if(totalLength==i){

                        res.render('userRequest/paymentStatus',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

                     }
                     i++;

                  })
                  .catch(function(err)
                  {
                      var result=[];
                      req.flash('error',err);
                      res.render('userRequest/paymentStatus',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});
                  })


                   })
                  .catch(function(err)
                  {
                      var result=[];
                      req.flash('error',err);
                      res.render('userRequest/paymentStatus',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});
                  })

                })//map
          }else{
                      res.render('userRequest/paymentStatus',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});

          }

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/paymentStatus',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
    })

    
    //res.render('userRequest/createUserRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
   
});





userRequestRoute.get('/userExecuteRequest', function (req, res) {
  


    console.log("userExecuteRequest list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    UserRequests.findAll({'where':{"status":'COMPL'}})
      .then(function(result)
      {

         var totalLength= result.length;

         var i =1;
        if(totalLength){

            result.map(function(elm){

                Users.find({'where':{"id":elm.driverId}})
                  .then(function(users)
                  {

                    Users.find({'where':{"id":elm.managerId}})
                      .then(function(takenByMgr)
                      {
                      elm.driverName= users.name;
                      elm.takenByMgrName = takenByMgr.name;

                       if(totalLength==i){

                        res.render('userRequest/userExecuteRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

                       }
                       i++;

                    })
                    .catch(function(err)
                    {
                        var result=[];
                        req.flash('error',err);
                        res.render('userRequest/userExecuteRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
                    })


                   })
                  .catch(function(err)
                  {
                      var result=[];
                      req.flash('error',err);
                      res.render('userRequest/userExecuteRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
                  })

                })//map

          }else{
                res.render('userRequest/userExecuteRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});


          }

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/userExecuteRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr});
    })
   
});




userRequestRoute.get('/execute/:pickUpId',function(req,res){

        ssn = req.session;
        var accesstoken = ssn.accesstoken;
        var username = ssn.username;
        var userType = ssn.userType;
        var userId = ssn.userId;
        var ssnprofileImage = ssn.profileImage;
        var menuArr = ssn.menuArr;

        var pickUpId = req.params.pickUpId;
        var status = 'EX';

        var paymentStatus ='Paid';

        UserRequests.find({'where':{'id':pickUpId}})
          .then(function(customerData)
          { 

          UserRequests.update({'status':status,'paymentStatus':paymentStatus},{'where':{'id':pickUpId}})
            .then(function(data){
                 
        var custName = customerData.name;

        var custMobile = customerData.mobile;
        var custEmail = customerData.email;

        //var mailContent="Hi "+custName+", Your pickup request successfully executed.Thank you.";
        //var smsContent="Dear "+custName+", Your order has been successfully executed.Thank you for choosing PoopLogg.";

        var mailContent="Dear "+custName+", Your order has been successfully executed.<br></br>Thank you for choosing PoopLogg.<br></br>Best regards,<br></br>The PoopLogg Team";
        var smsContent="Dear "+custName+", Your order has been successfully executed.Thank you for choosing PoopLogg.Best regards,The PoopLogg Team";

            request({
                        url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLogg&messagetext='+smsContent+'.&flash=0&recipients='+custMobile+'',
                        method: 'GET',
                    }, function(error, response, body){
                        if(error) {
                           req.flash('success', 'Customer Request Executed Successfully.');
                           res.redirect('/userRequest/userExecutedRequest');
                        } else {

                          if(custEmail){
                
                              var mailOptions = {
                                        from: 'info@pooplogg.com',
                                        to: custEmail,
                                        subject: 'PoopLogg:PickUp Executed',
                                        html: mailContent
                                      };

                              transporter.sendMail(mailOptions, function(error, info){
                                    if (error) {
                                       req.flash('success', 'Customer Request Executed Successfully.');
                                        res.redirect('/userRequest/userExecutedRequest');
                                    } else {
                                        req.flash('success', 'Customer Request Executed Successfully.');
                                        res.redirect('/userRequest/userExecutedRequest');                      }
                                  });
                            }else{
                                req.flash('success', 'Customer Request Executed Successfully.');
                                res.redirect('/userRequest/userExecutedRequest');

                            }

                        }//sms
                      })//sms


              })
              .catch(function(err)
              {
                  req.flash('error', 'Customer Request Could Not Be Executed.');
                  res.redirect('/userRequest/userExecuteRequest');
              });

            })
            .catch(function(err)
            {
                req.flash('error', 'Customer Request Could Not Be Executed.');
                res.redirect('/userRequest/userExecuteRequest');
            });
        
});




userRequestRoute.get('/userExecutedRequest', function (req, res) {
  


    console.log("userExecutedRequest list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    UserRequests.findAll({'where':{"status":'EX'}})
      .then(function(result)
      {

         var totalLength= result.length;

         var i =1;
        if(totalLength){

            result.map(function(elm){

                Users.find({'where':{"id":elm.driverId}})
                  .then(function(users)
                  {

                    Users.find({'where':{"id":elm.managerId}})
                      .then(function(takenByMgr)
                      {
                        elm.driverName= '';
                        if(users){
                          elm.driverName= users.name;
                        }
                      
                      elm.takenByMgrName = takenByMgr.name;

                       if(totalLength==i){

                        res.render('userRequest/userExecutedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result,menuArr:menuArr});

                       }
                       i++;

                    })
                    .catch(function(err)
                    {
                        req.flash('error',err);
                        res.render('userRequest/userExecutedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,result:[]});
                    })


                   })
                  .catch(function(err)
                  {
                      req.flash('error',err);
                      res.render('userRequest/userExecutedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,result:[]});
                  })

                })//map

          }else{
                res.render('userRequest/userExecutedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:[],menuArr:menuArr});


          }

      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('userRequest/userExecutedRequest',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,result:[]});
    })
   
});




userRequestRoute.get('/update/:pickUpId',function(req,res){
  console.log("update................");

      ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

  var pickUpId = req.params.pickUpId;

 
Areas.findAll({'where':{"status":1}})
        .then(function(areaData)
        {
          UserRequests.find({'where':{"id":pickUpId}})
              .then(function(result)
              {
                var selectedAreaCode = result.areaCode;
                res.render('userRequest/updateUserRequest',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,pickUpId:pickUpId,result:result,selectedAreaCode:selectedAreaCode,areaData:areaData});
              })
            .catch(function(err)
            {
                var result=[];
                req.flash('error',err);
                res.render('userRequest/updateUserRequest',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,pickUpId:pickUpId,result:result,selectedAreaCode:'',areaData:areaData});

            });
        })
        .catch(function(err)
        {
            var result=[];
            req.flash('error',err);
            res.render('userRequest/updateUserRequest',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,pickUpId:pickUpId,result:result,selectedAreaCode:'',areaData:areaData});

        });
        
});




userRequestRoute.post('/update',function(req,res){
  console.log("update................");

      ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

        var dateTime = req.body.dateVal+' '+req.body.timeVal;


  
  var pickUpId = req.body.pickUpId;

var params = {

    name:req.body.name,
    email:req.body.email,
    mobile:req.body.mobile,
    numberOfTrips:req.body.numberOfTrips,
    areaCode:req.body.areaCode,
    address:req.body.address,
    dateTime:dateTime

  };
 

    UserRequests.update(params,{'where':{'id':pickUpId}})
      .then(function(data){
          req.flash('success', 'User Request Updated Successfully.');
          res.redirect('/home');
        })
      .catch(function(err)
      {
          req.flash('error', 'User Request Could Not Be Update.');
          res.redirect('/home');

      });
        
});


userRequestRoute.get('/getDriverData/:areaCode', function (req, res) {

    console.log("driverData");
    var areaCode = req.params.areaCode;

    console.log(areaCode);

    //Users.findAll({'where':{"userType":7,"areaCode":areaCode,status:'Y'}})//vehicleNumber
    Users.findAll({'where':{"userType":7,"areaCode":areaCode,status:'Y',"vehicleNumber":{$ne:null} }})//vehicleNumber
        .then(function(driversData)
        { 
            res.send(driversData);
        })
        .catch(function(err)
        {
            var driversData=[];
            res.send(driversData);
        })

});


function sendPushNotification(deviceToken,message,title){

    var message = {
      to: deviceToken, // required fill with device token or topics
      collapse_key: 'your_collapse_key', 
      data: {
          your_custom_data_key: 'your_custom_data_value'
      },
      notification: {
          title: title,
          body: message

      }
  };

  //callback style
  fcm.send(message, function(err, response){
      if (err) {

        console.log(err);
          console.log("Something has gone wrong!");
      } else {
          console.log("Successfully sent with response: ", response);
      }
  });
}



module.exports = userRequestRoute;
