var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
var fs = require("fs");
var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var sendmail = require('sendmail')();
var nodemailer = require('nodemailer');
var FCM = require('fcm-push');
var bulk = require('node-bulk-sms');
var request = require('request');

var paymentRoute = require('express').Router();

//var Users = require('./../models').Users;
var Payments = require('./../models').Payments;
var Users = require('./../models').Users;
var UserRequests = require('./../models').UserRequests;

//var UpdateDriverRequests = require('./../models').UpdateDriverRequests;

var transporter = nodemailer.createTransport({
        host: 'server207.web-hosting.com',
        port: 465,
        secure: true,
        auth: {
            user: 'info@pooplogg.com',
            pass: '#pooplogg2020'
        }
      });
var serverKey = 'AIzaSyAcdysa4IDh_Mi_JaZUJA9Qxx-UnUFa2V0';
var fcm = new FCM(serverKey);

paymentRoute.get('/:pickUpId/:amount', function (req, res) {

  console.log("Payment route");
  ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var pickUpId = req.params.pickUpId;
    var amount = req.params.amount;
    return res.render('payment/payment',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,pickUpId:pickUpId,amount:amount});
});

paymentRoute.get('/:email', function (req, res) {

  console.log("Payment route");
  ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var email = req.params.email;

    return res.render('payment/payment',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,email:email});
});

paymentRoute.post('/successTransaction', function (req, res) {

  console.log("Payment tests");
  var today = new Date();

  console.log(req.body);
  var status = req.body.status;
  var pickUpId = req.body.pickUpId;

  console.log("Payment tests");

  UserRequests.find({'where':{"id":pickUpId}})
    .then(function(userData)
    {       
        if(userData){

          var email = userData.email;

        Users.find({'where':{"email":email}})
        .then(function(payuserData)
        {
          console.log("succcccccc");
          console.log(status);
          var params={
            pickUpId:pickUpId,
            email:userData.email,
            VendorReferenceNumber:req.body.VendorReferenceNumber,
            amount:req.body.Amount,
            PaymentPlatform:req.body.PaymentPlatform,
            paymentDate:today,
            status:status,
          };
          var amount = req.body.Amount;
          var email = userData.email;
          Payments.create(params)
            .then(function(result){
              if(status=='Transaction Successful'){
                var paymentStatus = 'success';
                var subject = 'Successful payment confirmation';
                var mailContent='Hello, '+userData.name+' <br></br>This is to inform you that your payment has been received successfully.';
                var message = 'Payment successfully';
                var smsContent='Hello, '+userData.name+', This is to inform you that your payment has been received successfully.';

              }else{
                var paymentStatus = 'failed';
                var subject = 'Payment failed';
                var mailContent='Hello, '+userData.name+' <br></br>This is to inform you that your payment has been failed.';
                var smsContent='Hello, '+userData.name+', This is to inform you that your payment has been failed.';

                var message = 'Payment failed';
              }
              UserRequests.update({'paymentStatus':paymentStatus,'amount':amount},{'where':{'id':pickUpId}})
                .then(function(data){


              var deviceToken ='';
              if(payuserData)
                deviceToken = payuserData.deviceId;

                  //var deviceToken = payuserData.deviceId;
                  var custMobile = userData.mobile;

                console.log(deviceToken);

                if(deviceToken){
                  var messageText = smsContent;
                  var title = 'PoopLogg '+message;
                  sendPushNotification(deviceToken,messageText,title);
                }


                 request({
                      url: 'http://api.ebulksms.com:8080/sendsms?username=J.falore@pooplogg.com&apikey=4d017f3072bc746b466a1740c205d9ae4aeba695&sender=PoopLog&messagetext='+smsContent+'.&flash=0&recipients='+custMobile+'',
                      method: 'GET',
                  }, function(error, response, body){
                      if(error) {
                         res.send(message);
                              //res.redirect('/userRequest/userRequestPendingList');
                      } else {

                        if(email){

                          var mailOptions = {
                                        from: 'info@pooplogg.com',
                                        to: email,
                                        subject: subject,
                                        html: mailContent
                                      };

                          transporter.sendMail(mailOptions, function(error, info){
                                if (error) {
                                      res.send(message);
                                } else {
                                  res.send(message);
                                }
                              });

                        }else{
                          res.send(message);
                        }
                      }//sms
                    })
               
              })
            .catch(function(err)
            {
                console.log(err);
                res.send("Payment failed..");
            })
          })
          .catch(function(err)
          {
              console.log(err);
              res.send("Payment failed..");
          })

          })
          .catch(function(err)
          {
              console.log(err);
              res.send("Payment failed..");
          })
        }else{
            res.send("User not found..");
        }

      })
      .catch(function(err)
      {
          console.log(err);
          res.send("Payment failed..");
      })
  
});


paymentRoute.post('/successTransactionOld', function (req, res) {

  console.log("Payment tests");
  var today = new Date();

  console.log(req.body);
  var status = req.body.status;
  var email = req.body.email;

  console.log("Payment tests");

  Users.findAll({'where':{"email":email}})
    .then(function(userData)
    {       
        if(userData.length){
          var params={
            email:email,
            VendorReferenceNumber:req.body.VendorReferenceNumber,
            amount:req.body.Amount,
            PaymentPlatform:req.body.PaymentPlatform,
            paymentDate:today,
            status:status,
          };
          var amount = req.body.Amount;
          Payments.create(params)
            .then(function(result){
              if(status=='Transaction Successful'){
                var paymentStatus = 'success';
                var subject = 'Successful payment confirmation';
                var mailContent='Hello, '+userData[0].name+' <br></br>This is to inform you that your payment has been received successfully.';
                var message = 'Payment successfully';
              }else{
                var paymentStatus = 'failed';
                var subject = 'Payment failed';
                var mailContent='Hello, '+userData[0].name+' <br></br>This is to inform you that your payment has been failed.';
                var message = 'Payment failed';
              }
              Users.update({'paymentStatus':paymentStatus,'amount':amount},{'where':{'email':email}})
                .then(function(data){




                  var mailOptions = {
                                  from: 'info@pooplogg.com',
                                  to: email,
                                  subject: subject,
                                  html: mailContent
                                };

                    transporter.sendMail(mailOptions, function(error, info){
                          if (error) {
                                res.send(message);
                          } else {
                            res.send(message);
                          }
                        });
                
                // sendmail({
                //             from: 'patil.dilipmg@gmail.com',
                //             to: email,
                //             subject: subject,
                //             html: mailContent,
                //           }, function(err, reply) {
                //             if(err){
                //             res.send(message);
                //             }
                //             res.send(message);
                //         });
              })
            .catch(function(err)
            {
                console.log(err);
                res.send("Payment failed..");
            })
          })
          .catch(function(err)
          {
              console.log(err);
              res.send("Payment failed..");
          })
        }else{
            res.send("User not found..");
        }

      })
      .catch(function(err)
      {
          console.log(err);
          res.send("Payment failed..");
      })
  
});



function sendPushNotification(deviceToken,message,title){

    var message = {
      to: deviceToken, // required fill with device token or topics
      collapse_key: 'your_collapse_key', 
      data: {
          your_custom_data_key: 'your_custom_data_value'
      },
      notification: {
          title: title,
          body: message

      }
  };

  //callback style
  fcm.send(message, function(err, response){
      if (err) {

        console.log(err);
          console.log("Something has gone wrong!");
      } else {
          console.log("Successfully sent with response: ", response);
      }
  });
}


module.exports = paymentRoute;
