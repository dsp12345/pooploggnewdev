var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
var signUpRoute = require('express').Router();

var Users = require('../models').Users;
console.log("databaseee");
console.log(config.dbname);

signUpRoute.get('/', function (req, res) {
    //req.flash('error','Username allready exist.');
    res.render('register/register');

});


signUpRoute.post('/', function (req, res) {

    var today = new Date();
    var username = req.body.username;
    var profileImage = 'default';

    var  params = {
                "username":req.body.username,
                "email":req.body.email,
                "name":req.body.name,
                "mobile":req.body.mobile,
                "password":req.body.password,
                "userType":req.body.userType,
                "profileImage":profileImage,
                "status":'Y',
                "createdAt":today,
                "updatedAt":today
            };

    Users.findAll({'where':{"username":username}})
    .then(function(result)
    {       
        if(result.length){
            req.flash('error','Username allready exist.');
            return res.render('register/register');
        }else{
             Users.create(params)
            .then(function(result){
                req.flash('success', 'User registerd Successfully.');
                return res.render('register/register');
            })
            .catch(function(err)
            {
                req.flash('error', 'User Could Not Be Added.');
                return res.render('register/register');
            })
        }
    })
    .catch(function(err)
    {
        console.log(err);
      return res.status(401).send({status:'error','data':{'message':'User Could Not Be Added'}});
    })

});



//mobile api 

// signUpRoute.post('/', function (req, res) {

//     var today = new Date();
//     var username = req.body.username;

//     console.log("username");
//      console.log(username);

//     var  params = {
//                 "username":req.body.username,
//                 "email":req.body.email,
//                 "name":req.body.name,
//                 "mobile":req.body.mobile,
//                 "password":req.body.password,
//                 "userType":req.body.userType,
//                 "createdAt":today,
//                 "updatedAt":today
//             };

//     Users.findAll({'where':{"username":username}})
//     .then(function(result)
//     {       
//         if(result.length){
//             return res.status(401).send({status:'error','data':{'message':'Username allready exist.'}});
//         }else{
//              Users.create(params)
//             .then(function(result){
//                  return res.status(200).send({status:'success','data':result,'message':'User Created Successfully'});
//             })
//             .catch(function(err)
//             {
//               return res.status(401).send({status:'error','data':{'message':'User Could Not Be Added'}});
//             })
//         }
//     })
//     .catch(function(err)
//     {
//         console.log(err);
//       return res.status(401).send({status:'error','data':{'message':'User Could Not Be Added'}});
//     })

// });


signUpRoute.post('/api', function (req, res) {

    console.log("appp");
});

module.exports = signUpRoute;