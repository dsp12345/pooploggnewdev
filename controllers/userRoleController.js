var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var arraySort = require('array-sort');


var userRoleRoute = require('express').Router();

var Users = require('./../models').Users;
var UserRoles = require('./../models').UserRoles;
var MenuSubMenus = require('./../models').MenuSubMenus;
var MenuAccess = require('./../models').MenuAccess;

userRoleRoute.get('/', function (req, res) {
    //console.log(req.userId);

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var menuArr = ssn.menuArr;

    UserRoles.findAll()
    .then(function(userRolesData)
    { 
        userRolesData.reverse();

        res.render('user/userRole',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:userRolesData,menuArr:menuArr,userRole:'',userRoleId:''});
    })
    .catch(function(err)
    {
        var userRolesData=[];
        req.flash('error', err);
        return res.render('user/userRole',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:userRolesData,menuArr:menuArr,userRole:'',userRoleId:''});
    })
});




userRoleRoute.post('/', function (req, res) {
    console.log("userRole post");


    console.log(req.body);

    var userRole = req.body.userRole;

    var params = {
                    userRole:req.body.userRole,
                    status:1
    };


    UserRoles.findAll({'where':{"userRole":userRole}})
    .then(function(userRolesData)
    { 

        console.log(userRolesData);

        if(userRolesData.length){

            req.flash('error', 'User Role Allready Exist.');
            res.redirect('/userRole');

        }else{


            UserRoles.create(params)
            .then(function(result){
                req.flash('success', 'User Role Added Successfully.');
                res.redirect('/userRole');
                //return res.render('user/userRole',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
            })
            .catch(function(err)
            {
                console.log(err);
                req.flash('error', 'User Role Could Not Be Added.');
                res.redirect('/userRole');
                //return res.render('user/userRole',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
            })

        }
    })
    .catch(function(err)
    {
        console.log(err);
        req.flash('error', 'User Role Could Not Be Added.');
        res.redirect('/userRole');
        //return res.render('user/userRole',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })
});






userRoleRoute.get('/menuAccess', function (req, res) {
    //console.log(req.userId);

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    //UserRoles.findAll()
    UserRoles.findAll({'where':{id:{$notIn:[1]},status:1}})
    .then(function(userRolesData)
    { 
        var i=1;
        var menuSubMenusDataArr = [];
        MenuSubMenus.findAll()
        .then(function(menuSubMenusData)
        { 
            var menuSubMenusDataLength = menuSubMenusData.length;

            if(menuSubMenusDataLength){
                menuSubMenusData.map(function(elm){

                    menuSubMenusDataArr.push({'subMenuId':elm.subMenuId,'subMenuName':elm.subMenuName,
                                                    'checkSelected':0});
                    if(menuSubMenusDataLength==i){

                        res.render('user/menuAccess',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:userRolesData,menuSubMenusData:menuSubMenusDataArr,seletedUserRoleId:'',menuArr:menuArr});

                    }
                    i++;

                })
            }else{
                res.render('user/menuAccess',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:userRolesData,menuSubMenusData:[],seletedUserRoleId:'',menuArr:menuArr});
            }
            

        })
        .catch(function(err)
        {
            var userRolesData=[];
            var menuSubMenusData = [];
            req.flash('error', err);
            return res.render('user/menuAccess',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:userRolesData,menuSubMenusData:menuSubMenusData,seletedUserRoleId:'',menuArr:menuArr});
        })
    })
    .catch(function(err)
    {
        var userRolesData=[];
        var menuSubMenusData = [];
        req.flash('error', err);
        return res.render('user/menuAccess',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:userRolesData,menuSubMenusData:menuSubMenusData,seletedUserRoleId:'',menuArr:menuArr});
    })
});


userRoleRoute.post('/menuAccess', function (req, res) {


    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var userRole = req.body.userRole;
    var subMenusArr = req.body.subMenus;

    var menuAccessArr = [];
    if(subMenusArr.constructor === Array){
        subMenusArr.map(function(elm){
            menuAccessArr.push({userRoleId:userRole,subMenuId:elm});

        })
    }else{
        menuAccessArr.push({userRoleId:userRole,subMenuId:subMenusArr});
    }


        MenuAccess.destroy({'where':{"userRoleId":userRole}})
        .then(function(userRolesData)
        { 

            MenuAccess.bulkCreate(menuAccessArr)
            .then(function(result){
                req.flash('success', 'User Role Menu Access Assign Successfully.');
                res.redirect('/userRole/menuAccess');
                //return res.render('user/userRole',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
            })
            .catch(function(err)
            {
                console.log(err);
                req.flash('error', 'User Role Menu Access Assign Failed.');
                res.redirect('/userRole/menuAccess');
            })

        })
        .catch(function(err)
        {
            console.log(err);
            req.flash('error', 'User Role Menu Access Assign Failed.');
            res.redirect('/userRole/menuAccess');
        })
});



userRoleRoute.get('/menuAccess/:userRoleId', function (req, res) {
    //console.log(req.userId);

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    var userRoleId = req.params.userRoleId;



    console.log(userRoleId);

    UserRoles.findAll()
    .then(function(userRolesData)
    { 
        var menuSubMenusDataArr =[];
        var i=1;
        MenuSubMenus.findAll()
        .then(function(menuSubMenusData)
        { 

            var menuSubMenusDataLength = menuSubMenusData.length;

            if(menuSubMenusDataLength){

                menuSubMenusData.map(function(elm){

                    MenuAccess.find({'where':{"userRoleId":userRoleId,'subMenuId':elm.subMenuId}})
                        .then(function(menuAccessData)
                        { 

                            var checkSelected= 0;
                            if(menuAccessData){
                               checkSelected= 1;
                            }

                            menuSubMenusDataArr.push({'subMenuId':elm.subMenuId,'subMenuName':elm.subMenuName,
                                                'checkSelected':checkSelected});
                            if(menuSubMenusDataLength==i)
                            {
                                res.render('user/menuAccess',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:userRolesData,menuSubMenusData:menuSubMenusDataArr,seletedUserRoleId:userRoleId,menuArr:menuArr});
                            }
                            i++;

                        })
                        .catch(function(err)
                        {
                            //var userRolesData=[];
                            var menuSubMenusData = [];
                            req.flash('error', err);
                            return res.render('user/menuAccess',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:[],menuSubMenusData:menuSubMenusData,seletedUserRoleId:'',menuArr:menuArr});
                        })  

                })
            }else{

                 //var userRolesData=[];
                            var menuSubMenusData = [];
                            req.flash('error', err);
                            return res.render('user/menuAccess',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:[],menuSubMenusData:menuSubMenusData,seletedUserRoleId:'',menuArr:menuArr});

            }
            

        })
        .catch(function(err)
        {
            //var userRolesData=[];
            var menuSubMenusData = [];
            req.flash('error', err);
            return res.render('user/menuAccess',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:[],menuSubMenusData:menuSubMenusData,seletedUserRoleId:'',menuArr:menuArr});
        })
    })
    .catch(function(err)
    {
        //var userRolesData=[];
        var menuSubMenusData = [];
        req.flash('error', err);
        return res.render('user/menuAccess',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:[],menuSubMenusData:menuSubMenusData,seletedUserRoleId:'',menuArr:menuArr});
    })
});



userRoleRoute.get('/update/:userRoleId',function(req,res){
  console.log("update................");

      ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

  var userRoleId = req.params.userRoleId;

 

          UserRoles.find({'where':{"id":userRoleId}})
              .then(function(result)
              {
                var userRole = result.userRole;
                res.render('user/updateUserRole',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:[],menuArr:menuArr,userRole:userRole,userRoleId:userRoleId});
              })
            .catch(function(err)
            {
                var result=[];
                req.flash('error',err);
                res.render('user/updateUserRole',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,userRolesData:[],menuArr:menuArr,userRole:'',userRoleId:userRoleId});

            });
        
});



userRoleRoute.post('/update',function(req,res){
  console.log("update................");

      ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

  var userRoleId = req.body.userRoleId;
  var userRole = req.body.userRole;

 

          UserRoles.update({'userRole':userRole},{'where':{'id':userRoleId}})
            .then(function(data){
                req.flash('success', 'User Role Updated Successfully.');
                res.redirect('/userRole');
              })
            .catch(function(err)
            {
                req.flash('error', 'User Role Could Not Be Update.');
                res.redirect('/userRole');

            });
        
});



userRoleRoute.get('/action/:userRoleId/:status',function(req,res){
  console.log("update................");

      ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

  var userRoleId = req.params.userRoleId;
  var status = req.params.status;

 

          UserRoles.update({'status':status},{'where':{'id':userRoleId}})
            .then(function(data){
                
                if(status==1){
                    req.flash('success', 'User Role Resume Successfully.');
                }else{
                    req.flash('error', 'User Role Suspended Successfully.');
                }
                
                res.redirect('/userRole');
              })
            .catch(function(err)
            {
                req.flash('error', 'User Role Could Not Be Update.');
                res.redirect('/userRole');

            });
        
});



userRoleRoute.get('/delete/:userRoleId',function(req,res){
  console.log("update................");

      ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

  var userRoleId = req.params.userRoleId;

  Users.find({'where':{"userType":userRoleId,"status":"Y"}})
              .then(function(Users)
              {

                if(Users){

                    req.flash('error', 'This User Role is Assign to Multiple Users. You Cannot Delete This User Until you have removed all users and accouts linked to this user.');
                    res.redirect('/userRole');

                }else{

                  UserRoles.destroy({'where':{"id":userRoleId}})
                      .then(function(result)
                      {
                        req.flash('success', 'User Role Deleted Successfully.');
                        res.redirect('/userRole');
                      })
                    .catch(function(err)
                    {
                        var result=[];
                        req.flash('error',err);
                        //req.flash('error', 'User Role Could Not Be Update.');
                        res.redirect('/userRole');
                    });
                }

    })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.redirect('/userRole');
    });
        
});


module.exports = userRoleRoute;
