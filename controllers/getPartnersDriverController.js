var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');


var getDriverRoute = require('express').Router();

var Users = require('./../models').Users;
var Vehicles = require('./../models').Vehicles;
var DriverLocations = require('./../models').DriverLocations;
var CustomerPickUpRequests = require('./../models').CustomerPickUpRequests;

getDriverRoute.get('/:partnerId', function (req, res) {
  var partnerId = req.params.partnerId;
  var dataArr = [];

  Users.findAll({'where':{"partnerId":partnerId},attributes: ['name', 'email','mobile']
})
    .then(function(resultDriverIds)
    {            
        return res.status(200).send({status:'success','data':{'message':'success.',"data":resultDriverIds}});                
          
  })
  .catch(function(err)
  {

    console.log("error");
    console.log(err);
    return res.status(401).send({status:'error','data':{'message':'Driver location data could not be fetched.',"data":[]}});

  })
});


module.exports = getDriverRoute;
