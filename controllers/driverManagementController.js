var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var nodemailer = require('nodemailer');

var driverManageMentRoute = require('express').Router();

var Users = require('./../models').Users;
var UserRequests = require('./../models').UserRequests;
var Areas = require('./../models').Areas;


driverManageMentRoute.get('/:driverId/:status',function(req,res){
  console.log("update................");

        var driverId = req.params.driverId;
        var status = req.params.status;

          Users.update({'onDuty':status},{'where':{'id':driverId}})
            .then(function(data){
                
                if(status==1){
                    return res.status(200).send({status:'success','data':{'message':'Your duty started.',"data":[]}});
                }else{
                    return res.status(200).send({status:'success','data':{'message':'Your duty ended.',"data":[]}});
                }
                
              })
            .catch(function(err)
            {
                return res.status(400).send({status:'error','data':{'message':'Duty failed.',"data":[]}});

            });
        
});





module.exports = driverManageMentRoute;
