var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');


var multer = require('multer');

var upload = multer({ dest: '/tmp' })

var spotersRoute = require('express').Router();

var Users = require('./../models').Users;
console.log("databaseee");
console.log(config.dbname);

spotersRoute.get('/', function (req, res) {
    console.log("spoterssssss");

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;


    Users.findAll({'where':{"userType":4,addedBy:userId,spoterAssign:0}})
        .then(function(result)
        {  
            res.render('spoter/add_spoters',{username:username,userType:userType,userId:userId,result:result,ssnprofileImage:ssnprofileImage});

        })
        .catch(function(err)
        {
            var result=[];
            console.log(err);
            res.render('spoter/add_spoters',{username:username,userType:userType,userId:userId,result:result,ssnprofileImage:ssnprofileImage});
        })

});

spotersRoute.post('/',upload.single('profileImage'), function (req, res) {
    console.log("post driver");
    var today = new Date();
    var email = req.body.email;
    var password = "test123";
    var profileImage =req.file.filename;
    var addUserType = 5;
    
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var  params = {
                "username":req.body.email,
                "email":req.body.email,
                "name":req.body.name,
                "mobile":req.body.mobile,
                "password":password,
                "userType":addUserType,
                "profileImage":profileImage,
                "licenceNumber":'',
                "vehicleMakeModel":req.body.vehicleMakeModel,
                "vehicleColour":'',
                "vehicleLicenseNumber":'',
                "addedBy":userId,
                "createdAt":today,
                "updatedAt":today
            };

    if(req.file){

    var targetPath = path.resolve('./public/user_images/'+req.file.filename);
        fs.rename(req.file.path,targetPath, function(err) {
          if (err) {
            req.flash('error',err);
            return res.render('spoter/add_spoters',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
          } else {
                Users.findAll({'where':{"username":email}})
                .then(function(result)
                {       
                    if(result.length){
                        req.flash('error','Username allready exist.');
                        res.redirect('/spoters');
                    }else{
                         Users.create(params)
                        .then(function(result){


                            //update spoterAssign

                                Users.update({'spoterAssign':1},{'where':{'userType':4,'vehicleMakeModel':req.body.vehicleMakeModel}})
                                .then(function(data){
                                req.flash('success', 'Spoter added Successfully.');
                                res.redirect('/spoters');

                                })
                            .catch(function(err)
                            {
                                req.flash('error', 'Spoter Could Not Be Added.');
                                res.redirect('/spoters');
                            })
                        })
                        .catch(function(err)
                        {
                            req.flash('error', 'Spoter Could Not Be Added.');
                            res.redirect('/spoters');
                        })
                    }
                })
                .catch(function(err)
                {
                 console.log(err);
                    req.flash('error', 'Spoter Could Not Be Added.');
                    res.redirect('/spoters');
                })

          }
        
        });
    }else{//profile empty
            Users.findAll({'where':{"username":email}})
                .then(function(result)
                {       
                    if(result.length){
                        req.flash('error','Username allready exist.');
                        res.redirect('/spoters');
                        //return res.render('spoter/add_spoters');
                    }else{
                         Users.create(params)
                        .then(function(result){
                            //update spoterAssign

                                Users.update({'spoterAssign':1},{'where':{'userType':4,'vehicleMakeModel':req.body.vehicleMakeModel}})
                                .then(function(data){
                                req.flash('success', 'Spoter added Successfully.');
                                res.redirect('/spoters');

                                })
                                .catch(function(err)
                                {
                                    req.flash('error', 'Spoter Could Not Be Added.');
                                    res.redirect('/spoters');
                                })
                            //return res.render('spoter/add_spoters');
                        })
                        .catch(function(err)
                        {
                            req.flash('error', 'Spoter Could Not Be Added.');
                            res.redirect('/spoters');
                            //return res.render('spoter/add_spoters');
                        })
                    }
                })
                .catch(function(err)
                {
                 console.log(err);
                 req.flash('error', err);
                 res.redirect('/spoters');
                  //return res.status(401).send({status:'error','data':{'message':'Driver Could Not Be Added'}});
                })
    }
});



spotersRoute.get('/:providerId', function (req, res) {

     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage =ssn.profileImage;


    console.log("sPOTER list");
    console.log(req.params.providerId);
    var providerId = req.params.providerId;

    Users.findAll({'where':{"addedBy":providerId,userType:5}})
      .then(function(result)
      {
        console.log(result);
        res.render('spoter/list_spoters',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('spoter/list_spoters',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })

});

module.exports = spotersRoute;
