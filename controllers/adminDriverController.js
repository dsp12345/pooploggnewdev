var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var nodemailer = require('nodemailer');

var admindriverRoute = require('express').Router();

var Users = require('./../models').Users;
var Vehicles = require('./../models').Vehicles;
var UpdateDriverRequests = require('./../models').UpdateDriverRequests;


var transporter = nodemailer.createTransport({
        host: 'server207.web-hosting.com',
        port: 465,
        secure: true,
        auth: {
            user: 'info@pooplogg.com',
            pass: '#pooplogg2020'
        }
      });

admindriverRoute.get('/approvedDrivers', function (req, res) {
    console.log("driver approvedDrivers list");
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var driverType = 4;

    Users.findAll({'where':{"userType":driverType,"status":'Y'}})
    .then(function(result)
    {

        //console.log(result);
        res.render('driver/admin_approved_driver',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});
    })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('driver/admin_approved_driver',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});
    })
});

admindriverRoute.get('/requested', function (req, res) {
    console.log("driver requested list");

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var driverType = 4;

    Users.findAll({'where':{"userType":driverType,"status":''}})
    .then(function(result)
    {

        //console.log(result);
        res.render('driver/admin_requested_driver',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});
    })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('driver/admin_requested_driver',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});
    })
});



admindriverRoute.get('/driverRequest/:id/:status', function (req, res) {

    var id = req.params.id;
    var status = req.params.status;


    Users.find({'where':{"id":id}})
    .then(function(usersData)
    { 

      var driverEmail = usersData.email;

        Users.update({'status':status},{'where':{'id':id}})
            .then(function(data){
                if(status=='A'){
                  var mailContent ='Hi, Your sign up request approved successfully';



                   var mailOptions = {
                                  from: 'info@pooplogg.com',
                                  to: driverEmail,
                                  subject: 'PoopLogg:Your sign up request approved successfully.',
                                  html: mailContent
                                };

                    transporter.sendMail(mailOptions, function(error, info){
                          if (error) {
                                req.flash('error', 'Request Could Not Be Approved.');
                              res.redirect('/adminDriver/requested');
                          } else {
                            req.flash('success', 'Driver Approved Successfully.');
                            res.redirect('/adminDriver/requested');
                          }
                        });

                  // sendmail({
                  //             from: 'patil.dilipmg@gmail.com',
                  //             to: driverEmail,
                  //             subject: 'PoopLog:Your sign up request approved successfully.',
                  //             html: mailContent,
                  //           }, function(err, reply) {
                  //             if(err){
                  //             req.flash('error', 'Request Could Not Be Approved.');
                  //             res.redirect('/adminDriver/requested');
                  //             }
                  //             req.flash('success', 'Driver Approved Successfully.');


                  //             //res.redirect('/partners/requested');
                  //         });
                     //req.flash('success', 'Driver Approved Successfully.');
                 }else{
                    req.flash('success', 'Driver Rejected Successfully.');
                 }
                res.redirect('/adminDriver/requested');
            //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});

        })
        .catch(function(err)
        {
            console.log(err);
            req.flash('error', 'Request Could Not Be Approved.');
            res.redirect('/adminDriver/requested');
            //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
        })

    })
    .catch(function(err)
    {
        console.log(err);
        req.flash('error', 'Request Could Not Be Approved.');
        res.redirect('/adminDriver/requested');
        //return res.render('driver/add_driver',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })
});


admindriverRoute.get('/driversAssistantAssociation',function(req,res){
    ssn = req.session;

    if(req.user){

    var username = req.user.username;

      ssn.accesstoken=req.user.accesstoken;
      ssn.username=req.user.username;
      ssn.userType=req.user.userType;
      ssn.userId=req.user.id;
      ssn.profileImage=req.user.profileImage;
      ssn.accesstoken = req.user. accesstoken;


      var username=ssn.username;
      var userType=ssn.userType;
      var userId=ssn.userId;
      var ssnprofileImage=ssn.profileImage;
      var accesstoken = ssn.accesstoken;

      if(userType==1){

        if(accesstoken){
          //req.user.accesstoken;
          var userData = req.user;
          var dataArr = [];
          // Users.update({'accesstoken':accesstoken},{'where':{'username':username}})
          //     .then(function(data){

                var driverType = 4;
                var spoterType= 5;
                var driverLength=1;
                 //Users.findAll({'where':{"userType":driverType,"status":'Y'}})
                 Users.findAll({'where':{"userType":driverType}})
                      .then(function(result)
                      {
                        
                        var totalLength = result.length;

                        if(totalLength){

                          result.map(function(elm){
                            var driverObj={};

                        Users.find({'where':{"userType":spoterType,"status":'Y',"vehicleNumber":elm.vehicleNumber}})
                          .then(function(spoterData)
                          {
                            var spoterObj={};
                            if(spoterData){
                                spoterObj.name=spoterData.name;
                                spoterObj.email=spoterData.email;
                                spoterObj.mobile=spoterData.mobile;
                                spoterObj.profileImage=spoterData.profileImage;
                                spoterObj.licenceNumber=spoterData.licenceNumber;
                                spoterObj.vehicleMakeModel=spoterData.vehicleMakeModel;
                                spoterObj.vehicleNumber=spoterData.vehicleNumber;

                            }

                            //driver obj
                            driverObj.name=elm.name;
                            driverObj.email=elm.email;
                            driverObj.mobile=elm.mobile;
                            driverObj.licenceNumber=elm.licenceNumber;
                            driverObj.vehicleMakeModel=elm.vehicleMakeModel;
                            driverObj.profileImage=elm.profileImage;
                            driverObj.vehicleNumber=elm.vehicleNumber;

                            dataArr.push({"driverObj":driverObj,"spoterObj":spoterObj});
                            if(driverLength==totalLength){

                                console.log(dataArr);
                                res.render('driver/driversAssistantAssociation',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:dataArr});
                            }
                            driverLength++;

                        })//spoter
                            .catch(function(err)
                            {
                                var result=[];
                                req.flash('error',err);
                                res.render('driver/driversAssistantAssociation',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});
                            })

                        })//map
                        }else{//if drivers not available

                              var result=[];
                               // req.flash('error',err);
                                res.render('driver/driversAssistantAssociation',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});

                        }                        
                        

                      })
                    .catch(function(err)
                    {
                        var result=[];
                        req.flash('error',err);
                        res.render('driver/driversAssistantAssociation',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,result:result});
                    })
                   //res.render('user/index',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
             
          }else{
                req.flash('error','User accesstoken not found.');
                res.redirect('/');
          }
      
      }else{
         req.flash('error','Authentication Failed.');
         res.redirect('/');
      }
    }else{//req.user
            req.flash('error','Authentication Failed.');
            res.redirect('/');
    }
});


admindriverRoute.get('/adminupdateRequestedDrivers',function(req,res){
  console.log("updateRequestedDrivers... admin .............");
    var today = new Date();
    ssn = req.session; 
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    console.log("Driver list ");
      UpdateDriverRequests.findAll({'where':{"status":"P"}})

      .then(function(result)
      {
        console.log(result);           
      
        res.render('driver/adminupdateRequestedDrivers',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
      })
    .catch(function(err)
    {
        var result=[];
        req.flash('error',err);
        res.render('driver/adminupdateRequestedDrivers',{data:result,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage});
    })

});

admindriverRoute.get('/updateApproveRequest/:id',function(req,res){
  console.log("updateRequestedDrivers... admin .............");
    var today = new Date();

    ssn = req.session; 
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    var id = req.params.id;
    console.log("Id :"+id);


    console.log("Driver list ");
    UpdateDriverRequests.find({'where':{"id":id}})
      .then(function(result)
      {

        if(result){

          var  params = {
                  "email":result.email,
                  "username":result.email,
                  "name":result.name,
                  "mobile":result.mobile,
                  "vehicleNumber":result.vehicleNumber,
                  "profileImage":result.profileImage,
                  "driverLicence":result.driverLicence,
                  "lasdriLicense":result.lasdriLicense,
                  "updatedAt":today
              };

              Users.update(params,{'where':{'id':result.driverId}})
              .then(function(updateData)
                { 
                  //if(updateData){

                    console.log("checkId : "+id);
                     UpdateDriverRequests.update({'status':'Y'},{'where':{'id':id}})
                    .then(function(userData)
                    {
                      req.flash('success','Driver updated request successfully approved.');
                      res.redirect('/adminDriver/adminupdateRequestedDrivers'); 
                    })
                    .catch(function(err)
                    {
                      req.flash('error','Driver updated request failed.');
                      res.redirect('/adminDriver/adminupdateRequestedDrivers'); 
                    })

                  // }else{
                  //   req.flash('error','Driver updated request failed.');
                  //   res.redirect('/adminDriver/adminupdateRequestedDrivers'); 
                  // }

                               
                })
                .catch(function(err)
                {
                    req.flash('error','Driver updated request failed.');
                  res.redirect('/adminDriver/adminupdateRequestedDrivers'); 
                })

        }else{
               req.flash('error','Record not found.');
              res.redirect('/adminDriver/adminupdateRequestedDrivers');
        }
      })
    .catch(function(err)
    {
      console.log(err);
        req.flash('error',err);
        res.redirect('/adminDriver/adminupdateRequestedDrivers');
    })

});




admindriverRoute.get('/:driverId', function (req, res) {

console.log("Driver list");
     ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;

    console.log("Driver list");
    console.log(req.params.driverId);
    var driverId = req.params.driverId;

    Users.findAll({'where':{"id":driverId}})
      .then(function(result)
      {
        console.log(result);
        res.send(result);
      })
    .catch(function(err)
    {
        var result=[];
        
        res.send(result);
    })

});



module.exports = admindriverRoute;
