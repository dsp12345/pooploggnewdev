var mysql = require('mysql');
var config = require('./../config');
var mysqlModel = require('mysql-model');
 var fs = require("fs");
 var path = require('path');
var multer = require('multer');
var upload = multer({ dest: '/tmp' });
var async = require('async');
var nodemailer = require('nodemailer');

var areaRoute = require('express').Router();

var Users = require('./../models').Users;
var UserRequests = require('./../models').UserRequests;
var Areas = require('./../models').Areas;


var transporter = nodemailer.createTransport({
        host: 'server207.web-hosting.com',
        port: 465,
        secure: true,
        auth: {
            user: 'info@pooplogg.com',
            pass: '#pooplogg2020'
        }
      });

areaRoute.get('/', function (req, res) {
    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    Areas.findAll()
    .then(function(result)
    { 
        result.reverse();
        res.render('area/area',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,result:result});
    })
    .catch(function(err)
    {
        req.flash('error', err);
        res.render('area/area',{accesstoken:accesstoken,username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,result:[]});
    })
    
   
});


areaRoute.post('/', function (req, res) {

  var params = {
    areaCode:req.body.areaCode,
    description:req.body.description,
    status:1
  };

  var areaCode = req.body.areaCode;

  Areas.find({'where':{'areaCode':areaCode}})
      .then(function(areaData){

        if(areaData){

            req.flash('error', 'Area Code Already  Exist.');
             res.redirect('/area');

        }else{

        Areas.create(params)
            .then(function(result){

             req.flash('success', 'Area Code Added Successfully.');
             res.redirect('/area');

          })
          .catch(function(err)
          {
              req.flash('error', 'Area Code Could Not Be Added.');
              res.redirect('/area');
          })
        }

    })
    .catch(function(err)
    {
        req.flash('error', 'Area Code Could Not Be Added.');
        res.redirect('/area');
    })

});


areaRoute.get('/update/:areaId',function(req,res){

    ssn = req.session;
    var accesstoken = ssn.accesstoken;
    var username = ssn.username;
    var userType = ssn.userType;
    var userId = ssn.userId;
    var ssnprofileImage = ssn.profileImage;
    var menuArr = ssn.menuArr;

    var areaId = req.params.areaId;

          Areas.find({'where':{"id":areaId}})
              .then(function(result)
              {
                res.render('area/updateArea',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,result:result});
              })
            .catch(function(err)
            {
                req.flash('error',err);
                res.render('area/updateArea',{username:username,userType:userType,userId:userId,ssnprofileImage:ssnprofileImage,menuArr:menuArr,result:[]});

            });
        
});



areaRoute.post('/update',function(req,res){
  console.log("update................");

        ssn = req.session;
        var accesstoken = ssn.accesstoken;
        var username = ssn.username;
        var userType = ssn.userType;
        var userId = ssn.userId;
        var ssnprofileImage = ssn.profileImage;
        var menuArr = ssn.menuArr;

        var description = req.body.description;
        var areaId = req.body.areaId;

          Areas.update({'description':description},{'where':{'id':areaId}})
            .then(function(data){
                req.flash('success', 'Area Updated Successfully.');
                res.redirect('/area');
              })
            .catch(function(err)
            {
                req.flash('error', 'Area Could Not Be Update.');
                res.redirect('/area');

            });
        
});



areaRoute.get('/action/:areaId/:status',function(req,res){
  console.log("update................");

        ssn = req.session;
        var accesstoken = ssn.accesstoken;
        var username = ssn.username;
        var userType = ssn.userType;
        var userId = ssn.userId;
        var ssnprofileImage = ssn.profileImage;
        var menuArr = ssn.menuArr;

        var areaId = req.params.areaId;
        var status = req.params.status;

          Areas.update({'status':status},{'where':{'id':areaId}})
            .then(function(data){
                
                if(status==1){
                    req.flash('success', 'Area Resume Successfully.');
                }else{
                    req.flash('error', 'Area Suspended Successfully.');
                }
                
                res.redirect('/area');
              })
            .catch(function(err)
            {
                req.flash('error', 'Area Could Not Be Update.');
                res.redirect('/area');

            });
        
});





module.exports = areaRoute;
